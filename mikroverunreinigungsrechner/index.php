<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>EduNat</title>
 <link rel="shortcut icon" href="http://nt3.ch/favicon.png" type="image/png" />
    <!-- Bootstrap -->
    <link href="http://nt3.ch/plugins/font-awesome/font-awesome.min.css" rel="stylesheet">
    <link href="http://nt3.ch/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <script src="dist/js/main.js"></script>
    <script src="http://nt3.ch/js/tooltip.js"></script>

    <![endif]-->
    <link href="/fonts/font-awesome.min.css" rel="stylesheet">
    <style>
    @font-face {
      font-family: 'CalibreWeb-Bold';
      src: url(/fonts/CalibreWeb-Bold.eot);
      src: url(/fonts/CalibreWeb-Bold.eot?#iefix) format('embedded-opentype');
      src: url(/fonts/CalibreWeb-Bold.woff);
      /*src: url(/fonts/CalibreWeb-Bold.woff2);*/
    }

    @font-face {
      font-family: 'CalibreWeb-Light';
      src: url(/fonts/CalibreWeb-Light.eot?#iefix) format('embedded-opentype');
      src: url(/fonts/CalibreWeb-Light.eot);
      src: url(/fonts/CalibreWeb-Light.woff);
      /* src: url(/fonts/CalibreWeb-Light.woff2);*/
    }

    @font-face {
      font-family: 'CalibreWeb-LightItalic';
      src: url(/fonts/CalibreWeb-Light.eot?#iefix) format('embedded-opentype');
      src: url(/fonts/CalibreWeb-LightItalic.eot);
      src: url(/fonts/CalibreWeb-LightItalic.woff);
      /* src: url(/fonts/CalibreWeb-LightItalic.woff2);*/
    }
    </style>
</head>
<body class="themeBlue" data-spy="scroll" data-target="#navbar" data-offset="200">
<div class="nav-wrapper">
    <?php include ("templates/header.html"); ?>
    <div class="main content-js">
        <div class="content content-main background-theme">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <div class="content-text content-text-color">
                        <span class="title-main title ">
                            <span class="title-line">
                            <h1 class="title-sm hidden-xs">MIKROVERUNREINIGUNGEN</h1>
                            <h1 class="title-sm hidden-sm hidden-md hidden-lg">MIKROVERUN- <br>REINIGUNGEN</h1>
                            </span>
                        <span class="byline">Spurenstoffe mit Klärungsbedarf</span>
                        </span>
                            <p>Zehntausende verschiedene Chemikalien werden jedes Jahr in der Schweiz hergestellt und
                                verarbeitet. Viele davon finden sich in Alltagsprodukten wie Kosmetika oder Medikamenten
                                wieder. Diese Stoffe gelangen z.B. bei der Körperpflege oder beim Toilettengang ins
                                Abwasser und können in Abwasserreinigungsanlagen kaum aus dem Wasser entfernt werden.
                                Dadurch gelangen die Chemikalien in die Gewässer unserer Umwelt, wo sie als
                                Mikroverunreinigungen bezeichnet werden. Der Begriff leitet sich ab von den typischen
                                Konzentrationen der Stoffe, die in Gewässern im Bereich von Mikrogramm pro Liter
                                auftreten. Schon in solch niedrigen Konzentrationen können Mikroverunreinigungen auf in
                                Gewässern beheimatete Lebewesen giftig wirken – Grund genug, Verbrauch, Freisetzung und
                                Wirkung dieser Stoffe genauer unter die Lupe zu nehmen!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="flex-box">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                         <a href="#" id="school" class="linknav-js">
                            <div class="content-text content-text-small content-text-color content-text-column">
                        <span class="title title-small title-small-column">
                        <h2>Schule</h2>
                        </span>
                                <p>Greifen Sie das Thema Mikroverunreinigungen in der Schule auf. Gebrauchsfertige
                                    Unterlagen für den handlungsorientierten Unterricht.</p>
                            </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <a href="#" id="freetime" class="linknav-js">
                            <div class="content-text content-text-small content-text-color content-text-column">
                        <span class="title title-small title-small-column">
                        <h2>Freizeit</h2>
                        </span>
                                <p>Begib dich auf eine spannende Schnitzeljagd zum Thema Mikroverunreinigungen.</p>
                            </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <a href="http://mikroverunreinigungsrechner.nt3.ch" target="_blank" id="webapp">
                            <div class="content-text content-text-small content-text-color content-text-column">
                        <span class="title title-small title-small-column">
                        <h2>Webapp</h2>
                        </span>
                                <p>Die eigenen Mikroverunreinigungen berechnen. Verbrauch schätzen, Klärmodus wählen und
                                    Gewässerbelastung ermitteln.</p>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include ("../templates/footer.html"); ?>
    </div>
    <div class="content-js school">
        <div class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <div class="content-text content-text-small content-text-color">
                        <span class="title title-small">
                        <h2>Schule</h2>
                        </span>
                            <p>Das Modul Mikroverunreinigungen wurde für den fächerübergreifenden
                                naturwissenschaftlichen Unterricht der Sek I entwickelt. Im Unterrichtsmodul wird der
                                Eintrag von Mikroverunreinigungen in Flüsse und Seen thematisiert, wobei technologische
                                Ansätze zur Entfernung dieser Stoffe aus dem Abwasser im Mittelpunkt stehen.</p>
                            <div>

                                <div id="moreinfo1" class="collapse">
                                    <p>Übergeordnetes Ziel ist es, die Jugendlichen für diese Problematik zu
                                        sensibilisieren und Lösungsansätze aufzuzeigen. Dabei unterstützt das Modul die
                                        handlungsorientierte Vermittlung von Grundlagen und Kompetenzen gemäss Lehrplan
                                        21:</p>
                                    <ul>
                                        <li>Chemie (Stoffkonzentrationen, Adsorptionsphänomene etc.)</li>
                                        <li>Biologie (aquatische Ökosysteme, Toxische Wirkungen auf Organismen)</li>
                                        <li>Mathematik (Berechnung von Massenbilanzen etc.)</li>
                                        <li>Mensch und Umwelt</li>
                                        <li>fächerübergreifende naturwissenschaftliche Kompetenzen (Interpretieren von
                                            Diagrammen, Experimentieren nach Protokoll, Auswerten von
                                            Versuchsergebnissen
                                            etc.)
                                        </li>
                                    </ul>
                                    <p>Das Modul kann in 7-10 Lektionen bearbeitet und je nach Niveau der Klasse und
                                        verfügbarer Zeit modular angepasst werden. Zu den Blöcken I-V finden Sie:</p>
                                    <ul>
                                        <li> Dokumente mit didaktischen Hinweisen</li>
                                        <li> Präsentationsmaterial</li>
                                        <li> Arbeitsblätter und Experimentieranleitungen</li>
                                        <li> Weiterführende Hintergrundinformationen</li>
                                    </ul>

                                </div>
                                <button data-toggle="collapse" data-target="#moreinfo1"
                                        class="nostyle link-italic moreinfo collapsed"><i class="fa fa-long-arrow-right"
                                                                                          aria-hidden="true"></i>
                                </button>
                            </div>
                            <a href="#" class="link-bold"><i class="fa fa-arrow-right" aria-hidden="true"></i> Modul
                                herunterladen</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content scrollspy" data-spy="scroll"
             data-target=".list-scrollspy">
            <div class="container-fluid">
                <div class="row">
                    <div class="block" id="block1">
                        <div class="col-md-12">
                            <div class="content-text content-text-small content-text-normal">
                            <span>
                            <ul class="list-block list-scrollspy">
                                <li><a href="#block1" class="active">Block I</a></li>
                                <li><a href="#block2">Block II</a></li>
                                <li><a href="#block3">Block III</a></li>
                                <li><a href="#block4">Block IV</a></li>
                                <li><a href="#block5">Block V</a></li>
                            </ul>

                            <span class="title-normal">
                         <h3 class="hidden-xs">WAS SIND MIKROVERUNREINIGUNGEN?</h3>
                         <h3 class="hidden visible-xs">WAS SIND MIKRO-<br>VERUNREINIGUNGEN?</h3>

                            <span class="byline byline-small">VORKOMMEN UND RELEVANZ</span>
                        </span>
                            <div class="row">
                                <div class="col-md-7">
                                    <i>Ausgehend von persönlichen Erfahrungen wird in Block I zunächst die
                                        Freisetzung von Mikroverunreinigungen und ihre Wirkung auf Mensch und Umwelt thematisiert, um den grundlegenden Handlungsbedarf aufzuzeigen.
                                    </i>

                                </div>
                                <div class="col-md-5">
                                    <table style="width:100%">
                                        <tr>
                                            <th>Dauer</th>
                                            <td>50-100min</td>
                                        </tr>

                                        <tr>
                                            <th>Anzahl Sequenzen</th>
                                            <td>6</td>
                                        </tr>
                                        <tr>
                                            <th>Voraussetzungen</th>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <th>Empfehlungen</th>
                                            <td>I.A, I.B, I.C, I.D, I.F</td>
                                        </tr>
                                    </table>

                                </div>
                                <div class="col-md-12">
                                    <a href="#" class="link-bold"><i class="fa fa-arrow-right" aria-hidden="true"></i>
                                        Block herunterladen</a>
                                </div>

                            </div>  </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="block" id="block2">
                        <div class="col-md-12">
                            <div class="content-text content-text-small content-text-normal">
                            <span>
                            <ul class="list-block list-scrollspy">
                                <li><a href="#block1">Block I</a></li>
                                <li><a href="#block2" class="active">Block II</a></li>
                                <li><a href="#block3">Block III</a></li>
                                <li><a href="#block4">Block IV</a></li>
                                <li><a href="#block5">Block V</a></li>
                            </ul>
                            <span class="title-normal">

                          <h3 class="hidden-xs">WASSERREINIGUNGSEXPERIMENTE</h3>
                          <h3 class="hidden visible-xs">WASSERREINIGUNGS-<br>EXPERIMENTE</h3>
                            <span class="byline byline-small">VERSUCHSDURCHFÜHRUNG MIT AKTIVKOHLE</span>
                        </span>
                            <div class="row">
                                <div class="col-md-7">
                                    <i>In Block II führen die Schülerinnen und Schüler in Kleingruppen Experimente zur
                                        Wasserreinigung mittels Sandfiltration und Aktivkohle durch. Die Ergebnisse
                                        werden im Protokoll festgehalten und anschliessend im Klassenverband
                                        ausgewertet.
                                    </i>
                                </div>
                                <div class="col-md-5">
                                    <table style="width:100%">
                                        <tr>
                                            <th>Dauer</th>
                                            <td>60-110min</td>
                                        </tr>

                                        <tr>
                                            <th>Anzahl Sequenzen</th>
                                            <td>4</td>
                                        </tr>
                                        <tr>
                                            <th>Voraussetzungen</th>
                                            <td>Vorbereitung Experimentiermaterial</td>
                                        </tr>
                                        <tr>
                                            <th>Empfehlungen</th>
                                            <td>II.A, II.B, II.C, II.D</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-12">
                                    <a href="#" class="link-bold"><i class="fa fa-arrow-right" aria-hidden="true"></i>
                                        Block herunterladen</a>
                                </div>
                            </div>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="block" id="block3">
                        <div class="col-md-12">
                            <div class="content-text content-text-small content-text-normal">
                            <span>
                            <ul class="list-block list-scrollspy">
                                <li><a href="#block1">Block I</a></li>
                                <li><a href="#block2">Block II</a></li>
                                <li><a href="#block3" class="active">Block III</a></li>
                                <li><a href="#block4">Block IV</a></li>
                                <li><a href="#block5">Block V</a></li>
                            </ul>

                            <span class="title-normal">
                         <h3>FILTRATION UND ADSORPTION</h3>
                            <span class="byline byline-small">TECHNISCHE ENTFERNUNG VON MIKROVERUNREINIGUNGEN</span>
                        </span>
                            <div class="row">
                                <div class="col-md-7">
                                    <i>In Block III werden Möglichkeiten vorgestellt, Mikroverunreinigungen aus
                                        dem Abwasser zu entfernen. Im Vordergrund steht die anschauliche
                                        Erklärung ihrer chemisch-physikalischen Funktionsweisen und der
                                        technischen Umsetzungen.
                                    </i>
                                </div>
                                <div class="col-md-5">
                                    <table style="width:100%">
                                        <tr>
                                            <th>Dauer</th>
                                            <td>40-55min</td>
                                        </tr>

                                        <tr>
                                            <th>Anzahl Sequenzen</th>
                                            <td>3</td>
                                        </tr>
                                        <tr>
                                            <th>Voraussetzungen</th>
                                            <td>I.B-I.D</td>
                                        </tr>
                                        <tr>
                                            <th>Empfehlungen</th>
                                            <td>III.A, III.B, III.C</td>
                                        </tr>
                                    </table>

                                </div>
                                <div class="col-md-12">
                                    <a href="#" class="link-bold"><i class="fa fa-arrow-right" aria-hidden="true"></i>
                                        Block herunterladen</a>
                                </div>
                            </div>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="block" id="block4>
                        <div class=" col-md-12">
                    <div class="content-text content-text-small content-text-normal">
                        <span>
                        <ul class="list-block list-scrollspy">
                            <li><a href="#block1">Block I</a></li>
                            <li><a href="#block2">Block II</a></li>
                            <li><a href="#block3">Block III</a></li>
                            <li><a href="#block4" class="active">Block IV</a></li>
                            <li><a href="#block5">Block V</a></li>
                        </ul>

                        <span class="title-normal">

                         <h3 class="hidden-xs">MIKROVERUNREINIGUNGSRECHNER</h3>
                          <h3 class="hidden visible-xs">MIKROVERUN-<br>REINIGUNGSRECHNER</h3>
                            <span class="byline byline-small">ERFASSEN, AUSWERTEN UND DARSTELLEN VON DATEN</span>
                        </span>
                        <div class="row">
                            <div class="col-md-7">
                                <i>In Block I haben die Jugendlichen den Schmerzmittelverbrauch in ihrem
                                    Haushalt erfasst. In Block IV werden die Daten im Klassenverband mithilfe
                                    des Mikroverunreinigungsrechners so ausgewertet, dass der eigene "Fussabdruck"
                                    erkennbar wird.
                                </i>
                            </div>
                            <div class="col-md-5">
                                <table style="width:100%">
                                    <tr>
                                        <th>Dauer</th>
                                        <td>55-100min</td>
                                    </tr>

                                    <tr>
                                        <th>Anzahl Sequenzen</th>
                                        <td>3</td>
                                    </tr>
                                    <tr>
                                        <th>Voraussetzungen</th>
                                        <td>I.B, I.D, I.F</td>
                                    </tr>
                                    <tr>
                                        <th>Empfehlungen</th>
                                        <td>IV.A, IV.B, IV.C</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-12">
                                <a href="#" class="link-bold"><i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    Block herunterladen</a>
                            </div>
                        </div>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="block" id="block5">
                <div class="col-md-12">
                    <div class="content-text content-text-small content-text-normal">
                            <span>
                            <ul class="list-block list-scrollspy">
                                <li><a href="#block1">Block I</a></li>
                                <li><a href="#block2">Block II</a></li>
                                <li><a href="#block3">Block III</a></li>
                                <li><a href="#block4">Block IV</a></li>
                                <li><a href="#block5" class="active">Block V</a></li>
                            </ul>

                            <span class="title-normal">
                         <h3>DISKUSSIONSRUNDE GEWÄSSERSCHUTZ</h3>
                            <span class="byline byline-small">GESAMTHEITLICHE BETRACHTUNG</span>
                        </span>
                            <div class="row">
                                <div class="col-md-7">
                                    <i>Im letzten Block werden die Inhalte in den grösseren gesellschaftlichen Rahmen eingebettet und zusammen mit den Jugendlichen gesamtheitlich diskutiert und reflektiert. Daraus resultieren praktische Hinweise für die Schülerinnen und Schüler, wie sie im Alltag zum verantwortungsvollen Umgang mit Mikroverunreinigungen beitragen können.
                                    </i>
                                </div>
                                <div class="col-md-5">
                                    <table style="width:100%">
                                        <tr>
                                            <th>Dauer</th>
                                            <td>50-90min</td>
                                        </tr>

                                        <tr>
                                            <th>Anzahl Sequenzen</th>
                                            <td>3</td>
                                        </tr>
                                        <tr>
                                            <th>Voraussetzungen</th>
                                            <td>I.B-I.D, III.A-III.C</td>
                                        </tr>
                                        <tr>
                                            <th>Empfehlungen</th>
                                            <td>V.A, V.B, V.C</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-12">
                                    <a href="#" class="link-bold"><i class="fa fa-arrow-right" aria-hidden="true"></i>
                                        Block herunterladen</a>
                                </div>
                            </div>
                            </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include ("../templates/footer.html"); ?>
</div>
<div class="content-js freetime">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-7 col-sm-12 col-xs-12">
                    <div class="content-text content-text-small content-text-color">
                        <span class="title-small title">
                        <h2>Freizeit</h2>
                        </span>
                    </div>
                </div>
            </div>
            <br>

        </div>
    </div>
    <?php include ("../templates/footer.html"); ?>
</div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="http://nt3.ch/dist/js/bootstrap.min.js"></script>

<script src="http://nt3.ch/js/tooltip.js"></script>
</body>
</html>