/**
 * Test Function
 */

//Globale Variablen
//Wirkstoffe
var Wirkstofflist = [{
	Wirkstoffname: "Diclofenac",
	Wirkstoffklasse: "wirkstoff",
	Beschreibung: "Diclofenac ist ein schmerzlindernder und entzündungshemmender Wirkstoff, der z.B. bei Sportverletzungen als Gel seine Anwendung findet.",
	Abbaukoerper: 0.3,
	ohne: 0,
	ohnefilter: 0.25,
	mitfilter: 0.95,
	Grenzwert: 0.0000001, //Grenzwert g/l
	Durchschnitt: 1.1,  //Durchschnittsverbrauch der Bevölkerung in Einheit der Anwendung,
	GrammproPackung: 1,
	Packungsart: "Tube",
	Anwendung: [0, 10, 0.1] //min-, max-Wert sowie step in der Einheit (Tube/ Packung )/Jahr
}, {
	Wirkstoffname: "Paracetamol",
	Wirkstoffklasse: "wirkstoff",
	Beschreibung: "Paracetamol ist ein Wirkstoff schmerzlindernden Eigenschaften, der z.B. in Tablettenform zur Behandlung von Kopf- und  Gliederschmerzen eingesetzt wird.",
	Abbaukoerper: 0.95,
	ohne: 0,
	ohnefilter: 0.9,
	mitfilter: 0.98,
	Grenzwert: 0.0000001,
	Durchschnitt: 1.1,
	GrammproPackung: 1,
	Packungsart: "Packung",
	Anwendung: [0, 15, 0.1] //min-, max-Wert sowie step in der Einheit (Tube/ Packung )/Jahr
}, {
	Wirkstoffname: "Ibuprofen",
	Wirkstoffklasse: "wirkstoff",
	Beschreibung: "Ibuprofen ist ein Stoff mit fiebersenkender und entzündungshemmender Wirkung, der z.B. in Tabletten gegen Fieber und Entzündungszustände verwendet wird.",
	Abbaukoerper: 0.9,
	ohne: 0,
	ohnefilter: 0.9,
	mitfilter: 0.9,
	Grenzwert: 0.0000001,
	Durchschnitt: 1.5,
	GrammproPackung: 1,
	Packungsart: "Tube",
	Anwendung: [0, 5, 0.1] //min-, max-Wert sowie step in der Einheit (Tube/ Packung )/Jahr
}];

//Wertebereich inclusive Durchschnitt definieren
//var Anwendung = [0, 5, 0.1]; //min-, max-Wert sowie step in der Einheit (Tube/ Packung )/Jahr

//Kläranlage
var Klaeranlagelist = [{
	Klaeranlagename: "ohne",
	Klaeranlageklasse: "klaeranlage",
	Klaeranlagebegriff: "ohne",
	Beschreibung1: "Ohne ARA: ",
	Beschreibung2: "% des Abwassers und der enthaltenen Wirkstoffe gelangen ohne Reinigung direkt in die Umwelt.",
}, {
	Klaeranlagename: "ohnefilter",
	Klaeranlageklasse: "klaeranlage",
	Klaeranlagebegriff: "ohne MV-Filter",
	Beschreibung1: "ARA ohne MV-Stufe: Das Abwasser wird in der Kläranlage biologisch und chemisch gereinigt, sodass nur ",
	Beschreibung2: "% der Wirkstoffmenge in die Umwelt gelangen."
}, {
	Klaeranlagename: "mitfilter",
	Klaeranlageklasse: "klaeranlage",
	Klaeranlagebegriff: "mit MV-Filter",
	Beschreibung1: "ARA mit MV-Stufe: Durch eine weitergehende Abwasserreinigung, z.B. mit Aktivkohle, wird der Wirkstoff gut aus dem Abwasser entfernt und es fliessen nur ",
	Beschreibung2: "% davon in die Umwelt."
}];

//Gewaesser
var Gewaesserlist = [{
	Name: "Bach",
	Verduennungswert: 10,
	Beschreibung: "Wenn das Abwasser in einen kleinen Bach gelangt,  ist der Wirkstoff im Gewässer verhältnismässig hoch konzentriert."
}];

//Loesungsvariablen
var Loesung = [Wirkstofflist[0], Klaeranlagelist[1],Gewaesserlist[1]];

//Verdünnung mit Abwasser
var vermabwasser = 300000; // l/Jahr

//Bilder für die versch. Resultate
var img = ["img/unhappy.svg",
	"img/middlehappy.svg",
	"img/happy.svg"];

//Texte für versch. Resultate
var resulttext = ["Der Grenzwert wurde überschritten.", "Man ist sehr nahe am Grenzwert.", "Der Grenzwert wurde unterschritten."];

var dosispropunkt = 0.1; //Dosis welche als Pkt dargestellt wird (g/pkt)
var PunkteproTube = 10; //Punkte welche in einer Tube dargestellt werden

//Defaultwerte
var wirkstoff = Wirkstofflist[0];
var dosis = wirkstoff.Durchschnitt;
var klaeranl = Klaeranlagelist[0];
var gew = Gewaesserlist[0];
var durchschnitt = wirkstoff.Durchschnitt;
var koerpernichtabbau = (1 - wirkstoff.Abbaukoerper) * dosis;
var imabw = koerpernichtabbau / vermabwasser;
var nachanl = imabw * (1 - wirkstoff[klaeranl.Klaeranlagename]);
var restwert = nachanl / gew.Verduennungswert;
var radius = 2;

/**
 * Funktion zum initialisieren
 */
function init() {

	//Wirkstoffe initialisieren
	initWirkstoff();

	//Menge initialisieren
	initMenge();

	//Mengentext ändern
	document.getElementById("range1").innerHTML = (dosis / wirkstoff.GrammproPackung) + " " + wirkstoff.Packungsart + " /Jahr";
	document.getElementById("range1_1").innerHTML = dosis + " Gramm /Jahr";

	//Mengentext ändern
	document.getElementById("min1").innerHTML = wirkstoff.Anwendung[0];
	document.getElementById("max1").innerHTML = wirkstoff.Anwendung[1];

	//Menge darstellen
	anzeigeMenge(dosis);
	visMenge(dosis);

	anzeigeAbbKoerper(dosis, koerpernichtabbau);
	visAbbKoerper(dosis, koerpernichtabbau);
	visAbw(imabw, wirkstoff);

	initAnlage();
	visAbbAnlage(imabw, nachanl);
	initGew();

	anzeigeRest(restwert, wirkstoff, (gew.Verduennungswert * vermabwasser));
	visRest(dosis, restwert, wirkstoff);

	var endOfUrl = document.URL.split("?")[1];
	if (endOfUrl == "Loesung") {
		Grenzwert_zu_Verbrauch(Loesung[0], Loesung[1], Loesung[2], vermabwasser);
	}
}

/**
 * Funktion zum initialisieren der Wirkstoff Optionen
 */
function initWirkstoff() {

	//für alle Elemente der globalen Variable Wirkstofflist
	for (var i = 0; i < Wirkstofflist.length; i++) {

		//Optionen in ID wirkstoffselect schreiben
		d3.select("#wirkstoffselect")
			.append("option")
			.attr("id", Wirkstofflist[i].Wirkstoffname)//Wirkstoffname des Elementes der Wirkstofflist
			.attr("class", "wirkstoff")
			.attr("value", Wirkstofflist[i].Wirkstoffname)
			.text(Wirkstofflist[i].Wirkstoffname);

	}

	//Auswahl der angezeigten Option mithilfe des Attributes "selected"
	document.getElementById(wirkstoff.Wirkstoffname).setAttribute("selected", "1");
}

/**
 * Funktion zum initialisieren der Menge Optionen
 */
function initMenge() {

	//Elemente neu füllen, damit Werte und Anzeigen in Sliders stimmen
	var scale1 = document.createElement("input");
	scale1.setAttribute("id", "scale1");
	scale1.setAttribute("class", "seekBar");
	scale1.setAttribute("type", "range");
	scale1.setAttribute("min", wirkstoff.Anwendung[0].toString());
	scale1.setAttribute("max", wirkstoff.Anwendung[1].toString());
	scale1.setAttribute("step", wirkstoff.Anwendung[2].toString());
	scale1.setAttribute("value", (dosis/ wirkstoff.GrammproPackung).toString());
	scale1.setAttribute("onchange", "selectorMenge(this.value)");
	document.getElementById('scalelegend').appendChild(scale1);
}

/**
 * Funktion zum initialisieren der Wirkstoff Optionen
 */
function initAnlage() {

	//für alle Elemente der globalen Variable Klaeranlagelist
	for (var i = 0; i < Klaeranlagelist.length; i++) {

		//Optionen in ID wirkstoffselect schreiben
		d3.select("#klaeranlageselect")
			.append("option")
			.attr("id", Klaeranlagelist[i].Klaeranlagename)
			.attr("class", "klaeranlage")
			.attr("value", Klaeranlagelist[i].Klaeranlagename)
			.text(Klaeranlagelist[i].Klaeranlagebegriff);
	}

	//Auswahl der angezeigten Option mithilfe des Attributes "selected"
	document.getElementById(klaeranl.Klaeranlagename).setAttribute("selected", "1");
}

/**
 * Funktion zum initialisieren der Gewässer Optionen
 */
function initGew() {

	//für alle Elemente der globalen Variable Gewaesserlist
	for (var i = 0; i < Gewaesserlist.length; i++) {

		//Optionen in ID gewaesserselect schreiben
		d3.select("#gewaesserselect")
			.append("option")
			.attr("id", Gewaesserlist[i].Name)
			.attr("class", "gewaesser")
			.attr("value", Gewaesserlist[i].Name)
			.text(Gewaesserlist[i].Name);
	}

	//Auswahl der angezeigten Option mithilfe des Attributes "selected"
	document.getElementById(gew.Name).setAttribute("selected", "1");
}

/**
 * Funktion zur Auswahl des Wirkstoffes in der legende
 * @param {String}    Wirkstoff    Wirkstoffstyp
 */
function selectorWirkstofflegend(Wirkstoff) {

	//Wirkstoff aus Wirkstoffliste extrahieren
	for (var i = 0; i < Wirkstofflist.length; i++) {
		if (Wirkstofflist[i].Wirkstoffname == Wirkstoff) {
			wirkstoff = Wirkstofflist[i];
		}
	}

	//Durchschnitt des gewählten Wirkstoffes herauslesen
	dosis = wirkstoff.Durchschnitt;
	durchschnitt = wirkstoff.Durchschnitt;

	selectorMenge((dosis / wirkstoff.GrammproPackung));



	//Slider-element löschen
	document.getElementById('scale1').remove();

	//Slider-element neu aufbauen
	var scale = document.createElement("input");

	scale.setAttribute("id", 'scale1');
	scale.setAttribute("class", "seekBar");
	scale.setAttribute("type", "range");
	scale.setAttribute("min", String(wirkstoff.Anwendung[0]));
	scale.setAttribute("max", String(wirkstoff.Anwendung[1]));
	scale.setAttribute("step", String(wirkstoff.Anwendung[2]));
	scale.setAttribute("value", String((dosis / wirkstoff.GrammproPackung)));
	scale.setAttribute("onchange", "selectorMenge(this.value)");

	document.getElementsByClassName('scale1')[0].appendChild(scale);

	//Visualisierung des Wirkstoffes und der Menge
	visMenge(dosis);

	//Berechnung und Visualisierung des Abbaus im Körper
	berAbbKoerper(wirkstoff, dosis);
	anzeigeAbbKoerper(dosis, koerpernichtabbau);
	visAbbKoerper(dosis, koerpernichtabbau);

	//Vermischung mit dem Abwasser und Vis/ Berechnung der Kläranlage
	vermAbw(koerpernichtabbau, vermabwasser);
	visAbw(imabw, wirkstoff);
	abbauAnlage(imabw, klaeranl, wirkstoff);
	visAbbAnlage(imabw, nachanl);

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, (gew.Verduennungswert * vermabwasser));
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (wirkstoff);
	//return(durchschnitt);
}

/**
 * Funktion um Slidervalue zu ändern
 * @param {Number}    Tube    gewählte Dosis (g/Jahr)
 */
function selectorMenge(Tube) {

	//Dosis definieren
	dosis = Tube * wirkstoff.GrammproPackung;

	document.getElementById("min1").innerHTML = wirkstoff.Anwendung[0];
	document.getElementById("max1").innerHTML = wirkstoff.Anwendung[1];

	//Sliders einander anpasseun
	changeSlider(Tube);

	//Visualisierung der Menge
	anzeigeMenge(dosis);
	visMenge(dosis);

	//Berechnung und Visualisierung des Abbaus im Körper
	berAbbKoerper(wirkstoff, dosis);
	visAbbKoerper(dosis, koerpernichtabbau);
	anzeigeAbbKoerper(dosis, koerpernichtabbau);

	//Vermischung mit dem Abwasser und Vis/ Berechnung der Kläranlage
	vermAbw(koerpernichtabbau, vermabwasser);
	visAbw(imabw, wirkstoff);
	abbauAnlage(imabw, klaeranl, wirkstoff);
	visAbbAnlage(imabw, nachanl);

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, (gew.Verduennungswert * vermabwasser));
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (dosis);
}

/**
 * Funktion um Anlage in der Legenede zu selektieren
 * @param {String}    Anlage    Anlagetyp
 */
function selectorAnlagelegend(Anlage) {

	//Kläranlage aus Kläranlageliste extrahieren
	for (var i = 0; i < Klaeranlagelist.length; i++) {
		if (Klaeranlagelist[i].Klaeranlagename == Anlage) {
			klaeranl = Klaeranlagelist[i];
		}
	}

	//Vermischung mit dem Abwasser und Vis/ Berechnung der Kläranlage
	abbauAnlage(imabw, klaeranl, wirkstoff);
	visAbbAnlage(imabw, nachanl);

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, (gew.Verduennungswert * vermabwasser));
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (klaeranl);
}

/**
 * Funktion um Gewässer in der Legende selektieren
 * @param {String}    Gewaesser    Gewässertyp
 */
function selectorGewlegend(Gewaesser) {

	//Gewässer aus Gewässerliste extrahieren
	for (var i = 0; i < Gewaesserlist.length; i++) {
		if (Gewaesserlist[i].Name == Gewaesser) {
			gew = Gewaesserlist[i];
		}
	}

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, (gew.Verduennungswert * vermabwasser));
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (gew);
}

/**
 * Funktion um anzuzeigen wie hoch der maximale Durchschnittsverbrauch bei bestimmetn Einstellungen sein darf
 * @param {Object} Wirkstoff gewählter Wirkstoff mit all seinen Eigenschaften
 * @param {Object} Anlage gewählte Anlage mit all ihren Eigenschaften
 * @param {Object} Gewaesser gewähltes Gewässer mit all seinen Eigenschaften
 * @param {Object} VermAbwasser vermischte Abwassermenge
 */
function Grenzwert_zu_Verbrauch(Wirkstoff, Anlage, Gewaesser, VermAbwasser) {

	//Berechnung der dosis
	dosis = Wirkstoff.Grenzwert * Gewaesser.Verduennungswert / (1 - Wirkstoff[Anlage.Klaeranlagename]) * VermAbwasser / (1 - Wirkstoff.Abbaukoerper);

	//dosis wird auf 10-tel Stelle abgerundet
	dosis = (Math.floor(dosis * 10) / 10);

	//Werte
	wirkstoff = Wirkstoff;
	klaeranl = Anlage;
	gew = Gewaesser;
	durchschnitt = wirkstoff.Durchschnitt;
	koerpernichtabbau = (1 - wirkstoff.Abbaukoerper) * dosis;
	imabw = koerpernichtabbau / vermabwasser;
	nachanl = imabw * (1 - wirkstoff[klaeranl.Klaeranlagename]);
	restwert = nachanl / gew.Verduennungswert;

	//Dosis wird herausgegeben
	alert("Mit diesen Einstellungen dürfen durchschnittlich " + dosis + "g pro Jahr gebraucht werden.");

	//Diverses anpassen
	document.getElementById("scalelegend").innerHTML = "";
	document.getElementById("wirkstoffselect").innerHTML = "";
	document.getElementById("klaeranlageselect").innerHTML = "";
	document.getElementById("gewaesserselect").innerHTML = "";
	initWirkstoff();
	initMenge();
	changeSlider(dosis / wirkstoff.GrammproPackung);
	initAnlage();
	initGew();

	//Visualisierung der Menge
	anzeigeMenge(dosis);
	visMenge(dosis);

	//Berechnung und Visualisierung des Abbaus im Körper
	berAbbKoerper(wirkstoff, dosis);
	visAbbKoerper(dosis, koerpernichtabbau);
	anzeigeAbbKoerper(dosis, koerpernichtabbau);

	//Vermischung mit dem Abwasser und Vis/ Berechnung der Kläranlage
	vermAbw(koerpernichtabbau, vermabwasser);
	visAbw(imabw, wirkstoff);
	abbauAnlage(imabw, klaeranl, wirkstoff);
	visAbbAnlage(imabw, nachanl);

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, (gew.Verduennungswert * vermabwasser));
	visRest(dosis, restwert, wirkstoff);

}

/**
 * Funktion um Berechnungen anzuzeigen
 */
function clickgen1() {

	//lokale Variablen definieren
	var gen1 = document.querySelectorAll(".generation1"); //Skizzen-canvases
	var gen2 = document.querySelectorAll(".generation2"); //Berechnungs-canvases

	//für jedes Element der Klasse generation1
	for (var i = 0; i < gen1.length; i++) {

		//z-index = 0; somit kommen die Elemente in den Hintergrund
		gen1[i].setAttribute("style", "z-index:0");
	}

	//für jedes Element der Klasse generation2
	for (var k = 0; k < gen2.length; k++) {

		//z-index = 1; somit kommen die Elemente in den Vordergrund
		gen2[k].setAttribute("style", "z-index:1");
	}

	//clickbuttontext (value) ändern und neue Funktion (onclick) zuweisen
	document.getElementById("generationchange").setAttribute("value", "Skizzen anzeigen");
	document.getElementById("generationchange").setAttribute("onclick", "clickgen2()");
}

/**
 * Funktion um Skizzen anzuzeigen
 */
function clickgen2() {

	//lokale Variablen definieren
	var gen1 = document.querySelectorAll(".generation1");
	var gen2 = document.querySelectorAll(".generation2");

	//für jedes Element der Klasse generation1
	for (var i = 0; i < gen1.length; i = i + 1) {

		//z-index = 1; somit kommen die Elemente in den Vordergrund
		gen1[i].setAttribute("style", "z-index:1");
	}

	//für jedes Element der Klasse generation2
	for (var k = 0; k < gen2.length; k++) {

		//z-index = 0; somit kommen die Elemente in den Hintergrund
		gen2[k].setAttribute("style", "z-index:0");
	}

	//clickbuttontext (value) ändern und neue Funktion (onclick) zuweisen
	document.getElementById("generationchange").setAttribute("value", "Berechnungen anzeigen");
	document.getElementById("generationchange").setAttribute("onclick", "clickgen1()")
}

/**
 * Funktion um Slidervalue zu ändern
 * @param    {Number} Tube    gewählte Dosis (Tuben/Jahr)
 */
function changeSlider(Tube) {

	//Mengentexte ändern
	document.getElementById("range1").innerHTML = Tube + " " + wirkstoff.Packungsart + " /Jahr";
	document.getElementById("range1_1").innerHTML = dosis + " Gramm /Jahr";
}

/**
 * Funktion um in der Legende gewählte Anlage aunzuzeigen
 * @param {Object}    Anlage    Anlage mit all ihren Werten
 */
function anzeigeAnlagelegend(Anlage) {

	//für alle Elemente der globalen Liste
	for (var i = 0; i < Klaeranlagelist.length; i++) {

		//Durchsuchen nach dem gewählten Element
		if (Klaeranlagelist[i].Klaeranlagename == Anlage.Klaeranlagename) {

			//die Klassen zuweisen (für css Einfärbung)
			document.getElementById((Klaeranlagelist[i].Klaeranlagename + "anlage")).setAttribute("class", "selectanlage");
		}
		else {
			document.getElementById((Klaeranlagelist[i].Klaeranlagename + "anlage")).setAttribute("class", "klaeranlage");
		}
	}
}

/**
 * Funktion um die Dosis grafisch (in Tuben) darzustellen
 * @param    {Number} Menge    gewählte Dosis (g/Jahr)
 */

function anzeigeMenge(Menge) {

	//Vlokale Variablen definieren
	var Pkte = Menge / dosispropunkt; //Anzahl dargestellter Punkte
	var e = 0; //Hilfsvariable
	var Tube = Math.ceil(Pkte / PunkteproTube); //Anzahl dargestellter Tuben (pro 10 Pkte 1 Tube)

	//Variablen des svg's (Attribute)
	var w = 800;
	var h = 200 * Tube + 100;
	//h an w anpassen
	if (h < w) {
		h = w;
	};
	var padding = 100;

	//Alte Elemente in canvas 1 löschen
	document.getElementById('canvas1').innerHTML = '';

	//Create new svg-Element with class circle
	var svg = d3.select("#canvas1")
		.append("svg")
		.attr("class", "circle")
		.attr("width", w)
		.attr("height", h)
		.attr("viewBox", "0 0 " + w + " " + h);

	//erstellen von Tubenelementen
	for (e; e < Tube; e = e + 1) {
        d3.select(".circle")
            .append("defs")
            .append("pattern")
            .attr("id", "capsule")
            .attr("x", "0%")
            .attr("y", "0%")
            .attr("height", "100%")
            .attr("width", "100%")
            .attr("viewBox", "0 0 115 115")
            .append("image")
            .attr("x", "0")
            .attr("y", "0")
            .attr("width", "100")
            .attr("height", "100")
            .attr("xlink:href", "img/capsule.svg");
        d3.select(".circle")
            .append("defs")
            .append("pattern")
            .attr("id", "capsuleGrey")
            .attr("x", "0%")
            .attr("y", "0%")
            .attr("height", "100%")
            .attr("width", "100%")
            .attr("viewBox", "0 0 115 115")
            .append("image")
            .attr("x", "0")
            .attr("y", "0")
            .attr("width", "100")
            .attr("height", "100")
            .attr("xlink:href", "img/capsule_grey.svg");
        d3.select(".circle")
            .append("defs")
            .append("pattern")
            .attr("id", "medicineTube")
            .attr("x", "0%")
            .attr("y", "0%")
            .attr("height", "100%")
            .attr("width", "100%")
            .attr("viewBox", "0 0 100 100")
            .append("image")
            .attr("x", "0")
            .attr("y", "0")
            .attr("width", "100")
            .attr("height", "100")
            .attr("xlink:href", "img/Medicine_Drugs.svg");

        d3.select(".circle")
            .append("rect")
            .attr('class', 'Tube')
            .attr('x', 0)
            .attr('y', e * 200)
            .attr("width", 200)
            .attr("height", 180)
            .attr("fill", "url(#medicineTube)");


		//erstellen von einzelnen Punkten (pro 0.1g des Wirkstoffes 1 Pkt)
            for (var i = 0; i < Pkte && i < ((e + 1) * PunkteproTube); i = i + 1) {
                d3.select(".circle")
                    .append("circle")
                    .attr('class', 'circle')
                    .attr("cx", (i % (PunkteproTube / 2)) * 60 + padding)
                    .attr("cy", Math.floor(i / (PunkteproTube / 2)) * 50 + 75 + Math.floor(i / PunkteproTube) * 100)
                    .attr("r", (radius / 100 * h / 0.5))
                    .attr("fill", "url(#capsule)");
                ;
		}
	}
}

/**
 * Funktion um die Dosis mit der Gesamten Bevölkerung zu vergleichen
 * @param {Number}    Menge    gewählte Dosis (g/ Jahr)
 */
function visMenge(Menge) {

	//Alte Elemente in canvas 1_1 leeren
	document.getElementById('canvas1_1').innerHTML = '';

	//Skalen definieren
	var xScale = d3.scaleLinear()
		.domain([1, 0])
		.range([285, 75]);

	var yScale = d3.scaleLinear()
		.domain([0, wirkstoff.Anwendung[1]])
		.range([285, 15]);

	//Achsen definieren
	var xAxis = d3.axisBottom()
		.scale(xScale)
		.ticks(0);

	var yAxis = d3.axisLeft()
		.scale(yScale)
		.ticks(10);

	//SVG definieren
	var svg = d3.select("#canvas1_1")
		.append("svg")
		.attr("id", "vergleichbev")
		.attr("viewBox", "0 0 300 300");

	//Bar Element zeichnen
	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "vergleichbevbar")
		.attr("x", 120)
		.attr("y", (15 + 270 - (270 / wirkstoff.Anwendung[1] * Menge)))
		.attr("height", (270 / wirkstoff.Anwendung[1] * Menge))
		.attr("width", 100);

	//Durchschnitt hinzufügen (als Linie)
	svg.append("g")
		.append("line")
		.attr("class", "durchschnitt")
		.attr("x1", 75)
		.attr("y1", (15 + 270 - 270 / wirkstoff.Anwendung[1] * durchschnitt))
		.attr("x2", 285)
		.attr("y2", (15 + 270 - 270 / wirkstoff.Anwendung[1] * durchschnitt));

	//Text hinzufügen
	svg.select("g")
		.append("text")
		.attr("class", "durchschnitttext")
		.attr("x", 0)
		.attr("y", -10)
		.attr("dy", "0.5em")
		.attr("transform", "translate(" + 285 + "," + (15 + 270 - 270 / wirkstoff.Anwendung[1] * durchschnitt) + ")")
		.attr("text-anchor", "end")
		.attr("font-size", 10)
		.attr("font-family", "sans-serif")
		.text("Durchschnitt");

	//X-Achse kreieren
	svg.append("g")
		.attr("class", "axis")
		.attr("transform", "translate(0,285)")
		.call(xAxis);

	//Y-Achse kreieren
	svg.append("g")
		.attr("id", "axisymg")
		.attr("class", "axis")
		.attr("transform", "translate(75, 0)")
		.call(yAxis)
		.append("text")
		.attr("fill", "#000")
		.attr("x", -9)
		.attr("y", 0.5)
		.attr("dy", "0.2em")
		.attr("transform", "rotate(-90 0 0) translate(-5, 10)")
		.style("text-anchor", "end")
		.style("text-anchor", "end")
		.text("Menge pro Jahr");

	//Legendendefinition
	var legend = svg.append("g")
		.attr("font-size", 10)
		.attr("transform", "translate(225, 0)");

	legend.append("rect")
		.attr("class", "legend")
		.attr("x", 0)
		.attr("y", 0)
		.attr("fill", "white")
		.attr("stroke", "black")
		.attr("height", 25)
		.attr("width", 75);

	legend.append("rect")
		.attr("class", "vergleichbevbar")
		.attr("x", 2.5)
		.attr("y", 17.5)
		.attr("height", 5)
		.attr("width", 5);

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 2.5)
		.attr("y", 10)
		.attr("style", "text-anchor: start; text-decoration: underline")
		.text("Legende");

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 10)
		.attr("y", 25)
		.attr("dy", "-0.2em")
		.attr("style", "text-anchor: start;")
		.text("Menge pro Jahr");

	//Achsentext um den String g/Jahr erweitern
	var a = document.getElementById('vergleichbev').getElementById('axisymg').getElementsByClassName('tick');
	for (var i = 0; i < a.length; i++) {
		a[i].getElementsByTagName('text')[0].innerHTML = a[i].getElementsByTagName('text')[0].innerHTML + "g/Jahr";
	}
}

/**
 * Funktion herauszugeben/ zu berechnen, welche Menge der Körper nicht abbaut
 * @param {Number}    Menge    gewählte Dosis (g/ Jahr)
 * @param {Object} Wirkstoff Wirkstofftyp mit all seinen Werten
 */
function berAbbKoerper(Wirkstoff, Menge) {
	koerpernichtabbau = (1 - Wirkstoff.Abbaukoerper) * Menge;

	return (koerpernichtabbau);
}

/**
 * Funktion zum darstellen welcher Anteil vom Körper abgebaut wird (symbolisch)
 * @param {Number}    Koerpernichtabbau    vom Körper nicht abgebaute Doseis (g/ Jahr)
 * @param {Object} Menge Wirkstofftyp mit all seinen Werten
 */
var KoerpernichtabbauGlobal;
var Punkte_nicht_abgGlobal;
function anzeigeAbbKoerper(Menge, Koerpernichtabbau) {
    var Pkte = Menge / dosispropunkt; //Anzahl dargestellter Punkte
	var maxVerdwert = gew.Verduennungswert;
	//maximaler Verdünnungswert
	for (var i = 0; i < Gewaesserlist.length; i++) {
		if (maxVerdwert < Gewaesserlist[i].Verduennungswert) {
			maxVerdwert = Gewaesserlist[i].Verduennungswert
		}
	}

	//Canvas2/ generation1 leeren
	document.getElementById('canvas2').innerHTML = '';
	document.getElementById('canvas2_1').innerHTML = '';
	document.getElementById('canvas2_2').innerHTML = '';

	//neue svg-Elemente kreieren
	d3.select("#canvas2")
		.append("svg")
		.attr("class", "abbau1")
		.attr("viewBox", "0 0 150 180");

	d3.select("#canvas2_1")
		.append("svg")
		.attr("class", "abbau2")
		.attr("viewBox", "-50 0 150 200");



	//Grundgerüst für vom Körper abgebaute Wirkstoffe
	d3.select("#canvas2")
		.append("p")
		.attr("id", "abbtext")
		.attr("class", "abbautext")
		.text("Abbau im Körper: " + (Math.round(( 1 - (Koerpernichtabbau / Menge)) * 100) ) + "%");

	/*d3.select(".abbau1")
		.append("polygon")
		.attr('class', 'Abbau')
		.attr('points', (100 + "," + 200 + " " + 50 + "," + 0 + " " + 0 + "," + 200))
		.attr("style", "fill:white;stroke:black;stroke-width:2;fill-opacity:1.0;stroke-opacity:1.0");*/
/*
	d3.select(".abbau1")
		.append("circle")
		.attr('class', 'Abbau')
		.attr('cx', 50)
		.attr('cy', 50)
		.attr('r', 50)
		.attr("style", "fill:white;stroke:black;stroke-width:2;fill-opacity:1.0;stroke-opacity:1.0");

	d3.select(".abbau1")
		.append("rect")
		.attr('class', 'Abbau')
		.attr('x', 25)
		.attr('y', 100)
		.attr("width", 50)
		.attr("height", 100)
		.attr("style", "fill:none;stroke:none;stroke-width:0;fill-opacity:0.0;stroke-opacity:0.0");*/
	//Grundgerüst für vom Körper nicht abgebaute Wirkstoffe
	d3.select("#canvas2_1")
		.append("p")
		.attr("id", "nabbtext")
		.attr("class", "abbautext")
		.text("Ins Abwasser: " + Math.round((Koerpernichtabbau / Menge * 100 )) + "%");

	d3.select(".abbau2")
		.append("rect")
		.attr('class', 'InUmlauf')
        .attr('x', 0)
        .attr('y', 100)
        .attr("width", 100)
        .attr("height", 100)
		.attr("style", "fill:none;stroke:none;stroke-width:5;fill-opacity:0.1;stroke-opacity:0.9");

	d3.select(".abbau2")
		.append("defs")
		.append("pattern")
		.attr("id", "image")
		.attr("x", "0%")
		.attr("y", "0%")
		.attr("height", "100%")
		.attr("width", "100%")
		.attr("viewBox", "0 0 100 100")
		.append("image")
		.attr("x", "0")
		.attr("y", "0")
		.attr("width", "100")
		.attr("height", "100")
		.attr("xlink:href", "img/abbau.png");

	d3.select(".abbau2")
		.append("rect")
		.attr('class', 'InUmlauf')
		.attr('x', 0)
		.attr('y', 100)
		.attr("width", 100)
		.attr("height", 100)
		.attr("fill", "url(#image)");


	/*d3.select(".abbau3")
		.append("defs")
		.append("pattern")
		.attr("id", "image2")
		.attr("x", "0%")
		.attr("y", "0%")
		.attr("height", "100%")
		.attr("width", "100%")
		.attr("viewBox", "0 0 400 100")
		.append("image")
		.attr("x", "0")
		.attr("y", "0")
		.attr("width", "400")
		.attr("height", "100")
		.attr("xlink:href", "img/welle.png");*/


	/*for (var imgheight = 0; imgheight < 100; i = imgheight = imgheight + 25) {
		d3.select(".abbau3")
			.append("rect")
			.attr('class', 'inclAbwasser')
			.attr('x', 0)
			.attr('y', imgheight)
			.attr("width", 100)
			.attr("height", 25)
			.attr("style", "fill:url(#image2)");
	}

	d3.select(".abbau3")
		.append("rect")
		.attr('class', 'inclAbwasser')
		.attr('x', 0)
		.attr('y', 0)
		.attr("width", 100)
		.attr("height", 100)
		.attr("style", "fill:none;stroke:black;stroke-width:5;fill-opacity:0.1;stroke-opacity:0.9");
*/
	//lokale Variablen zur Darstellung
	var e = 0;
	var Pkte_total = Math.round(Menge / dosispropunkt);
    var Pkte_totalNoround = Menge / dosispropunkt;
	var Pktenicht_abgeb = Math.round(Koerpernichtabbau / dosispropunkt);
    var Pktenicht_abgebNoround = Koerpernichtabbau / dosispropunkt;
    Punkte_nicht_abgGlobal = Koerpernichtabbau / dosispropunkt;
	var Pkte_abgeb = Pkte_totalNoround - Pktenicht_abgeb;
    KoerpernichtabbauGlobal = Pktenicht_abgeb;
	var Anz_horiz_Abbau = Math.ceil(Math.sqrt(1 / 2) * Math.sqrt(Pkte_abgeb));
	var Anz_vert_Abbau = Math.ceil(Pkte_abgeb / Anz_horiz_Abbau);
	var Abstand_horiz_Abbau = 50 / Anz_horiz_Abbau;
	var Abstand_vert_Abbau = 100 / Anz_vert_Abbau;

	//effektive abgebaute Wirkstoffe

        for (var i = 0; i < Pktenicht_abgeb; i = i + 1) {
            d3.select(".abbau1")
                .append("circle")
                .attr('class', 'Abbau')
                .attr("cx", (i % (PunkteproTube / 2)) * 13 + 10 )
                .attr("cy", Math.floor(i / (PunkteproTube / 2)) * 10 + 41 )
                .attr("r", (4 * radius))
                .attr("fill", "url(#capsule)");
        }
	for (var i = 0; i < Pkte_abgeb; i = i + 1) {
		d3.select(".abbau1")
			.append("circle")
			.attr('class', 'Abbau')
            .attr("cx", (i % Anz_horiz_Abbau) * Abstand_horiz_Abbau + 80)
            .attr("cy", (Math.floor(i / Anz_horiz_Abbau) ) * Abstand_vert_Abbau + 41 )
            .attr("r", (4 * radius))
            .attr("fill", "url(#capsuleGrey)");
	}
	//lokale Variablen zur Darstellung
	var Anz_horiz_nAbbau = Math.ceil(Math.sqrt(1 / 2) * Math.sqrt(Pktenicht_abgeb));
	var Anz_vert_nAbbau = Math.ceil(Pktenicht_abgeb / Anz_horiz_nAbbau);
	var Abstand_horiz_nAbbau = 50 / Anz_horiz_nAbbau;
	var Abstand_vert_nAbbau = 100 / Anz_vert_nAbbau;

	//effektive in Umwelt entlassene Wirkstoffe
	for (e; e < Pktenicht_abgeb; e = e + 1) {
		d3.select(".abbau2")
			.append("circle")
			.attr('class', 'nAbbau')
			.attr("cx", ((e % Anz_horiz_nAbbau) + 0.5 ) * Abstand_horiz_nAbbau )
			.attr("cy", (Math.floor(e / Anz_horiz_nAbbau) + 0.5) * Abstand_vert_nAbbau + 32)
			.attr("r", (4.5 * radius))
   		 	.attr("fill", "url(#capsule)");
	}

	//lokale Variablen zur Darstellung
	/*var Abstand = 100 / Math.sqrt(Pktenicht_abgeb);
	var Anz = Math.ceil(100 / Abstand);
	var Abstand_neu_horiz = 180 / Anz;
	var Anz_vert = Math.ceil(Pktenicht_abgeb / Anz);
	var Abstand_neu_vert = 380 / Anz_vert;

	var u = 0;

	//Wirkstoffe pro 300000l Abwasser
	for (u; u < Pktenicht_abgeb; u = u + 1) {
		d3.select(".abbau3")
			.append("circle")
			.attr('class', 'nAbbau')
			.attr("cx", u % Anz * Abstand_neu_horiz + Abstand_neu_horiz + 150 )
			.attr("cy", Math.floor(u / (Anz)) * Abstand_neu_vert + 200  )
            .attr("r", (17 * radius))
            .attr("fill", "url(#capsule)");
	}*/

}

/**
 * Funktion zur Darstellung der im Körper abgebauten Stoffe (zahlenmässig/ dimensionsmässig)
 * @param    {Number} Menge    Dosis (g/Jahr)
 * @param {Number} Koerpernichtabbau  Im Körper nicht abgebauter Anteil der Stoffe
 */
function visAbbKoerper(Menge, Koerpernichtabbau) {

	//canvas 2_1 leeren
	document.getElementById('canvas2_3').innerHTML = '';

	//Differenz der Skalenhöhe (damit diese Nachbarskala entspricht)
	var hoehendif = 270 * (1 - Menge / wirkstoff.Anwendung[1])

	//Skalen definieren
	var xScale = d3.scaleLinear()
		.domain([1, 0])
		.range([230, 40]);

	var yScaleleft = d3.scaleLinear()
		.domain([0, 100])
		.range([285, (15 + hoehendif)]);

	var yScaleright = d3.scaleLinear()
		.domain([0, Menge])
		.range([285, (15 + hoehendif)]);

	//Achsen definieren
	var xAxis = d3.axisBottom()
		.scale(xScale)
		.ticks(0);

	var yAxisleft = d3.axisLeft()
		.scale(yScaleleft)
		.ticks(5);

	var yAxisright = d3.axisRight()
		.scale(yScaleright)
		.ticks(Math.ceil(Menge * 2));

	//svg erstellen
	var svg = d3.select("#canvas2_3")
		.append("svg")
		.attr("id", "nicht_abgebaut")
		.attr("viewBox", "0 0 300 300");

	//bar's und beschreibende Texte erstellen
	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "vomKoerpernicht_abgebautbar")
		.attr("x", 85)
		.attr("y", (15 + hoehendif + (Menge / wirkstoff.Anwendung[1]) * (270 - (270 * (Koerpernichtabbau / Menge)))))
		.attr("height", ((Menge / wirkstoff.Anwendung[1]) * (270 - (270 * (1 - (Koerpernichtabbau / Menge))))/*(270 * abbaukoerper)*/))
		.attr("width", 100);

	/*svg.selectAll("bar")
		.data([1])
		.enter()
		.append("text")
		.attr("class", "vomKoerpernicht_abgebautbartext")
		.attr("text-anchor", "middle")
		.attr("alignment-baseline", "hanging")
		.attr("font-size", 8)
		.attr("x", 85 + 50)
		.attr("dy", "0.32em")
		.attr("y", (15 + 270 - (270 * (Koerpernichtabbau / Menge))))
		.text("Ins Abwasser: " + Math.round((Koerpernichtabbau / Menge) * 100) + "%");*/

	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "vomKoerper_abgebautbar")
		.attr("x", 85)
		.attr("y", (15 + hoehendif))
		.attr("height", ((Menge / wirkstoff.Anwendung[1]) * (270 * (1 - (Koerpernichtabbau / Menge)))))
		.attr("width", 100);

	/*svg.selectAll("bar")
		.data([1])
		.enter()
		.append("text")
		.attr("class", "vomKoerpernicht_abgebautbartext")
		.attr("text-anchor", "middle")
		.attr("alignment-baseline", "hanging")
		.attr("font-size", 8)
		.attr("x", 85 + 50)
		.attr("dy", "0.32em")
		.attr("y", 15)
		.text("Abbau im Körper: " + Math.round((1 - (Koerpernichtabbau / Menge)) * 100) + "%");*/

		var b = "";
		if(dosis < (0.5* (wirkstoff.Anwendung[0] + wirkstoff.Anwendung[1]))) {
			b = "start"
		}
		else {
			b = "end"
		}

	//Achsen definieren und Beschriftung anpassen
	svg.append("g")
		.attr("id", "axisbottom")
		.attr("class", "axis")
		.attr("transform", "translate(0,285)")
		.call(xAxis);

	svg.append("g")
		.attr("id", "axisleft")
		.attr("class", "axis")
		.attr("transform", "translate(40, 0)")
		.call(yAxisleft)
		.append("text")
		.attr("fill", "#000")
		.attr("x", -9)
		.attr("y", 0.5)
		.attr("dy", "0.2em")
		.attr("transform", "rotate(-90 0 0) translate(" + (-9 - hoehendif ) + ", 10)")
		.style("text-anchor", b)
		.text("prozentualer Abbau");

	var a = document.getElementById('nicht_abgebaut').getElementById('axisleft').getElementsByClassName('tick');
	for (var i = 0; i < a.length; i++) {
		a[i].getElementsByTagName('text')[0].innerHTML = a[i].getElementsByTagName('text')[0].innerHTML + "%";
	}

	svg.append("g")
		.attr("id", "axisright")
		.attr("class", "axis")
		.attr("transform", "translate(230, 0)")
		.call(yAxisright)
		.append("text")
		.attr("fill", "#000")
		.attr("x", -9)
		.attr("y", 0.5)
		.attr("dy", "0.2em")
		.attr("transform", "rotate(-90 0 0) translate(" + (-9 - hoehendif ) + ", -10)")
		.style("text-anchor", b)
		.text("effektiver Abbau");

	//Legendendefinition
	var legend = svg.append("g")
		.attr("font-size", 10)
		.attr("transform", "translate(225, 0)");

	legend.append("rect")
		.attr("class", "legend")
		.attr("x", 0)
		.attr("y", 0)
		.attr("fill", "white")
		.attr("stroke", "black")
		.attr("height", 35)
		.attr("width", 75);

	legend.append("rect")
		.attr("class", "vomKoerper_abgebautbar")
		.attr("x", 2.5)
		.attr("y", 17.5)
		.attr("height", 5)
		.attr("width", 5);

	legend.append("rect")
		.attr("class", "vomKoerpernich_abgebautbar")
		.attr("x", 2.5)
		.attr("y", 27.5)
		.attr("height", 5)
		.attr("width", 5);

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 2.5)
		.attr("y", 10)
		.attr("style", "text-anchor: start; text-decoration: underline")
		.text("Legende");

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 10)
		.attr("y", 25)
		.attr("dy", "-0.2em")
		.attr("style", "text-anchor: start;")
		.text("Abgebaut");

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 10)
		.attr("y", 35)
		.attr("dy", "-0.2em")
		.attr("style", "text-anchor: start;")
		.text("Nicht abgebaut");

	var a2 = document.getElementById('nicht_abgebaut').getElementById('axisright').getElementsByClassName('tick');
	for (var k = 0; k < a2.length; k++) {
		a2[k].getElementsByTagName('text')[0].innerHTML = a2[k].getElementsByTagName('text')[0].innerHTML + " g/Jahr";
	}
}

/**
 * Funktion zur Berechnung der Menge pro l
 * @param    {Number} Vermabw    Anzahl l Abwasser
 * @param {Number} Koerpernichtabbau  Im Körper nicht abgebauter Anteil der Stoffe
 */
function vermAbw(Koerpernichtabbau, Vermabw) {
	imabw = Koerpernichtabbau / Vermabw;

	return (imabw);
}

/**
 * Funktion zur Darstellung der im Abwasser vorhnandenen Stoffe (zahlenmässig/ dimensionsmässig)
 * @param    {Number} Imabwasser    Dosis pro hl-Abwasser
 * @param {Number} Wirkstoff  Im Körper nicht abgebauter Anteil der Stoffe
 */
function visAbw(Imabwasser, Wirkstoff) {

	//suche maximum im Abwasser
	var maximAbw = (1 - Wirkstoff["Abbaukoerper"]) * wirkstoff.Anwendung[1] / vermabwasser;

	//canvas leeren
	document.getElementById('canvas2_4').innerHTML = '';

	//Achsen definieren
	var xScale = d3.scaleLinear()
		.domain([1, 0])
		.range([285, 65]);

	var yScale = d3.scaleLinear()
		.domain([0, maximAbw * 1000])
		.range([285, 15]);

	var xAxis = d3.axisBottom()
		.scale(xScale)
		.ticks(0);

	var yAxis = d3.axisLeft()
		.scale(yScale)
		.ticks(10);

	//svg erstellen
	var svg = d3.select("#canvas2_4")
		.append("svg")
		.attr("id", "imabwasser")
		.attr("viewBox", "0 0 300 300");

	//Bar Element definieren
	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "imabwasser")
		.attr("x", 115)
		.attr("y", (15 + 270 - (270 * Imabwasser / maximAbw)))
		.attr("height", (270 * Imabwasser / maximAbw))
		.attr("width", 100);

	//Create X axis
	svg.append("g")
		.attr("class", "axis")
		.attr("transform", "translate(0,285)")
		.call(xAxis);

	//Create Y axis
	svg.append("g")
		.attr("id", "axisy")
		.attr("class", "axis")
		.attr("transform", "translate(65, 0)")
		.call(yAxis)
		.append("text")
		.attr("fill", "#000")
		.attr("x", -9)
		.attr("y", 0.5)
		.attr("dy", "0.2em")
		.attr("transform", "rotate(-90 0 0) translate(-10, 10)")
		.style("text-anchor", "end")
		.style("text-anchor", "end")
		.text("Menge pro Liter Abwasser");

	//Legendendefinition
	var legend = svg.append("g")
		.attr("font-size", 10)
		.attr("transform", "translate(225, 0)");

	legend.append("rect")
		.attr("class", "legend")
		.attr("x", 0)
		.attr("y", 0)
		.attr("fill", "white")
		.attr("stroke", "black")
		.attr("height", 25)
		.attr("width", 75);

	legend.append("rect")
		.attr("class", "imabwasser")
		.attr("x", 2.5)
		.attr("y", 17.5)
		.attr("height", 5)
		.attr("width", 5);

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 2.5)
		.attr("y", 10)
		.attr("style", "text-anchor: start; text-decoration: underline")
		.text("Legende");

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 10)
		.attr("y", 25)
		.attr("dy", "-0.2em")
		.attr("style", "text-anchor: start;")
		.text("Im Abwasser");

	//Text ändern
	var a = document.getElementById('imabwasser').getElementById('axisy').getElementsByClassName('tick');
	for (var i = 0; i < a.length; i++) {
		a[i].getElementsByTagName('text')[0].innerHTML = a[i].getElementsByTagName('text')[0].innerHTML + "mg/l";
	}
}

/**
 * Funktion zur Berechnung der Menge nach dem Durchgang einer Kläranlage
 * @param    {Number} ImAbw    ImAbwasser vor Kläranlage vorhandene Stoffe
 * @param {String} Anlage  Anlagetyp
 * @param {Object} Wirkstoff gewählter Wirkstoff mit all seinen Attributen
 */
function abbauAnlage(ImAbw, Anlage, Wirkstoff) {
	nachanl = ImAbw * (1 - Wirkstoff[Anlage.Klaeranlagename]);

	return (nachanl);
}

/**
 * Funktion zur Darstellung der in der Kläranlage abgebauten Stoffe (zahlenmässig/ dimensionsmässig)
 * @param    {Number} ImAbwasser    Im Abwasser vor Kläranlage vorhandenen Stoffe
 * @param {Number} Nachanlage  Im Abwasser nach Kläranlagedurchlauf vorhandenen Stoffe
 */
function visAbbAnlage(ImAbwasser, Nachanlage) {

	//suche maximum im Abwasser
	var maximAbw = (1 - wirkstoff["Abbaukoerper"]) * wirkstoff.Anwendung[1] / vermabwasser;

	//Höhendifferenz Skala (anpassen an Nachbarskala)
	var hoehendif = 270 * (1 - (ImAbwasser / maximAbw));

	//canvas 4_1 leeren
	document.getElementById('canvas3_1').innerHTML = '';

	//Skalen definieren
	var xScale = d3.scaleLinear()
		.domain([1, 0])
		.range([230, 40]);

	var yScaleleft = d3.scaleLinear()
		.domain([0, 100])
		.range([285, (15 + hoehendif)]);

	var yScaleright = d3.scaleLinear()
		.domain([0, ImAbwasser * 1000])
		.range([285, (15 + hoehendif)]);

	//Achsen definieren
	var xAxis = d3.axisBottom()
		.scale(xScale)
		.ticks(0);

	var yAxisleft = d3.axisLeft()
		.scale(yScaleleft)
		.ticks(5);

	var yAxisright = d3.axisRight()
		.scale(yScaleright)
		.ticks(Math.ceil(ImAbwasser * 2000));

	//svg erstellen
	var svg = d3.select("#canvas3_1")
		.append("svg")
		.attr("id", "Anlnicht_abgebaut")
		.attr("viewBox", "0 0 300 300");

	//bar's und beschreibende Texte erstellen
	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "vonAnlagenicht_abgebautbar")
		.attr("x", 85)
		.attr("y", (15 + hoehendif + (ImAbwasser / maximAbw) * (270 - (270 * (Nachanlage / ImAbwasser)))))
		.attr("height", ((ImAbwasser / maximAbw) * (270 - (270 * (1 - (Nachanlage / ImAbwasser)))))/*(270 * abbaukoerper)*/)
		.attr("width", 100);

	/*svg.selectAll("bar")
		.data([1])
		.enter()
		.append("text")
		.attr("class", "vonAnlagenicht_abgebautbartext")
		.attr("text-anchor", "middle")
		.attr("alignment-baseline", "hanging")
		.attr("font-size", 8)
		.attr("x", 85 + 50)
		.attr("dy", "0.32em")
		.attr("y", (15 + 270 - (270 * (Nachanlage / ImAbwasser))))
		.text("Ins Gewässer: " + Math.round((Nachanlage / ImAbwasser) * 100) + "%");*/

	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "vonAnlage_abgebautbar")
		.attr("x", 85)
		.attr("y", 15 + hoehendif)
		.attr("height", ((ImAbwasser / maximAbw) * (270 * (1 - (Nachanlage / ImAbwasser)))))
		.attr("width", 100);

	/*if (Math.round(Nachanlage / ImAbwasser * 100) == 100) {
		//nichts geschieht
	}
	else {
		svg.selectAll("bar")
			.data([1])
			.enter()
			.append("text")
			.attr("class", "vonAnlagenicht_abgebautbartext")
			.attr("text-anchor", "middle")
			.attr("alignment-baseline", "hanging")
			.attr("font-size", 8)
			.attr("x", 85 + 50)
			.attr("dy", "0.32em")
			.attr("y", 15)
			.text("Abbau in der Anlage: " + Math.round((1 - (Nachanlage / ImAbwasser)) * 100) + "%");
	}*/

	//Achsen definieren und Beschriftung anpassen
	svg.append("g")
		.attr("id", "axisbottom")
		.attr("class", "axis")
		.attr("transform", "translate(0,285)")
		.call(xAxis);

	//Hilfsvariable a
	var b = "";
	if(dosis < (0.5* (wirkstoff.Anwendung[0] + wirkstoff.Anwendung[1]))) {
		b = "start"
	}
	else {
		b = "end"
	}

	svg.append("g")
		.attr("id", "axisleft")
		.attr("class", "axis")
		.attr("transform", "translate(40, 0)")
		.call(yAxisleft)
		.append("text")
		.attr("fill", "#000")
		.attr("x", -9)
		.attr("y", 0.5)
		.attr("dy", "0.2em")
		.attr("transform", "rotate(-90 0 0) translate(" + (-9 - hoehendif ) + ", 10)")
		.style("text-anchor", b)
		.text("prozentualer Abbau");

	var a = document.getElementById('Anlnicht_abgebaut').getElementById('axisleft').getElementsByClassName('tick');
	for (var i = 0; i < a.length; i++) {
		a[i].getElementsByTagName('text')[0].innerHTML = a[i].getElementsByTagName('text')[0].innerHTML + "%";
	}

	svg.append("g")
		.attr("id", "axisright")
		.attr("class", "axis")
		.attr("transform", "translate(230, 0)")
		.call(yAxisright)
		.append("text")
		.attr("fill", "#000")
		.attr("x", -9)
		.attr("y", 0.5)
		.attr("dy", "0.2em")
		.attr("transform", "rotate(-90 0 0) translate(" + (-9 - hoehendif ) + ", -10)")
		.style("text-anchor", b)
		.text("effektiver Abbau");

		//Legendendefinition
		var legend = svg.append("g")
			.attr("font-size", 10)
			.attr("transform", "translate(225, 0)");

		legend.append("rect")
			.attr("class", "legend")
			.attr("x", 0)
			.attr("y", 0)
			.attr("fill", "white")
			.attr("stroke", "black")
			.attr("height", 35)
			.attr("width", 75);

		legend.append("rect")
			.attr("class", "vonAnlage_abgebautbar")
			.attr("x", 2.5)
			.attr("y", 17.5)
			.attr("height", 5)
			.attr("width", 5);

		legend.append("rect")
			.attr("class", "vonAnlagenicht_abgebautbar")
			.attr("x", 2.5)
			.attr("y", 27.5)
			.attr("height", 5)
			.attr("width", 5);

		legend.append("text")
			.attr("fill", "#000")
			.attr("x", 2.5)
			.attr("y", 10)
			.attr("style", "text-anchor: start; text-decoration: underline")
			.text("Legende");

		legend.append("text")
			.attr("fill", "#000")
			.attr("x", 10)
			.attr("y", 25)
			.attr("dy", "-0.2em")
			.attr("style", "text-anchor: start;")
			.text("Abgebaut");

		legend.append("text")
			.attr("fill", "#000")
			.attr("x", 10)
			.attr("y", 35)
			.attr("dy", "-0.2em")
			.attr("style", "text-anchor: start;")
			.text("Nicht abgebaut");

	var a2 = document.getElementById('Anlnicht_abgebaut').getElementById('axisright').getElementsByClassName('tick');
	for (var k = 0; k < a2.length; k++) {
		a2[k].getElementsByTagName('text')[0].innerHTML = a2[k].getElementsByTagName('text')[0].innerHTML + "mg";
	}
}

/**
 * Funktion zur Berechnung der im Gewässer vorhandenen Stoffe
 * @param    {Number} Nachanl    Dosis nach Kläranlagedurchlauf
 * @param {Object} Gewaesser  gewähltes Gewässer mit all seinen Eigenschaften
 */
function vermGew(Nachanl, Gewaesser) {
	restwert = Nachanl / Gewaesser.Verduennungswert;

	return (restwert);
}

/**
 * Funktion zur Darstellung der im vorhandenen Stoffe und Vergleich mit dem Grenzwert
 * @param    {Number} Restwert   Dosis im Gewässer (pro Hektare)
 * @param {Object} Wirkstoff  gewählter Wirkstoff mit all seinen Eigenschaften
 * @param {Number} Darstw  dargestellet Wassermenge
 */
function anzeigeRest(Restwert, Wirkstoff, Darstw) {

	//canvas leeren
	document.getElementById('canvas4').innerHTML = '';
    document.getElementById('canvas2_2').innerHTML = '';
	//Fischbild und Resultattext wählen
	var img4, verglstr, color;
	if ((Math.round(Wirkstoff.Grenzwert * Darstw / dosispropunkt)) < (Math.round(Restwert * Darstw / dosispropunkt))) {
		img4 = img[0];
		verglstr = resulttext[0];
		color = '#b74f46';
	}
	else if ((Math.round(Wirkstoff.Grenzwert * Darstw / dosispropunkt)) == (Math.round(Restwert * Darstw / dosispropunkt))) {
		img4 = img[1];
		verglstr = resulttext[1];
        color = '#b48b02';
	}
	else {
		img4 = img[2];
		verglstr = resulttext[2];
        color = '#056b24';
	}

	var maxVerdwert = gew.Verduennungswert;
	//maximaler Verdünnungswert
	for (var i = 0; i < Gewaesserlist.length; i++) {
		if (maxVerdwert < Gewaesserlist[i].Verduennungswert) {
			maxVerdwert = Gewaesserlist[i].Verduennungswert
		}
	}

	//canvas neu aufbauen (incl Texte)


    d3.select("#canvas2_2")
        .append("svg")
        .attr("class", "abbau3")
        .attr("viewBox", "0 0 " + Math.round(100 * Math.sqrt(maxVerdwert)) + " " + Math.round(100 * Math.sqrt(maxVerdwert)));
    //Grundgerüst für vom Körper nicht abgebaute Wirkstoffe pro 300l Abwasser
    d3.select("#canvas2_2")
        .append("p")
        .attr("id", "vermtext")
        .attr("class", "abbautext")
        .text("Abbau in der Kläranlage");

	d3.select("#canvas4")
		.append("svg")
		.attr("class", "vergleich1")
		.attr("viewBox", "0 0 " + Math.round(100 * Math.sqrt(maxVerdwert / gew.Verduennungswert)) + " " + Math.round(100 * Math.sqrt(maxVerdwert / gew.Verduennungswert)));

	d3.select("#canvas4")
		.append("h2")
		.attr("id", "modtext")
		.attr("class", "vergltext")
		.text("Modell");

	d3.select("#canvas4")
		.append("svg")
		.attr("class", "vergleich2")
		.attr("viewBox", "0 0 " + Math.round(100 * Math.sqrt(maxVerdwert / gew.Verduennungswert)) + " " + Math.round(100 * Math.sqrt(maxVerdwert / gew.Verduennungswert)));

	d3.select("#canvas4")
		.append("p")
		.attr("id", "grzwtext")
		.attr("class", "vergltext")
		.text("Grenzwert (pro " + Darstw + "l)");

	d3.select("#canvas4")
		.append("svg")
		.attr("class", "vergleich3")
		.attr("viewBox", "0 0 100 200");

	d3.select("#canvas4")
		.append("h1")
		.attr("id", "vergltext")
		.attr("class", "vergltext")
        .style('color', color)
		.html(verglstr);

	//Grundgerüst für Modell
	d3.select(".vergleich1")
		.append("defs")
		.append("pattern")
		.attr("id", "image3")
		.attr("x", "0%")
		.attr("y", "0%")
		.attr("height", "100%")
		.attr("width", "100%")
		.attr("viewBox", "0 0 400 400")
		.append("image")
		.attr("x", "0")
		.attr("y", "0")
		.attr("width", "400")
		.attr("height", "400")
		.attr("xlink:href", "img/welle.png");

	d3.select(".vergleich2")
		.append("defs")
		.append("pattern")
		.attr("id", "image3")
		.attr("x", "0%")
		.attr("y", "0%")
		.attr("height", "100%")
		.attr("width", "100%")
		.attr("viewBox", "0 0 400 400")
		.append("image")
		.attr("x", "0")
		.attr("y", "0")
		.attr("width", "200")
		.attr("height", "200")
		.attr("xlink:href", "img/welle.png");

	d3.select(".vergleich3")
		.append("defs")
		.append("pattern")
		.attr("id", "image4")
		.attr("x", "0%")
		.attr("y", "0%")
		.attr("height", "100%")
		.attr("width", "100%")
		.attr("viewBox", "0 0 200 200")
		.append("image")
		.attr("x", "0")
		.attr("y", "0")
		.attr("width", "200")
		.attr("height", "200")
		.attr("xlink:href", img4);


	for (var imgheight = 0; imgheight < 400; imgheight = imgheight + 400) {
		d3.select(".vergleich1")
			.append("rect")
			.attr('class', 'inclAbwasser')
			.attr('x', 0)
			.attr('y', 0)
			.attr("width", 100)
			.attr("height", 100)
			.attr("style", "fill:url(#image3)");

		d3.select(".vergleich2")
			.append("rect")
			.attr('class', 'inclAbwasser')
			.attr('x', 0)
			.attr('y', 0)
			.attr("width", 100)
			.attr("height", 100)
			.attr("style", "fill:url(#image3)");
	}

	d3.select(".vergleich1")
		.append("rect")
		.attr('class', 'Modell')
		.attr('x', 0)
		.attr('y', 0)
		.attr("width", 100)
		.attr("height", 100)
		.attr("style", "fill:none;stroke:#5c7697;stroke-width:4;fill-opacity:0.1;stroke-opacity:0.9");

	//Grundgerüst für Grenzwert
	d3.select(".vergleich2")
		.append("rect")
		.attr('class', 'Grenzwert')
		.attr('x', 0)
		.attr('y', 0)
		.attr("width", 100)
		.attr("height", 100)
		.attr("style", "fill:none;stroke:#e2dcd7;stroke-width:4;fill-opacity:0.1;stroke-opacity:0.9");

	//Hilfsvariablen für die Punktanzahl incl Punktabbildung
	var Pktenicht_abgeb = Math.round(Restwert * Darstw / dosispropunkt);
	var Abstand = 100 / Math.sqrt(Pktenicht_abgeb);
	var Anz = Math.ceil(100 / Abstand);
	var Abstand_neu_horiz = 100 / Anz;
	var Anz_vert = Math.ceil(Pktenicht_abgeb / Anz);
	var Abstand_neu_vert = 100 / Anz_vert;
	var u = 0;
	for (u; u < Pktenicht_abgeb; u = u + 1) {
		d3.select(".vergleich1")
			.append("circle")
			.attr('class', 'Modell')
			.attr("cx", u % Anz * Abstand_neu_horiz + Abstand_neu_horiz / 2)
			.attr("cy", Math.floor(u / (Anz)) * Abstand_neu_vert + Abstand_neu_vert / 2)
			.attr("r", (radius * Math.round(Math.sqrt(maxVerdwert / gew.Verduennungswert) * 100) / 40))
            .attr("fill", "url(#capsule)");
	}
    for (u; u < Pktenicht_abgeb; u = u + 1) {

        d3.select(".abbau3")
            .append("circle")
            .attr('class', 'Modell')
            .attr("cx", (u % (PunkteproTube / 2)) * 25 + 20 )
            .attr("cy", Math.floor(u / (PunkteproTube / 2)) * 15 + 75 )
            .attr("r", (radius * 7))
            .attr("fill", "url(#capsule)");
    }
    var u = 0;
    if (Anz == 0){
        Anz = 1;
        Abstand_neu_horiz = 20;
        Abstand_neu_vert = 30;
    }
    for (u; u < KoerpernichtabbauGlobal - Pktenicht_abgeb; u = u + 1) {
        d3.select(".abbau3")
            .append("circle")
            .attr('class', 'Modell')
            .attr("cx", (u % (PunkteproTube / 2)) * 25 + 150 )
            .attr("cy", Math.floor(u / (PunkteproTube / 2)) * 15 + 75 )
            .attr("r", (radius * 7))
            .attr("fill", "url(#capsuleGrey)");
    }

    var u = 0;

    for (u; u < Pktenicht_abgeb; u = u + 1) {

        d3.select(".abbau3")
            .append("circle")
            .attr('class', 'Modell')
            .attr("cx", (u % (PunkteproTube / 2)) * 25 + 20 )
            .attr("cy", Math.floor(u / (PunkteproTube / 2)) * 15 + 75 )
            .attr("r", (radius * 7))
            .attr("fill", "url(#capsule)");
    }
	Pktenicht_abgeb = Math.round(Wirkstoff.Grenzwert * Darstw / dosispropunkt);
	Abstand = 100 / Math.sqrt(Pktenicht_abgeb);
	Anz = Math.ceil(100 / Abstand);
	Abstand_neu_horiz = 100 / Anz;
	Anz_vert = Math.ceil(Pktenicht_abgeb / Anz);
	Abstand_neu_vert = 100 / Anz_vert;

	u = 0;

	for (u; u < Pktenicht_abgeb; u = u + 1) {
		d3.select(".vergleich2")
			.append("circle")
			.attr('class', 'Grenzwert')
			.attr("cx", u % Anz * Abstand_neu_horiz + Abstand_neu_horiz / 2)
			.attr("cy", Math.floor(u / (Anz)) * Abstand_neu_vert + Abstand_neu_vert / 2)
			.attr("r", (radius * Math.round(Math.sqrt(maxVerdwert / gew.Verduennungswert) * 100) / 40))
            .attr("fill", "url(#capsule)");

	}

	d3.select(".vergleich3")
		.append("rect")
		.attr('class', 'inclAbwasser')
		.attr('x', 0)
		.attr('y', 0)
		.attr("width", 200)
		.attr("height", 100)
		.attr("style", "fill:url(#image4)")

}

/**
 * Funktion zur Darstellung der im Gewässer noch vorhandener Stoffe (zahlenmässig/ dimensionsmässig)
 * @param    {Number} Menge    Dosis (g/Jahr)
 * @param {Number} Restwert  Restwert im Gewässer
 * @param {Object} Wirkstoff  gewählter Wirkstoff mit all seinen Eigenschaften
 */
function visRest(Menge, Restwert, Wirkstoff) {

	//suche minimalen Gewässerverdünnungswert
	var minverdW = Gewaesserlist[0].Verduennungswert;

	for (var i = 0; i < Gewaesserlist.length; i++) {

		if (Gewaesserlist[i].Verduennungswert < minverdW) {
			minverdW = Gewaesserlist[i].Verduennungswert;
		}
	}

	//maximale Dosis mit minimalem Verdünnungswert und ohne Anlage verrechnet.
	var ohneanlmitminverdW = wirkstoff.Anwendung[1] * (1 - wirkstoff.Abbaukoerper) / vermabwasser / minverdW;

	//maximaler Wert der Domain festlegen
	var maxWert_Domain;
	if (ohneanlmitminverdW < Wirkstoff.Grenzwert) {
		maxWert_Domain = Wirkstoff.Grenzwert;
	}
	else {
		maxWert_Domain = ohneanlmitminverdW;
	}

	//canvas leeren
	document.getElementById('canvas4_1').innerHTML = '';

	//Skalen und Achsen definieren
	var xScale = d3.scaleLinear()
		.domain([1, 0])
		.range([285, 65]);

	var yScale = d3.scaleLinear()
		.domain([0, maxWert_Domain * 1000000])
		.range([285, 15]);

	var xAxis = d3.axisBottom()
		.scale(xScale)
		.ticks(0);

	var yAxis = d3.axisLeft()
		.scale(yScale)
		.ticks(10);

	//svg definieren
	var svg = d3.select("#canvas4_1")
		.append("svg")
		.attr("id", "restwert")
		.attr("viewBox", "0 0 300 300");

	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "restwert")
		.attr("x", 115)
		.attr("y", (15 + 270 - (270 * Restwert / maxWert_Domain)))
		.attr("height", (270 * Restwert / maxWert_Domain))
		.attr("width", 100)
		.attr("data-legend", "Restwert");

	//Grenzwert hinzufügen
	svg.append("g")
		.append("line")
		.attr("class", "grenzwert")
		.attr("x1", 65)
		.attr("y1", (15 + 270 - (270 * Wirkstoff.Grenzwert / maxWert_Domain)))
		.attr("x2", 285)
		.attr("y2", (15 + 270 - (270 * Wirkstoff.Grenzwert / maxWert_Domain)));

	svg.select("g")
		.append("text")
		.attr("class", "grenzwerttext")
		.attr("x", 0)
		.attr("y", -10)
		.attr("dy", "0.5em")
		.attr("transform", "translate(" + 285 + "," + (15 + 270 - (270 * Wirkstoff.Grenzwert / maxWert_Domain)) + ")")
		.attr("text-anchor", "end")
		.attr("font-size", 10)
		.text("Grenzwert");

	//Create X axis
	svg.append("g")
		.attr("class", "axis")
		.attr("transform", "translate(0,285)")
		.call(xAxis);

	//Create Y axis
	svg.append("g")
		.attr("id", "axisy")
		.attr("class", "axis")
		.attr("transform", "translate(65, 0)")
		.call(yAxis)
		.append("text")
		.attr("fill", "#000")
		.attr("x", -9)
		.attr("y", 0.5)
		.attr("dy", "0.2em")
		.attr("transform", "rotate(-90 0 0) translate(-9, 10)")
		.style("text-anchor", "end")
		.style("text-anchor", "end")
		.text("Menge pro Liter Flusswasser");

	//Legendendefinition
	var legend = svg.append("g")
		.attr("font-size", 10)
		.attr("transform", "translate(250, 0)");

	legend.append("rect")
		.attr("class", "legend")
		.attr("x", 0)
		.attr("y", 0)
		.attr("fill", "white")
		.attr("stroke", "black")
		.attr("height", 25)
		.attr("width", 50);

	legend.append("rect")
		.attr("class", "restwert")
		.attr("x", 2.5)
		.attr("y", 17.5)
		.attr("height", 5)
		.attr("width", 5);

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 2.5)
		.attr("y", 10)
		.attr("style", "text-anchor: start; text-decoration: underline")
		.text("Legende");

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 10)
		.attr("y", 25)
		.attr("dy", "-0.2em")
		.attr("style", "text-anchor: start;")
		.text("Restwert");

	//Text anpassen
	var a = document.getElementById('restwert').getElementById('axisy').getElementsByClassName('tick');
	for (var k = 0; k < a.length; k++) {
		a[k].getElementsByTagName('text')[0].innerHTML = a[k].getElementsByTagName('text')[0].innerHTML + "µg/l";
	}
}
