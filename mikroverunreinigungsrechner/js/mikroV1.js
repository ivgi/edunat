/**
 * Test Function
 */

//Globale Variablen
//Wirkstoffe
var Wirkstofflist = [{
	Wirkstoffname: "Diclofenac",
	Wirkstoffklasse: "wirkstoff",
	Beschreibung: "Diclofenac ist ein schmerzlindernder und entzündungshemmender Wirkstoff, der z.B. bei Sportverletzungen als Gel seine Anwendung findet.",
	Abbaukoerper: 0.3,
	ohne: 0,
	ohnefilter: 0.25,
	mitfilter: 0.95,
	Grenzwert: 0.0000001, //Grenzwert g/l
	Durchschnitt: 1.1,  //Durchschnittsverbrauch der Bevölkerung in Einheit der Anwendung,
	GrammproPackung: 1,
	Packungsart: "Tube",
	Anwendung: [0, 10, 0.1] //min-, max-Wert sowie step in der Einheit (Tube/ Packung )/Jahr
}, {
	Wirkstoffname: "Paracetamol",
	Wirkstoffklasse: "wirkstoff",
	Beschreibung: "Paracetamol ist ein Wirkstoff schmerzlindernden Eigenschaften, der z.B. in Tablettenform zur Behandlung von Kopf- und  Gliederschmerzen eingesetzt wird.",
	Abbaukoerper: 0.95,
	ohne: 0,
	ohnefilter: 0.9,
	mitfilter: 0.98,
	Grenzwert: 0.0000001,
	Durchschnitt: 1.1,
	GrammproPackung: 1,
	Packungsart: "Packung",
	Anwendung: [0, 5, 0.1] //min-, max-Wert sowie step in der Einheit (Tube/ Packung )/Jahr
}, {
	Wirkstoffname: "Ibuprofen",
	Wirkstoffklasse: "wirkstoff",
	Beschreibung: "Ibuprofen ist ein Stoff mit fiebersenkender und entzündungshemmender Wirkung, der z.B. in Tabletten gegen Fieber und Entzündungszustände verwendet wird.",
	Abbaukoerper: 0.9,
	ohne: 0,
	ohnefilter: 0.9,
	mitfilter: 0.9,
	Grenzwert: 0.0000001,
	Durchschnitt: 1.5,
	GrammproPackung: 1,
	Packungsart: "Tube",
	Anwendung: [0, 5, 0.1] //min-, max-Wert sowie step in der Einheit (Tube/ Packung )/Jahr
}];

//Wertebereich inclusive Durchschnitt definieren
//var Anwendung = [0, 5, 0.1]; //min-, max-Wert sowie step in der Einheit (Tube/ Packung )/Jahr

//Kläranlage
var Klaeranlagelist = [{
	Klaeranlagename: "ohne",
	Klaeranlageklasse: "klaeranlage",
	Klaeranlagebegriff: "ohne",
	Beschreibung1: "Ohne ARA: ",
	Beschreibung2: "% des Abwassers und der enthaltenen Wirkstoffe gelangen ohne Reinigung direkt in die Umwelt.",
}, {
	Klaeranlagename: "ohnefilter",
	Klaeranlageklasse: "klaeranlage",
	Klaeranlagebegriff: "ohne MV-Filter",
	Beschreibung1: "ARA ohne MV-Stufe: Das Abwasser wird in der Kläranlage biologisch und chemisch gereinigt, sodass nur ",
	Beschreibung2: "% der Wirkstoffmenge in die Umwelt gelangen."
}, {
	Klaeranlagename: "mitfilter",
	Klaeranlageklasse: "klaeranlage",
	Klaeranlagebegriff: "mit MV-Filter",
	Beschreibung1: "ARA mit MV-Stufe: Durch eine weitergehende Abwasserreinigung, z.B. mit Aktivkohle, wird der Wirkstoff gut aus dem Abwasser entfernt und es fliessen nur ",
	Beschreibung2: "% davon in die Umwelt."
}];

//Gewaesser
var Gewaesserlist = [{
	Name: "Bach",
	Verduennungswert: 10,
	Beschreibung: "Wenn das Abwasser in einen kleinen Bach gelangt,  ist der Wirkstoff im Gewässer verhältnismässig hoch konzentriert."
}, {
	Name: "Fluss",
	Verduennungswert: 50,
	Beschreibung: "Fliesst das Abwasser in einen grösseren Fluss, wird die Wirkstoffmenge stärker verdünnt."
}, {
	Name: "Strom",
	Verduennungswert: 5,
	Beschreibung: "Wenn das Abwasser in einen grossen Strom geleitet wird, treten nur sehr geringe Konzentrationen des Wirkstoffs im Gewässer auf."
}];

//Verdünnung mit Abwasser
var vermabwasser = 300000; // l/Jahr

//Loesungsvariablen
var Loesung = [Wirkstofflist[0], Klaeranlagelist[1],Gewaesserlist[1]];

//Bilder für die versch. Resultate
var img = ["img/nonhappy.png",
	"img/middlehappy.png",
	"img/happy.png"];

//Texte für versch. Resultate
var resulttext = ["Der Grenzwert wurde überschritten.", "Man ist sehr nahe am Grenzwert.", "Der Grenzwert wurde unterschritten."];

var dosispropunkt = 0.1; //Dosis welche als Pkt dargestellt wird (g/pkt)
var PunkteproTube = 10; //Punkte welche in einer Tube dargestellt werden

//Defaultwerte
var wirkstoff = {};
var dosis = 0;
var klaeranl = {};
var gew = {};
var durchschnitt = 0;
var koerpernichtabbau = 0;
var imabw = 0;
var nachanl = 0;
var restwert = 0;
var r = 5;

/**
 * Funktion zur Auswahl (Lösung oder Beginn)
 */
function initStart() {
	var endOfUrl = document.URL.split("?")[1];
	if (endOfUrl == "Loesung") {

		//Defaultwerte
		wirkstoff = Loesung[0];
		dosis = wirkstoff.Durchschnitt;
		klaeranl = Loesung[1];
		gew = Loesung[2];
		durchschnitt = wirkstoff.Durchschnitt;
		koerpernichtabbau = (1 - wirkstoff.Abbaukoerper) * dosis;
		imabw = koerpernichtabbau / vermabwasser;
		nachanl = imabw * (1 - wirkstoff[klaeranl.Klaeranlagename]);
		restwert = nachanl / gew.Verduennungswert;

		document.getElementById("min1").innerHTML = wirkstoff.Anwendung[0];
		document.getElementById("max1").innerHTML = wirkstoff.Anwendung[1];
		document.getElementById("min2").innerHTML = wirkstoff.Anwendung[0];
		document.getElementById("max2").innerHTML = wirkstoff.Anwendung[1];

		document.getElementById("anwendungsbeschreibung").remove();
		document.getElementById("anwendungsbeschreibung2").remove();
		document.getElementById("anwendungsbeschreibung2_1").remove();
		document.getElementById("anwendung1").setAttribute("style", "z.-index: 0");

		document.getElementById("abbaubeschreibung").remove();
		document.getElementById("abbaubeschreibung2").remove();

		document.getElementById("resulttext").remove();

		init();

		Grenzwert_zu_Verbrauch(wirkstoff, klaeranl, gew, vermabwasser);
	}
	else {

		//DOM neu aufbauen
		document.getElementById("legende").innerHTML = '<h1 class="ueberschrift1">EduNat Proj</h1><p id="min1"></p><p id="max1"></p><input type="button" id="generationchange" value="Berechnungen anzeigen" onclick="clickgen1()" />';

		initWirkstoffStart()
	}
}

/**
 * Funktion zum initialisieren des Wirkstoffes
 */
function initWirkstoffStart() {


	d3.select("#legende")
		.append('a')  //untergeordnetes div erstellen
		.attr("id", "wirkstofflegend")
		.attr("class", "selectlegendlink")
		.attr("value", "wirkstoff")
		.attr("href", "#")
		.append("h2") //Überschrift erstellen
		.attr("class", "ueberschrift2")
		.text("Wirkstoff");


	d3.select("#wirkstoff")
		.append('p')  //Beschreibung
		.attr("id", "wirkstoffbeschreibung")
		.text("Wähle einen dieser " + Wirkstofflist.length +  " pharmazeutischen Wirkstoffe aus! Jeder von ihnen hilft uns Menschen als Medikament bei Krankheiten oder Verletzungen, kann aber in niedrigen Konzentrationen in der Umwelt z.B. zu Organschädigungen bei Fischen führen.")

	//für alle Elemente der globalen Variable Wirkstofflist
	for (var i = 0; i < Wirkstofflist.length; i++) {

		/*//Optionen in ID wirkstoffselect schreiben
		d3.select("#wirkstoffselect")
			.append("option")
			.attr("id", Wirkstofflist[i].Wirkstoffname)//Wirkstoffname des Elementes der Wirkstofflist
			.attr("class", "wirkstoff")
			.attr("value", Wirkstofflist[i].Wirkstoffname)
			.text(Wirkstofflist[i].Wirkstoffname);*/

		//Elemente in ID wirkstoff schreiben
		d3.select("#wirkstoff")
			.append('div')  //untergeordnetes div erstellen
			.attr("id", (Wirkstofflist[i].Wirkstoffname + "wirkstoff"))
			.attr("class", "wirkstoff")
			.append('a')  //link erstellen
			.attr("href", "#")
			.attr("onclick", ("selectorWirkstoffStart('" + Wirkstofflist[i].Wirkstoffname + "')"))
			.append("h2") //Überschrift erstellen
			.attr("class", "ueberschrift3")
			.text((Wirkstofflist[i].Wirkstoffname));

		//Beschreibung hinzufügen
		d3.select(("#" + Wirkstofflist[i].Wirkstoffname + "wirkstoff"))
			.append("p")  //Beschreibung erstellen
			.text((Wirkstofflist[i].Beschreibung));

		/*//Durchsuchen der Wirkstofflist nach dem gewählten Element
		if (Wirkstofflist[i].Wirkstoffname == wirkstoff.Wirkstoffname) {

			//Dem Wirkstoff die Klasse selectwirkstoff zuweisen (für css Einfärbung)
			document.getElementById((Wirkstofflist[i].Wirkstoffname + "wirkstoff")).setAttribute("class", "selectwirkstoff");
		}*/
	}

	/*//Auswahl der angezeigten Option mithilfe des Attributes "selected"
	document.getElementById(wirkstoff.Wirkstoffname).setAttribute("selected", "1");*/
}

/**
 * Funktion zur Auswahl des ersten Wirkstoffes
 * @param {String} Wirkstoff gewählter Wirkstoff
 */
function selectorWirkstoffStart(Wirkstoff) {

	d3.select("#legende")
		.append('select')  //untergeordnetes Element erstellen
		.attr("id", "wirkstoffselect")
		.attr("name", "wirkstoff")
		.attr("title", "wirkstoff")

	//für alle Elemente der globalen Variable Wirkstofflist
	for (var i = 0; i < Wirkstofflist.length; i++) {

		//Optionen in ID wirkstoffselect schreiben
		d3.select("#wirkstoffselect")
			.append("option")
			.attr("id", Wirkstofflist[i].Wirkstoffname)//Wirkstoffname des Elementes der Wirkstofflist
			.attr("class", "wirkstoff")
			.attr("value", Wirkstofflist[i].Wirkstoffname)
			.text(Wirkstofflist[i].Wirkstoffname);

		if (Wirkstofflist[i].Wirkstoffname == Wirkstoff) {

			//Wirkstoff zuweisen (globale Variable ändern)
			wirkstoff = Wirkstofflist[i];

			//Dem Wirkstoff die Klasse selectwirkstoff zuweisen (für css Einfärbung)
			document.getElementById((Wirkstofflist[i].Wirkstoffname + "wirkstoff")).setAttribute("class", "selectwirkstoff");
		}
	}

	document.getElementById("wirkstoffselect").disabled = true;

	//Auswahl der angezeigten Option mithilfe des Attributes "selected"
	document.getElementById(Wirkstoff).setAttribute("selected", "1");

	document.getElementById('wirkstoffbeschreibung').remove();

	durchschnitt = wirkstoff.Durchschnitt;

	initAbbauStart();

	return (wirkstoff);
}

/**
 * Funktion zum initialisieren des Abbaus
 */
function initAbbauStart() {

	document.getElementById('wirkstoff').setAttribute("style", "z-index:0");
	document.getElementById('wirkstofflegend').setAttribute("class", "legendlink");
	document.getElementById('anwendung').setAttribute("style", "z-index:1");
	//document.getElementById('anwendunglegend').setAttribute("class", "selectlegendlink");

	d3.select("#legende")
		.append('a')  //untergeordnetes div erstellen
		.attr("id", "anwendunglegend")
		.attr("class", "selectlegendlink")
		.attr("value", "anwendung")
		.attr("href", "#")
		.append("h2") //Überschrift erstellen
		.attr("class", "ueberschrift2")
		.text("Anwendung");

	d3.select("#legende")
		.append('div')  //untergeordnetes div erstellen
		.attr("id", "scalelegend")
		.attr("class", "scale1")

	d3.select("#legende")
		.append('p')  //untergeordnetes div erstellen
		.attr("id", "range1")

	d3.select("#legende")
		.append('p')  //untergeordnetes div erstellen
		.attr("id", "range1_1")

	document.getElementById('anwendungsbeschreibung').innerHTML = "Wähle einen jährlichen Verbrauch von " + wirkstoff.Wirkstoffname + "-" + wirkstoff.Packungsart + " aus, dessen Auswirkung Du beobachten möchtest!";

	//Elemente neu füllen, damit Werte und Anzeigen in Sliders stimmen
	/*var scale1 = document.createElement("input");
	scale1.setAttribute("id", "scale1");
	scale1.setAttribute("class", "seekBar");
	scale1.setAttribute("type", "range");
	scale1.setAttribute("min", String(Anwendung[0]));
	scale1.setAttribute("max", String(Anwendung[1]));
	scale1.setAttribute("step", String(Anwendung[2]));
	scale1.setAttribute("value", String((Anwendung[0] + Anwendung[1]) / 2));
	scale1.setAttribute("onchange", "selectorMengeStart(this.value, 'scale1', 'scale2')");
	document.getElementById('scalelegend').appendChild(scale1);*/

	var scale2 = document.createElement("input");
	scale2.setAttribute("id", "scale2");
	scale2.setAttribute("class", "seekBar");
	scale2.setAttribute("type", "range");
	scale2.setAttribute("min", String(wirkstoff.Anwendung[0]));
	scale2.setAttribute("max", String(wirkstoff.Anwendung[1]));
	scale2.setAttribute("step", String(wirkstoff.Anwendung[2]));
	scale2.setAttribute("value", String((wirkstoff.Anwendung[0] + wirkstoff.Anwendung[1]) / 2));
	scale2.setAttribute("onchange", "selectorMengeStart(this.value)");
	document.getElementById('scaleanwendung').appendChild(scale2);

	document.getElementById("min2").innerHTML = wirkstoff.Anwendung[0];
	document.getElementById("max2").innerHTML = wirkstoff.Anwendung[1];
}

/**
 * Funktion zur Auswahl des ersten Wirkstoffes
 * @param {Number} Dosis Dosis des gewählten Wirkstoffes
 */
function selectorMengeStart(Tube) {

	//globale Variable dosis
	dosis = Tube * wirkstoff.GrammproPackung;
	koerpernichtabbau = (1 - wirkstoff.Abbaukoerper) * dosis;
	imabw = koerpernichtabbau / vermabwasser;

	var scale1 = document.createElement("input");
	scale1.setAttribute("id", "scale1");
	scale1.setAttribute("class", "seekBar");
	scale1.setAttribute("type", "range");
	scale1.setAttribute("min", String(wirkstoff.Anwendung[0]));
	scale1.setAttribute("max", String(wirkstoff.Anwendung[1]));
	scale1.setAttribute("step", String(wirkstoff.Anwendung[2]));
	scale1.setAttribute("value", String(Tube));
	document.getElementById('scalelegend').appendChild(scale1);

	document.getElementById("scale1").disabled = true;

	//Mengentext ändern
	document.getElementById("min1").innerHTML = wirkstoff.Anwendung[0];
	document.getElementById("max1").innerHTML = wirkstoff.Anwendung[1];

	document.getElementById("range1").innerHTML = Tube + " " + wirkstoff.Packungsart + " /Jahr";
	document.getElementById("range1_1").innerHTML = dosis + " Gramm /Jahr";
	document.getElementById("range2").innerHTML = Tube + " " + wirkstoff.Packungsart + " /Jahr";
	document.getElementById("range2_1").innerHTML = dosis + " Gramm /Jahr";

	document.getElementById('anwendung1').setAttribute('style', 'z-index:0')

	//Neues Element aufbauen
	document.getElementById('anwendungsbeschreibung').remove();
	document.getElementById('anwendungsbeschreibungtext').innerHTML = "Du hast eine Medikamentendosis von " + Tube + " " + wirkstoff.Packungsart + " pro Jahr ausgewählt. Das entspricht einer jährlichen Wirkstoffmenge von etwa " + dosis + " g " + wirkstoff.Wirkstoffname + ".";
	document.getElementById('anwendungsbeschreibungtext2').innerHTML = "Du hast eine Medikamentendosis von " + Tube + " " + wirkstoff.Packungsart + " pro Jahr ausgewählt. Der Durchschnitt liegt bei " + wirkstoff.Durchschnitt + " Gramm pro Jahr.";

	anzeigeMenge(dosis);
	visMenge(dosis);

	return(dosis);
}

/**
 * Funktion zum initialisieren der Anwendung
 */
function initanwendungStart() {

	document.getElementById('anwendungsbeschreibung2').remove();
	document.getElementById('anwendungsbeschreibung2_1').remove();
	document.getElementById('anwendung').setAttribute("style", "z-index:0");
	document.getElementById('anwendunglegend').setAttribute("class", "legendlink");
	document.getElementById('abbau').setAttribute("style", "z-index:1");

	d3.select("#legende")
		.append('a')  //untergeordnetes div erstellen
		.attr("id", "abbaulegend")
		.attr("class", "selectedlegendlink")
		.attr("value", "abbau")
		.attr("href", "#")
		.append("h2") //Überschrift erstellen
		.attr("class", "ueberschrift2")
		.text("Abbau im Körper");

	document.getElementById('abbaubeschreibungtext').innerHTML = "Von dieser Wirkstoffmenge werden in Deinem Körper " + Math.round(wirkstoff.Abbaukoerper * 100) + " % abgebaut - die restliche Menge landet in der Toilette. Im Abwasser liegt der Wirkstoff in niedrigen Konzentrationen vor, weil er mit einer grossen Menge Wasser verdünnt wird.";
	document.getElementById('abbaubeschreibungtext2').innerHTML = "Von dieser Wirkstoffmenge werden in Deinem Körper " + Math.round(wirkstoff.Abbaukoerper * 100) + " % abgebaut - die restlichen " + (Math.round(koerpernichtabbau * 100) / 100) +" g " + wirkstoff.Wirkstoffname + " landen in der Toilette. Im Abwasser liegt der Wirkstoff in einer niedrigen Konzentration von " + (Math.round(imabw * 100000000) / 100) + " µg/L, weil er mit etwa 300'000 L Wasser pro Jahr verdünnt wird.";

	anzeigeAbbKoerper(dosis, koerpernichtabbau);
	visAbbKoerper(dosis, koerpernichtabbau);
	visAbw(imabw, wirkstoff)
}

/**
 * Funktion zum initialisieren der Kläranlage
 */
function initklaeranlageStart() {

	document.getElementById('abbaubeschreibung').remove();
	document.getElementById('abbaubeschreibung2').remove();

	document.getElementById('abbau').setAttribute("style", "z-index:0");
	document.getElementById('abbaulegend').setAttribute("class", "legendlink");
	document.getElementById('klaeranlage').setAttribute("style", "z-index:1");
	//document.getElementById('anwendunglegend').setAttribute("class", "selectlegendlink");

	d3.select("#legende")
		.append('a')  //untergeordnetes div erstellen
		.attr("id", "klaeranlagelegend")
		.attr("class", "selectlegendlink")
		.attr("value", "klaeranlage")
		.attr("href", "#")
		.append("h2") //Überschrift erstellen
		.attr("class", "ueberschrift2")
		.text("Kläranlage");

	d3.select("#klaeranlageliste")
		.append('p')  //Beschreibung
		.attr("id", "anlagebeschreibung")
		.text("Wähle aus, ob und wie das anfallende Abwasser behandelt werden soll!")

	//für alle Elemente der globalen Variable Klaeranlagelist
	for (var i = 0; i < Klaeranlagelist.length; i++) {

		/*//Optionen in ID wirkstoffselect schreiben
		d3.select("#klaeranlageselect")
			.append("option")
			.attr("id", Klaeranlagelist[i].Klaeranlagename)
			.attr("class", "klaeranlage")
			.attr("value", Klaeranlagelist[i].Klaeranlagename)
			.text(Klaeranlagelist[i].Klaeranlagebegriff);*/

		//Elemente in ID klaeranlage schreiben
		d3.select("#klaeranlageliste")
			.append("div")
			.attr("id", (Klaeranlagelist[i].Klaeranlagename + "anlage"))
			.attr("class", "klaeranlage")
			.append("a")
			.attr("href", "#")
			.attr("onclick", ("selectorAnlageStart('" + Klaeranlagelist[i].Klaeranlagename + "')"))
			.append("h2")
			.attr("class", "ueberschrift3")
			.text((Klaeranlagelist[i].Klaeranlagebegriff));

		//Beschreibung hinzufügen
		d3.select(("#" + Klaeranlagelist[i].Klaeranlagename + "anlage"))
			.append("p")
			.text((Klaeranlagelist[i].Beschreibung1 + (Math.round((1 - wirkstoff[Klaeranlagelist[i].Klaeranlagename]) * 100)) + Klaeranlagelist[i].Beschreibung2));

		/*//Durchsuchen der Klaeranlagelist nach dem gewählten Element
		if (Klaeranlagelist[i].Klaeranlagename == klaeranl.Klaeranlagename) {
			//Der Anlage die Klasse selectanlage zuweisen (für css Einfärbung)
			document.getElementById((Klaeranlagelist[i].Klaeranlagename + "anlage")).setAttribute("class", "selectanlage");
		}*/
	}

	/*//Auswahl der angezeigten Option mithilfe des Attributes "selected"
	document.getElementById(wirkstoff.Wirkstoffname).setAttribute("selected", "1");*/
}

/**
 * Funktion zur Auswahl des ersten Wirkstoffes
 * @param {String} Anlage gewählte Kläranlage
 */
function selectorAnlageStart(Anlage) {

	d3.select("#legende")
		.append('select')  //untergeordnetes Element erstellen
		.attr("id", "klaeranlageselect")
		.attr("name", "klaeranlage")
		.attr("title", "klaeranlage")

	//für alle Elemente der globalen Variable Klaeranlagelist
	for (var i = 0; i < Klaeranlagelist.length; i++) {

		d3.select("#klaeranlageselect")
			.append("option")
			.attr("id", Klaeranlagelist[i].Klaeranlagename)
			.attr("class", "klaeranlage")
			.attr("value", Klaeranlagelist[i].Klaeranlagename)
			.text(Klaeranlagelist[i].Klaeranlagebegriff);

		if (Klaeranlagelist[i].Klaeranlagename == Anlage) {

			//Kläranlage zuweisen (globale Variable ändern)
			klaeranl = Klaeranlagelist[i];

			//Dem Wirkstoff die Klasse selectanlage zuweisen (für css Einfärbung)
			document.getElementById((Klaeranlagelist[i].Klaeranlagename + "anlage")).setAttribute("class", "selectanlage");
		}
	}

	document.getElementById("klaeranlageselect").disabled = true;

	//Auswahl der angezeigten Option mithilfe des Attributes "selected"
	document.getElementById(Anlage).setAttribute("selected", "1");

	document.getElementById('anlagebeschreibung').remove();

	nachanl = imabw * (1 - wirkstoff[klaeranl.Klaeranlagename]);

	initGewaesserStart();

	return (klaeranl);
}

/**
 * Funktion zum initialisieren der Kläranlage
 */
function initGewaesserStart() {

	document.getElementById('klaeranlage').setAttribute("style", "z-index:0");
	document.getElementById('klaeranlagelegend').setAttribute("class", "legendlink");
	document.getElementById('gewaesser').setAttribute("style", "z-index:1");
	//document.getElementById('anwendunglegend').setAttribute("class", "selectlegendlink");

	d3.select("#legende")
		.append('a')  //untergeordnetes div erstellen
		.attr("id", "gewaesserlegend")
		.attr("class", "selectlegendlink")
		.attr("value", "gewaesser")
		.attr("href", "#")
		.append("h2") //Überschrift erstellen
		.attr("class", "ueberschrift2")
		.text("Gewässer");

	d3.select("#gewaesser")
		.append('p')  //Beschreibung
		.attr("id", "gewaesserbeschreibung")
		.text("Entscheide, in welches Gewässer das Abwasser geleitet werden soll!")

	//für alle Elemente der globalen Variable Gewaesserlist
	for (var i = 0; i < Gewaesserlist.length; i++) {

		/*//Optionen in ID wirkstoffselect schreiben
		d3.select("#klaeranlageselect")
			.append("option")
			.attr("id", Klaeranlagelist[i].Klaeranlagename)
			.attr("class", "klaeranlage")
			.attr("value", Klaeranlagelist[i].Klaeranlagename)
			.text(Klaeranlagelist[i].Klaeranlagebegriff);*/

		//Elemente in ID klaeranlage schreiben
		d3.select("#gewaesser")
			.append("div")
			.attr("id", (Gewaesserlist[i].Name + "gew"))
			.attr("class", "gewaesser")
			.append("a")
			.attr("href", "#")
			.attr("onclick", ("selectorGewaesserStart('" + Gewaesserlist[i].Name + "')"))
			.append("h2")
			.attr("class", "ueberschrift3")
			.text((Gewaesserlist[i].Name));

		//Beschreibung hinzufügen
		d3.select(("#" + Gewaesserlist[i].Name + "gew"))
			.append("p")
			.text((Gewaesserlist[i].Beschreibung));

		/*//Durchsuchen der Klaeranlagelist nach dem gewählten Element
		if (Klaeranlagelist[i].Klaeranlagename == klaeranl.Klaeranlagename) {
			//Der Anlage die Klasse selectanlage zuweisen (für css Einfärbung)
			document.getElementById((Klaeranlagelist[i].Klaeranlagename + "anlage")).setAttribute("class", "selectanlage");
		}*/
	}

	/*//Auswahl der angezeigten Option mithilfe des Attributes "selected"
	document.getElementById(wirkstoff.Wirkstoffname).setAttribute("selected", "1");*/
}

/**
 * Funktion zur Auswahl des ersten Wirkstoffes
 * @param {String} Gewaesser gewähltes Gewässer
 */
function selectorGewaesserStart(Gewaesser) {

	document.getElementById('gewaesserbeschreibung').remove()

	d3.select("#legende")
		.append('select')  //untergeordnetes Element erstellen
		.attr("id", "gewaesserselect")
		.attr("name", "gewaesser")
		.attr("title", "gewaesser")

	//für alle Elemente der globalen Variable Klaeranlagelist
	for (var i = 0; i < Gewaesserlist.length; i++) {

		d3.select("#gewaesserselect")
			.append("option")
			.attr("id", Gewaesserlist[i].Name)
			.attr("class", "gewaesser")
			.attr("value", Gewaesserlist[i].Name)
			.text(Gewaesserlist[i].Name);

		if (Gewaesserlist[i].Name == Gewaesser) {

			//Kläranlage zuweisen (globale Variable ändern)
			gew = Gewaesserlist[i];

			//Dem Wirkstoff die Klasse selectanlage zuweisen (für css Einfärbung)
			document.getElementById((Gewaesserlist[i].Name + "gew")).setAttribute("class", "selectgew");
		}
	}

	document.getElementById(Gewaesser).setAttribute("selected", "1");

	restwert = nachanl / gew.Verduennungswert;

	initresulttext();

	return(gew);
}

/**
 * Funktion zum initialisieren des Resultates
 */
function initresulttext() {

	document.getElementById("gewaesserselect").disabled = true;
	document.getElementById('gewaesser').setAttribute("style", "z-index:0");
	document.getElementById('gewaesserlegend').setAttribute("class", "legendlink");
	document.getElementById('resulttext').setAttribute("style", "z-index:1");

	var Beschreibung = [];
	if (restwert < wirkstoff.Grenzwert) {
		Beschreibung = ["niedrige", "noch lange nicht "]
	}
	else if (restwert == wirkstoff.Grenzwert) {
		Beschreibung = ["mittlete", "noch knapp nicht "]
	}
	else {
		Beschreibung = ["hohe", ""]
	}

	document.getElementById('resultbeschreibungtext').innerHTML = "Unter den von Dir gewählten Bedingungen wird im Gewässer eine " + Beschreibung[0] + " Konzentration an " + wirkstoff.Wirkstoffname + " vorkommen. Bei dieser Konzentration wäre " + Beschreibung[1] + "mit Gesundheitsschäden bei Fischen zu rechnen.";

	document.getElementById('resultbeschreibungtext2').innerHTML = "Unter den von Dir gewählten Bedingungen wird im Gewässer eine Konzentration von " + ((Math.round(restwert * 100000000))/100) +  " µg/L an " + wirkstoff.Wirkstoffname + " vorkommen. Mit Gesundheitsschäden bei Fischen wäre ab einer " + wirkstoff.Wirkstoffname + "-Konzentration von " + ((Math.round(wirkstoff.Grenzwert * 100000000))/100) + " µg/L zu rechnen.";

	visAbbAnlage(imabw, nachanl);
	anzeigeRest(restwert, wirkstoff, (gew.Verduennungswert * vermabwasser));
	visRest(dosis, restwert, wirkstoff);
}

/**
 * Funktion zum löschen einiger Elemente
 */
function beforeinit() {

	document.getElementById('resulttext').remove();

	document.getElementById('wirkstofflegend').setAttribute('onclick', "inVordergrund('wirkstoff')");
	document.getElementById("wirkstoffselect").disabled = false;
	document.getElementById('wirkstoffselect').setAttribute('onchange', "selectorWirkstofflegend(this.value)");
	document.getElementById('wirkstoffselect').innerHTML = "";

	document.getElementById('anwendunglegend').setAttribute('onclick', "inVordergrund('anwendung')");
	document.getElementById('scalelegend').innerHTML = "";
	document.getElementById('range1').innerHTML = "";
	document.getElementById("range1_1").innerHTML = "";

	document.getElementById('abbaulegend').setAttribute('onclick', "inVordergrund('abbau')");

	document.getElementById('klaeranlagelegend').setAttribute('onclick', "inVordergrund('klaeranlage')");
	document.getElementById("klaeranlageselect").disabled = false;
	document.getElementById('klaeranlageselect').setAttribute('onchange', "selectorAnlagelegend(this.value)");
	document.getElementById('klaeranlageselect').innerHTML = "";

	document.getElementById('gewaesserlegend').setAttribute('onclick', "inVordergrund('gewaesser')");
	document.getElementById("gewaesserselect").disabled = false;
	document.getElementById('gewaesserselect').setAttribute('onchange', "selectorGewlegend(this.value)");
	document.getElementById('gewaesserselect').innerHTML = "";

	document.getElementById('wirkstoff').innerHTML = "";

	document.getElementById('scaleanwendung').innerHTML = "";
	document.getElementById('range2').innerHTML = "";
	document.getElementById('range2_1').innerHTML = "";
	document.getElementById('canvas1').innerHTML = "";
	document.getElementById('canvas1_1').innerHTML = "";

	document.getElementById('canvas2').innerHTML = "";
	document.getElementById('canvas2_1').innerHTML = "";
	document.getElementById('canvas2_2').innerHTML = "";
	document.getElementById('canvas2_3').innerHTML = "";
	document.getElementById('canvas2_4').innerHTML = "";

	document.getElementById('klaeranlageliste').innerHTML = "";
	document.getElementById('canvas3_1').innerHTML = "";

	document.getElementById('gewaesser').innerHTML = "";

	document.getElementById('canvas4').innerHTML = "";
	document.getElementById('canvas4_1').innerHTML = "";



	//Wirkstoffe initialisieren
	d3.select("#wirkstoffselect")
		.attr("onchange", "selectorWirkstofflegend(this.value)")

	document.getElementById('wirkstoffselect').innerHTML = '';
	document.getElementById('wirkstoff').innerHTML = "";

	init();
}

/**
 * Funktion zum initialisieren aller restlichen Elemente
 */
function init() {

	initWirkstoff();

	//Menge initialisieren
	//document.getElementById('anwendung').innerHTML = '<h2 class="ueberschrift2">Anwendung</h2><div id="scaleanwendung" class="scale2"></div><p id="range2"></p><div id="canvas1" class="generation1" style="z-index: 1"></div><div id="canvas1_1" class="generation2" style="z-index: 0"></div>';

	initMenge();

	//Mengentext ändern
	document.getElementById("range1").innerHTML = (dosis / wirkstoff.GrammproPackung) + " " + wirkstoff.Packungsart + " /Jahr";
	document.getElementById("range1_1").innerHTML = dosis + " Gramm /Jahr";
	document.getElementById("range2").innerHTML = (dosis / wirkstoff.GrammproPackung) + " " + wirkstoff.Packungsart + " /Jahr";
	document.getElementById("range2_1").innerHTML = dosis + " Gramm /Jahr";

	//Menge darstellen
	anzeigeMenge(dosis);
	visMenge(dosis);

	anzeigeAbbKoerper(dosis, koerpernichtabbau);
	visAbbKoerper(dosis, koerpernichtabbau);
	visAbw(imabw, wirkstoff);

	initAnlage();
	visAbbAnlage(imabw, nachanl);
	initGew();

	anzeigeRest(restwert, wirkstoff, (gew.Verduennungswert * vermabwasser));
	visRest(dosis, restwert, wirkstoff);
}

/**
 * Funktion zum initialisieren der Wirkstoff Optionen
 */
function initWirkstoff() {

	//für alle Elemente der globalen Variable Wirkstofflist
	for (var i = 0; i < Wirkstofflist.length; i++) {

		//Optionen in ID wirkstoffselect schreiben
		d3.select("#wirkstoffselect")
			.append("option")
			.attr("id", Wirkstofflist[i].Wirkstoffname)//Wirkstoffname des Elementes der Wirkstofflist
			.attr("class", "wirkstoff")
			.attr("value", Wirkstofflist[i].Wirkstoffname)
			.text(Wirkstofflist[i].Wirkstoffname);

		//Elemente in ID wirkstoff schreiben
		d3.select("#wirkstoff")
			.append('div')  //untergeordnetes div erstellen
			.attr("id", (Wirkstofflist[i].Wirkstoffname + "wirkstoff"))
			.attr("class", "wirkstoff")
			.append('a')  //link erstellen
			.attr("href", "#")
			.attr("onclick", ("selectorWirkstoffdiv('" + Wirkstofflist[i].Wirkstoffname + "')"))
			.append("h2") //Überschrift erstellen
			.attr("class", "ueberschrift3")
			.text((Wirkstofflist[i].Wirkstoffname));

		//Beschreibung hinzufügen
		d3.select(("#" + Wirkstofflist[i].Wirkstoffname + "wirkstoff"))
			.append("p")  //Beschreibung erstellen
			.text((Wirkstofflist[i].Beschreibung));

		//Durchsuchen der Wirkstofflist nach dem gewählten Element
		if (Wirkstofflist[i].Wirkstoffname == wirkstoff.Wirkstoffname) {

			//Dem Wirkstoff die Klasse selectwirkstoff zuweisen (für css Einfärbung)
			document.getElementById((Wirkstofflist[i].Wirkstoffname + "wirkstoff")).setAttribute("class", "selectwirkstoff");
		}
	}

	//Auswahl der angezeigten Option mithilfe des Attributes "selected"
	document.getElementById(wirkstoff.Wirkstoffname).setAttribute("selected", "1");
}

/**
 * Funktion zum initialisieren der Menge Optionen
 */
function initMenge() {

	//Elemente neu füllen, damit Werte und Anzeigen in Sliders stimmen
	var scale1 = document.createElement("input");
	scale1.setAttribute("id", "scale1");
	scale1.setAttribute("class", "seekBar");
	scale1.setAttribute("type", "range");
	scale1.setAttribute("min", String(wirkstoff.Anwendung[0]));
	scale1.setAttribute("max", String(wirkstoff.Anwendung[1]));
	scale1.setAttribute("step", String(wirkstoff.Anwendung[2]));
	scale1.setAttribute("value", String(dosis / wirkstoff.GrammproPackung));
	scale1.setAttribute("onchange", "selectorMenge(this.value, 'scale1', 'scale2')");
	document.getElementById('scalelegend').appendChild(scale1);

	var scale2 = document.createElement("input");
	scale2.setAttribute("id", "scale2");
	scale2.setAttribute("class", "seekBar");
	scale2.setAttribute("type", "range");
	scale2.setAttribute("min", String(wirkstoff.Anwendung[0]));
	scale2.setAttribute("max", String(wirkstoff.Anwendung[1]));
	scale2.setAttribute("step", String(wirkstoff.Anwendung[2]));
	scale2.setAttribute("value", String(dosis / wirkstoff.GrammproPackung));
	scale2.setAttribute("onchange", "selectorMenge(this.value, 'scale2', 'scale1')");
	document.getElementById('scaleanwendung').appendChild(scale2);
}

/**
 * Funktion zum initialisieren der Wirkstoff Optionen
 */
function initAnlage() {

	//für alle Elemente der globalen Variable Klaeranlagelist
	for (var i = 0; i < Klaeranlagelist.length; i++) {

		//Optionen in ID wirkstoffselect schreiben
		d3.select("#klaeranlageselect")
			.append("option")
			.attr("id", Klaeranlagelist[i].Klaeranlagename)
			.attr("class", "klaeranlage")
			.attr("value", Klaeranlagelist[i].Klaeranlagename)
			.text(Klaeranlagelist[i].Klaeranlagebegriff);

		//Elemente in ID klaeranlage schreiben
		d3.select("#klaeranlageliste")
			.append("div")
			.attr("id", (Klaeranlagelist[i].Klaeranlagename + "anlage"))
			.attr("class", "klaeranlage")
			.append("a")
			.attr("href", "#")
			.attr("onclick", ("selectorAnlagediv('" + Klaeranlagelist[i].Klaeranlagename + "')"))
			.append("h2")
			.attr("class", "ueberschrift3")
			.text((Klaeranlagelist[i].Klaeranlagebegriff));

		//Beschreibung hinzufügen
		d3.select(("#" + Klaeranlagelist[i].Klaeranlagename + "anlage"))
			.append("p")
			.text((Klaeranlagelist[i].Beschreibung1 + (Math.round((1 - wirkstoff[Klaeranlagelist[i].Klaeranlagename]) * 100)) + Klaeranlagelist[i].Beschreibung2));

		//Durchsuchen der Klaeranlagelist nach dem gewählten Element
		if (Klaeranlagelist[i].Klaeranlagename == klaeranl.Klaeranlagename) {
			//Der Anlage die Klasse selectanlage zuweisen (für css Einfärbung)
			document.getElementById((Klaeranlagelist[i].Klaeranlagename + "anlage")).setAttribute("class", "selectanlage");
		}
	}

	//Auswahl der angezeigten Option mithilfe des Attributes "selected"
	document.getElementById(klaeranl.Klaeranlagename).setAttribute("selected", "1");
}

/**
 * Funktion zum initialisieren der Gewässer Optionen
 */
function initGew() {

	//für alle Elemente der globalen Variable Gewaesserlist
	for (var i = 0; i < Gewaesserlist.length; i++) {

		//Optionen in ID gewaesserselect schreiben
		d3.select("#gewaesserselect")
			.append("option")
			.attr("id", Gewaesserlist[i].Name)
			.attr("class", "gewaesser")
			.attr("value", Gewaesserlist[i].Name)
			.text(Gewaesserlist[i].Name);

		//Elemente in ID gewaesser schreiben
		d3.select("#gewaesser")
			.append("div")
			.attr("id", (Gewaesserlist[i].Name + "gew"))
			.attr("class", "gewaesser")
			.append("a")
			.attr("href", "#")
			.attr("onclick", ("selectorGewdiv('" + Gewaesserlist[i].Name + "')"))
			.append("h2")
			.attr("class", "ueberschrift3")
			.text((Gewaesserlist[i].Name));

		//Beschreibung hinzufügen
		d3.select(("#" + Gewaesserlist[i].Name + "gew"))
			.append("p")
			.text((Gewaesserlist[i].Beschreibung));

		//Durchsuchen der Gewaesserlist nach dem gewählten Element
		if (Gewaesserlist[i].Name == gew.Name) {

			//Dem Wirkstoff die Klasse selectgew zuweisen (für css Einfärbung)
			document.getElementById((Gewaesserlist[i].Name + "gew")).setAttribute("class", "selectgew");
		}
	}

	//Auswahl der angezeigten Option mithilfe des Attributes "selected"
	document.getElementById(gew.Name).setAttribute("selected", "1");
}

/**
 * Funktion zum ein bestimmtes div-Element in den Vordergrund zu setzen
 * @param {String}    Vordergrund    ID des gewählten/ in Vordergrund zu setzenden div-Elementes
 */
function inVordergrund(Vordergrund) {

	//Elemente mit der Klasse selectedlegendlink in lokale Variable setzen
	var selectedlink = document.getElementById("legende").querySelectorAll(".selectlegendlink");

	//für alle Elemente in Variable selectedlink
	for (var i = 0; i < selectedlink.length; i++) {

		//Neue Klasse (legendlink) definieren
		selectedlink[i].setAttribute("class", "legendlink");
	}

	//Elemente mit der Klasse legendlink in lokale Variable setzen
	var x = document.getElementById("legende").querySelectorAll(".legendlink");

	//Iteration durch alle Listenelemente von x
	for (var k = 0; k < x.length; k++) {

		//Abgriff des in den Vordergrund zu serzenden Wertes (value)
		if (x[k].getAttribute('value') == Vordergrund) {

			//Das Element mit der ID des Wertes ('value') bekommt den z-index 1 und ist im Vordergrund
			document.getElementById((x[k].getAttribute('value'))).setAttribute("style", "z-index:1");

			//Das gewählte Element bekommt die Klasse selectlegendlink (wird durch css eingefärbt)
			x[k].setAttribute("class", "selectlegendlink");
		}
		else {

			//Sämtliche Elemente mit anderen ID's bekommen den z-index: 0 und sind im Hintergrund
			document.getElementById((x[k].getAttribute('value'))).setAttribute("style", "z-index:0");//Rest bekommt z-index: , Hintergrund
		}
	}
}

/**
 * Funktion zur Auswahl des Wirkstoffes in der legende
 * @param {String}    Wirkstoff    Wirkstoffstyp
 */
function selectorWirkstofflegend(Wirkstoff) {

	//Wirkstoff aus Wirkstoffliste extrahieren
	for (var i = 0; i < Wirkstofflist.length; i++) {
		if (Wirkstofflist[i].Wirkstoffname == Wirkstoff) {
			wirkstoff = Wirkstofflist[i];
		}

	}

	//Durchschnitt des gewählten Wirkstoffes herauslesen
	dosis = wirkstoff.Durchschnitt;
	durchschnitt = wirkstoff.Durchschnitt;

	selectorMenge((dosis / wirkstoff.GrammproPackung), 'scale2', 'scale1');

	selectorMenge((dosis / wirkstoff.GrammproPackung), 'scale1', 'scale2');

	//Visualisierung des Wirkstoffes und der Menge
	anzeigeWirkstofflegend(wirkstoff);
	visMenge(dosis);

	//Berechnung und Visualisierung des Abbaus im Körper
	berAbbKoerper(wirkstoff, dosis);
	anzeigeAbbKoerper(dosis, koerpernichtabbau);
	visAbbKoerper(dosis, koerpernichtabbau);

	//Vermischung mit dem Abwasser und Vis/ Berechnung der Kläranlage
	vermAbw(koerpernichtabbau, vermabwasser);
	visAbw(imabw, wirkstoff);
	abbauAnlage(imabw, klaeranl, wirkstoff);
	visAbbAnlage(imabw, nachanl);

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, (gew.Verduennungswert * vermabwasser));
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (wirkstoff);

}

/**
 * Funktion zur AUswahl des Wirkstoffes im div
 * @param {String}    Wirkstoff    Wirkstoffstyp
 */
function selectorWirkstoffdiv(Wirkstoff) {

	//Wirkstoff aus Wirkstoffliste extrahieren
	for (var i = 0; i < Wirkstofflist.length; i++) {
		if (Wirkstofflist[i].Wirkstoffname == Wirkstoff) {
			wirkstoff = Wirkstofflist[i];
		}
	}

	//Durchschnitt des gewählten Wirkstoffes herauslesen
	dosis = wirkstoff.Durchschnitt;
	durchschnitt = wirkstoff.Durchschnitt;

	selectorMenge((dosis / wirkstoff.GrammproPackung), 'scale2', 'scale1');

	selectorMenge((dosis / wirkstoff.GrammproPackung), 'scale1', 'scale2');

	//Visualisierung des Wirkstoffes und der Menge
	anzeigeWirkstoffdiv(wirkstoff);
	visMenge(dosis);

	//Berechnung und Visualisierung des Abbaus im Körper
	berAbbKoerper(wirkstoff, dosis);
	anzeigeAbbKoerper(dosis, koerpernichtabbau);
	visAbbKoerper(dosis, koerpernichtabbau);

	//Vermischung mit dem Abwasser und Vis/ Berechnung der Kläranlage
	vermAbw(koerpernichtabbau, vermabwasser);
	visAbw(imabw, wirkstoff);
	abbauAnlage(imabw, klaeranl, wirkstoff);
	visAbbAnlage(imabw, nachanl);

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, (gew.Verduennungswert * vermabwasser));
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (wirkstoff);

}

/**
 * Funktion um Slidervalue zu ändern
 * @param {Number}    Tube    gewählte Dosis (Tuben/Jahr)
 * @param {String} Identbelives   id des nicht zu ändernden Sliders
 * @param {String} Identremove    id deszu ändernden Sliders
 */
function selectorMenge(Tube, Identbelives, Identremove) {

	//Dosis definieren
	dosis = Tube * wirkstoff.GrammproPackung;

	document.getElementById("min1").innerHTML = wirkstoff.Anwendung[0];
	document.getElementById("min2").innerHTML = wirkstoff.Anwendung[0];
	document.getElementById("max1").innerHTML = wirkstoff.Anwendung[1];
	document.getElementById("max2").innerHTML = wirkstoff.Anwendung[1];

	//Sliders einander anpasseun
	changeSlider(Tube, Identbelives, Identremove);

	//Visualisierung der Menge
	anzeigeMenge(dosis);
	visMenge(dosis);

	//Berechnung und Visualisierung des Abbaus im Körper
	berAbbKoerper(wirkstoff, dosis);
	visAbbKoerper(dosis, koerpernichtabbau);
	anzeigeAbbKoerper(dosis, koerpernichtabbau);

	//Vermischung mit dem Abwasser und Vis/ Berechnung der Kläranlage
	vermAbw(koerpernichtabbau, vermabwasser);
	visAbw(imabw, wirkstoff);
	abbauAnlage(imabw, klaeranl, wirkstoff);
	visAbbAnlage(imabw, nachanl);

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, (gew.Verduennungswert * vermabwasser));
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (dosis);
}

/**
 * Funktion um Anlage in der Legenede zu selektieren
 * @param {String}    Anlage    Anlagetyp
 */
function selectorAnlagelegend(Anlage) {

	//Kläranlage aus Kläranlageliste extrahieren
	for (var i = 0; i < Klaeranlagelist.length; i++) {
		if (Klaeranlagelist[i].Klaeranlagename == Anlage) {
			klaeranl = Klaeranlagelist[i];
		}
	}

	//Vermischung mit dem Abwasser und Vis/ Berechnung der Kläranlage
	anzeigeAnlagelegend(klaeranl);
	abbauAnlage(imabw, klaeranl, wirkstoff);
	visAbbAnlage(imabw, nachanl);

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, (gew.Verduennungswert * vermabwasser));
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (klaeranl);
}

/**
 * Funktion um Anlage im div zu selektieren
 * @param {String}    Anlage    Anlagetyp
 */
function selectorAnlagediv(Anlage) {

	//Kläranlage aus Kläranlageliste extrahieren
	for (var i = 0; i < Klaeranlagelist.length; i++) {
		if (Klaeranlagelist[i].Klaeranlagename == Anlage) {
			klaeranl = Klaeranlagelist[i];
		}
	}

	//Vermischung mit dem Abwasser und Vis/ Berechnung der Kläranlage
	anzeigeAnlagediv(klaeranl);
	abbauAnlage(imabw, klaeranl, wirkstoff);
	visAbbAnlage(imabw, nachanl);

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, (gew.Verduennungswert * vermabwasser));
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (klaeranl);
}

/**
 * Funktion um Gewässer in der Legende selektieren
 * @param {String}    Gewaesser    Gewässertyp
 */
function selectorGewlegend(Gewaesser) {

	//Gewässer aus Gewässerliste extrahieren
	for (var i = 0; i < Gewaesserlist.length; i++) {
		if (Gewaesserlist[i].Name == Gewaesser) {
			gew = Gewaesserlist[i];
		}
	}

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	anzeigeGewlegend(gew);
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, (gew.Verduennungswert * vermabwasser));
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (gew);
}

/**
 * Funktion um Gewässer im Div selektieren
 * @param {String}    Gewaesser    Gewässertyp
 */
function selectorGewdiv(Gewaesser) {

	//Gewässer aus Gewässerliste extrahieren
	for (var i = 0; i < Gewaesserlist.length; i++) {
		if (Gewaesserlist[i].Name == Gewaesser) {
			gew = Gewaesserlist[i];
		}
	}

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	anzeigeGewdiv(gew);
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, (gew.Verduennungswert * vermabwasser));
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (gew);
}

/**
 * Funktion um anzuzeigen wie hoch der maximale Durchschnittsverbrauch bei bestimmetn Einstellungen sein darf
 * @param {Object} Wirkstoff gewählter Wirkstoff mit all seinen Eigenschaften
 * @param {Object} Anlage gewählte Anlage mit all ihren Eigenschaften
 * @param {Object} Gewaesser gewähltes Gewässer mit all seinen Eigenschaften
 * @param {Object} VermAbwasser vermischte Abwassermenge
 */
function Grenzwert_zu_Verbrauch(Wirkstoff, Anlage, Gewaesser, VermAbwasser) {

	//Berechnung der dosis
	dosis = Wirkstoff.Grenzwert * Gewaesser.Verduennungswert / (1 - Wirkstoff[Anlage.Klaeranlagename]) * VermAbwasser / (1 - Wirkstoff.Abbaukoerper);

	//dosis wird auf 10-tel Stelle abgerundet
	dosis = (Math.floor(dosis * 10) / 10);

	//Dosis wird herausgegeben
	alert("Mit diesen Einstellungen dürfen durchschnittlich " + dosis + "g pro Jahr gebraucht werden.");

	//Sliders einander anpasseun
	changeSlider((dosis / wirkstoff.GrammproPackung), 'scale1', 'scale2');
	changeSlider((dosis / wirkstoff.GrammproPackung), 'scale2', 'scale1');

	//Visualisierung der Menge
	anzeigeMenge(dosis);
	visMenge(dosis);

	//Berechnung und Visualisierung des Abbaus im Körper
	berAbbKoerper(wirkstoff, dosis);
	visAbbKoerper(dosis, koerpernichtabbau);
	anzeigeAbbKoerper(dosis, koerpernichtabbau);

	//Vermischung mit dem Abwasser und Vis/ Berechnung der Kläranlage
	vermAbw(koerpernichtabbau, vermabwasser);
	visAbw(imabw, wirkstoff);
	abbauAnlage(imabw, klaeranl, wirkstoff);

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, (gew.Verduennungswert * vermabwasser));
	visRest(dosis, restwert, wirkstoff);

}

/**
 * Funktion um Berechnungen anzuzeigen
 */
function clickgen1() {

	//lokale Variablen definieren
	var gen1 = document.querySelectorAll(".generation1"); //Skizzen-canvases
	var gen2 = document.querySelectorAll(".generation2"); //Berechnungs-canvases

	//für jedes Element der Klasse generation1
	for (var i = 0; i < gen1.length; i++) {
		//z-index = 0; somit kommen die Elemente in den Hintergrund
		gen1[i].setAttribute("style", "z-index:0");
	}

	//für jedes Element der Klasse generation2
	for (var k = 0; k < gen2.length; k++) {
		//z-index = 1; somit kommen die Elemente in den Vordergrund
		gen2[k].setAttribute("style", "z-index:1");
	}

	//clickbuttontext (value) ändern und neue Funktion (onclick) zuweisen
	document.getElementById("generationchange").setAttribute("value", "Skizzen anzeigen");
	document.getElementById("generationchange").setAttribute("onclick", "clickgen2()");

	//Listenhöhe Anpassen
	document.getElementById("klaeranlageliste").setAttribute("style", "height:50%");
	document.getElementById("canvas3_1").setAttribute("style", "top: 50%; height: 50%");
}

/**
 * Funktion um Skizzen anzuzeigen
 */
function clickgen2() {

	//lokale Variablen definieren
	var gen1 = document.querySelectorAll(".generation1");
	var gen2 = document.querySelectorAll(".generation2");

	//für jedes Element der Klasse generation1
	for (var i = 0; i < gen1.length; i++) {
		//z-index = 1; somit kommen die Elemente in den Vordergrund
		gen1[i].setAttribute("style", "z-index:1");
	}

	//für jedes Element der Klasse generation2
	for (var k = 0; k < gen2.length; k++) {
		//z-index = 0; somit kommen die Elemente in den Hintergrund
		gen2[k].setAttribute("style", "z-index:0");
	}

	//clickbuttontext (value) ändern und neue Funktion (onclick) zuweisen
	document.getElementById("generationchange").setAttribute("value", "Berechnungen anzeigen");
	document.getElementById("generationchange").setAttribute("onclick", "clickgen1()");

	//Listenhöhe Anpassen
	document.getElementById("klaeranlageliste").setAttribute("style", "height:100%");
	document.getElementById("canvas3_1").setAttribute("style", "top: 100%; height: 0%");
}

/**
 * Funktion um in der Legende gewählten Wirkstoff aunzuzeigen
 * @param {Object}    Wirkstoff    Wirkstoff mit all seinen Werten
 */
function anzeigeWirkstofflegend(Wirkstoff) {

	//für alle Elemente der globalen Variable Wirkstofflist
	for (var i = 0; i < Wirkstofflist.length; i++) {

		//Durchsuchen der Wirkstofflist nach dem gewählten Element
		if (Wirkstofflist[i].Wirkstoffname == Wirkstoff.Wirkstoffname) {

			//Dem Wirkstoff die Klasse selectwirkstoff zuweisen (für css Einfärbung)
			document.getElementById((Wirkstofflist[i].Wirkstoffname + "wirkstoff")).setAttribute("class", "selectwirkstoff");
		}

		//die restlichen kriegen die Klasse Wirkstoff
		else {
			document.getElementById((Wirkstofflist[i].Wirkstoffname + "wirkstoff")).setAttribute("class", "wirkstoff");
		}
	}
}

/**
 * Funktion um im Div gewählten Wirkstoff aunzuzeigen
 * @param {Object}    Wirkstoff    Wirkstoff mit all seinen Werten
 */
function anzeigeWirkstoffdiv(Wirkstoff) {

	//Element mit id wirkstoffselect leeren
	document.getElementById('wirkstoffselect').innerHTML = '';

	//für alle Elemente der globalen Variable Wirkstofflist
	for (var i = 0; i < Wirkstofflist.length; i++) {

		//Optionen in ID wirkstoffselect schreiben
		d3.select("#wirkstoffselect")
			.append("option")
			.attr("id", Wirkstofflist[i].Wirkstoffname)//Wirkstoffname des Elementes der Wirkstofflist
			.attr("class", "wirkstoff")
			.attr("value", Wirkstofflist[i].Wirkstoffname)
			.text(Wirkstofflist[i].Wirkstoffname);

		//Durchsuchen der Wirkstofflist nach dem gewählten Element
		if (Wirkstofflist[i].Wirkstoffname == Wirkstoff.Wirkstoffname) {

			//Dem Wirkstoff die Klasse selectwirkstoff zuweisen (für css Einfärbung)
			document.getElementById((Wirkstofflist[i].Wirkstoffname + "wirkstoff")).setAttribute("class", "selectwirkstoff");
			document.getElementById(Wirkstofflist[i].Wirkstoffname).setAttribute("selected", "1");
		}

		//die restlichen kriegen die Klasse Wirkstoff
		else {
			document.getElementById((Wirkstofflist[i].Wirkstoffname + "wirkstoff")).setAttribute("class", "wirkstoff");
		}
	}
}

/**
 * Funktion um Slidervalue zu ändern
 * @param    {Number} Tube    gewählte Dosis (Tuben /Jahr)
 * @param {String} Identbelives   id des nicht zu ändernden Sliders
 * @param {String} Identremove    id deszu ändernden Sliders
 */
function changeSlider(Tube, Identbelives, Identremove) {

	//Mengentexte ändern
	document.getElementById("range1").innerHTML = Tube + " " + wirkstoff.Packungsart + " /Jahr";
	document.getElementById("range1_1").innerHTML = dosis + " Gramm /Jahr";
	document.getElementById("range2").innerHTML = Tube + " " + wirkstoff.Packungsart + " /Jahr";
	document.getElementById("range2_1").innerHTML = dosis + " Gramm /Jahr";

	//Slider-element löschen
	document.getElementById(Identremove).remove();

	//Slider-element neu aufbauen
	var scale = document.createElement("input");
	scale.setAttribute("id", Identremove);
	scale.setAttribute("class", "seekBar");
	scale.setAttribute("type", "range");
	scale.setAttribute("min", String(wirkstoff.Anwendung[0]));
	scale.setAttribute("max", String(wirkstoff.Anwendung[1]));
	scale.setAttribute("step", String(wirkstoff.Anwendung[2]));
	scale.setAttribute("value", String(Tube));
	scale.setAttribute("onchange", "selectorMenge(this.value, '" + Identremove + "', '" + Identbelives + "')");
	document.getElementsByClassName(Identremove)[0].appendChild(scale);
}

/**
 * Funktion um in der Legende gewählte Anlage aunzuzeigen
 * @param {Object}    Anlage    Anlage mit all ihren Werten
 */
function anzeigeAnlagelegend(Anlage) {

	//für alle Elemente der globalen Liste
	for (var i = 0; i < Klaeranlagelist.length; i++) {

		//Durchsuchen nach dem gewählten Element
		if (Klaeranlagelist[i].Klaeranlagename == Anlage.Klaeranlagename) {

			//die Klassen zuweisen (für css Einfärbung)
			document.getElementById((Klaeranlagelist[i].Klaeranlagename + "anlage")).setAttribute("class", "selectanlage");
		}
		else {
			document.getElementById((Klaeranlagelist[i].Klaeranlagename + "anlage")).setAttribute("class", "klaeranlage");
		}
	}
}

/**
 * Funktion um im Div gewählte Anlage aunzuzeigen
 * @param {Object}    Anlage    Anlage mit all ihren Werten
 */
function anzeigeAnlagediv(Anlage) {

	//Element leeren
	document.getElementById('klaeranlageselect').innerHTML = '';

	//für alle Elemente der globalen Liste
	for (var i = 0; i < Klaeranlagelist.length; i++) {

		//Optionen in ID schreiben
		d3.select("#klaeranlageselect")
			.append("option")
			.attr("id", Klaeranlagelist[i].Klaeranlagename)
			.attr("class", "klaeranlage")
			.attr("value", Klaeranlagelist[i].Klaeranlagename)
			.text(Klaeranlagelist[i].Klaeranlagebegriff);

		//Durchsuchen nach dem gewählten Element
		if (Klaeranlagelist[i].Klaeranlagename == Anlage.Klaeranlagename) {

			//Klassen zuweisen (für css Einfärbung)
			document.getElementById((Klaeranlagelist[i].Klaeranlagename + "anlage")).setAttribute("class", "selectanlage");
			document.getElementById(Klaeranlagelist[i].Klaeranlagename).setAttribute("selected", "1");
		}
		else {
			document.getElementById((Klaeranlagelist[i].Klaeranlagename + "anlage")).setAttribute("class", "klaeranlage");
		}
	}
}

/**
 * Funktion um in der Legende gewähltes Gew$sser aunzuzeigen
 * @param {Object}    Gewaesser    Gewässer mit all seinen Werten
 */
function anzeigeGewlegend(Gewaesser) {

	//für alle Elemente der globalen Liste
	for (var i = 0; i < Gewaesserlist.length; i++) {

		//Durchsuchen dnach dem gewählten Element
		if (Gewaesserlist[i].Name == Gewaesser.Name) {

			//Klassen zuweisen (für css Einfärbung)
			document.getElementById((Gewaesserlist[i].Name + "gew")).setAttribute("class", "selectgew");
		}
		else {
			document.getElementById((Gewaesserlist[i].Name + "gew")).setAttribute("class", "gewaesser");
		}
	}
}

/**
 * Funktion um im Div gewähltes Gew$sser aunzuzeigen
 * @param {Object}    Gewaesser    Gewässer mit all seinen Werten
 */
function anzeigeGewdiv(Gewaesser) {

	//Element leeren
	document.getElementById('gewaesserselect').innerHTML = '';

	//für alle Elemente der globalen Liste
	for (var i = 0; i < Gewaesserlist.length; i++) {

		//Optionen in ID schreiben
		d3.select("#gewaesserselect")
			.append("option")
			.attr("id", Gewaesserlist[i].Name)
			.attr("class", "gewaesser")
			.attr("value", Gewaesserlist[i].Name)
			.text(Gewaesserlist[i].Name);

		//Durchsuchen nach dem gewählten Element
		if (Gewaesserlist[i].Name == Gewaesser.Name) {

			//Klassen zuweisen (für css Einfärbung)
			document.getElementById((Gewaesserlist[i].Name + "gew")).setAttribute("class", "selectgew");
			document.getElementById(Gewaesserlist[i].Name).setAttribute("selected", "1");
		}
		else {
			document.getElementById((Gewaesserlist[i].Name + "gew")).setAttribute("class", "gewaesser");
		}
	}
}

/**
 * Funktion um die Dosis grafisch (in Tuben) darzustellen
 * @param    {Number} Menge    gewählte Dosis (g/Jahr)
 */
function anzeigeMenge(Menge) {

	//Vlokale Variablen definieren
	var i = 0; //Hilfsvariable
	var Pkte = Menge / dosispropunkt; //Anzahl dargestellter Punkte
	var e = 0; //Hilfsvariable
	var Tube = Math.ceil(Pkte / PunkteproTube); //Anzahl dargestellter Tuben (pro 10 Pkte 1 Tube)

	//Variablen des svg's (Attribute)
	var w = 800;
	var h = 200 * Tube + 100;
	//h auf w abgleichen
	if (h < w) {
		h = w;
	}
	var padding = 150;

	//Alte Elemente in canvas 1 löschen
	document.getElementById('canvas1').innerHTML = '';

	//Create new svg-Element with class circle
	var svg = d3.select("#canvas1")
		.append("svg")
		.attr("class", "circle")
		.attr("width", w)
		.attr("height", h)
		.attr("viewBox", "0 0 " + w + " " + h);

	//erstellen von Tubenelementen
	for (e; e < Tube; e = e + 1) {
		d3.select(".circle")
			.append("rect")
			.attr('class', 'Tube')
			.attr('x', padding - 40)
			.attr('y', e * 200 + 50)
			.attr("width", 400)
			.attr("height", 100)
			.attr("style", "fill:none;stroke:none;stroke-width:0;fill-opacity:0.0;stroke-opacity:0.0");

		d3.select(".circle")
			.append("polygon")
			.attr('class', 'Tube')
			.attr('points', ((padding - 100) + "," + (e * 200 + 160) + " " + (padding - 70) + "," + (e * 200 + 160) + " " + (padding + 360) + "," +
			(e * 200 + 150) + " " + (padding + 360) + "," + (e * 200 + 50) + " " + (padding - 70) + "," + (e * 200 + 40) +
			" " + (padding - 100) + "," + (e * 200 + 40)))
			.attr("style", "fill:white;stroke:black;stroke-width:2;fill-opacity:1.0;stroke-opacity:1.0");

		d3.select(".circle")
			.append("defs")
			.append("clipPath")
			.attr('id', 'cut-off-bottom')
			.append("rect")
			.attr('x', padding - 70)
			.attr('y', e * 200 + 30)
			.attr('width', 50)
			.attr('height', 140);

		d3.select(".circle")
			.append("ellipse")
			.attr('class', 'Tube')
			.attr('cx', padding - 70)
			.attr('cy', e * 200 + 100)
			.attr("rx", 40)
			.attr("ry", 60)
			.attr('clip-path', 'url(#cut-off-bottom)')
			.attr("style", "fill:white;stroke:black;stroke-width:2;fill-opacity:1.0;stroke-opacity:1.0");

		d3.select(".circle")
			.append("ellipse")
			.attr('class', 'Tube')
			.attr('cx', padding - 100)
			.attr('cy', e * 200 + 100)
			.attr("rx", 40)
			.attr("ry", 60)
			.attr("style", "fill:white;stroke:black;stroke-width:2;fill-opacity:1.0;stroke-opacity:1.0");

		//erstellen von einzelnen Punkten (pro 0.1g des Wirkstoffes 1 Pkt)
		for (i; i < Pkte && i < ((e + 1) * PunkteproTube); i = i + 1) {
			d3.select(".circle")
				.append("circle")
				.attr('class', 'circle')
				.attr("cx", (i % (PunkteproTube / 2)) * 80 + padding)
				.attr("cy", Math.floor(i / (PunkteproTube / 2)) * 50 + 75 + Math.floor(i / PunkteproTube) * 100)
				.attr("r", (r * h / 0.8 / 1000));
		}
	}
}

/**
 * Funktion um die Dosis mit der Gesamten Bevölkerung zu vergleichen
 * @param {Number}    Menge    gewählte Dosis (g/ Jahr)
 */
function visMenge(Menge) {

	//Alte Elemente in canvas 1_1 leeren
	document.getElementById('canvas1_1').innerHTML = '';

	//Skalen definieren
	var xScale = d3.scaleLinear()
		.domain([1, 0])
		.range([285, 75]);

	var yScale = d3.scaleLinear()
		.domain([0, wirkstoff.Anwendung[1]])
		.range([285, 15]);

	//Achsen definieren
	var xAxis = d3.axisBottom()
		.scale(xScale)
		.ticks(0);

	var yAxis = d3.axisLeft()
		.scale(yScale)
		.ticks(10);

	//SVG definieren
	var svg = d3.select("#canvas1_1")
		.append("svg")
		.attr("id", "vergleichbev")
		.attr("viewBox", "0 0 300 300");

	//Bar Element zeichnen
	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "vergleichbevbar")
		.attr("x", 120)
		.attr("y", (15 + 270 - (270 / wirkstoff.Anwendung[1] * Menge)))
		.attr("height", (270 / wirkstoff.Anwendung[1] * Menge))
		.attr("width", 100);

	//Durchschnitt hinzufügen (als Linie)
	svg.append("g")
		.append("line")
		.attr("class", "durchschnitt")
		.attr("x1", 75)
		.attr("y1", (15 + 270 - 270 / wirkstoff.Anwendung[1] * durchschnitt))
		.attr("x2", 285)
		.attr("y2", (15 + 270 - 270 / wirkstoff.Anwendung[1] * durchschnitt));

	//Text hinzufügen
	svg.select("g")
		.append("text")
		.attr("class", "durchschnitttext")
		.attr("x", 0)
		.attr("y", 5)
		.attr("dy", "0.5em")
		.attr("transform", "translate(" + 285 + "," + (15 + 270 - 270 / wirkstoff.Anwendung[1] * durchschnitt) + ")")
		.attr("text-anchor", "end")
		.attr("font-size", 10)
		.attr("font-family", "sans-serif")
		.text("Durchschnitt");

	//X-Achse kreieren
	svg.append("g")
		.attr("class", "axis")
		.attr("transform", "translate(0,285)")
		.call(xAxis);

	//Y-Achse kreieren
	svg.append("g")
		.attr("id", "axisymg")
		.attr("class", "axis")
		.attr("transform", "translate(75, 0)")
		.call(yAxis)
		.call(yAxis)
		.append("text")
		.attr("fill", "#000")
		.attr("x", -9)
		.attr("y", 0.5)
		.attr("dy", "0.2em")
		.attr("transform", "rotate(-90 0 0) translate(-5, 10)")
		.style("text-anchor", "end")
		.style("text-anchor", "end")
		.text("Menge pro Jahr");

	//Legendendefinition
	var legend = svg.append("g")
		.attr("font-size", 10)
		.attr("transform", "translate(225, 0)");

	legend.append("rect")
		.attr("class", "legend")
		.attr("x", 0)
		.attr("y", 0)
		.attr("fill", "white")
		.attr("stroke", "black")
		.attr("height", 25)
		.attr("width", 75);

	legend.append("rect")
		.attr("class", "vergleichbevbar")
		.attr("x", 2.5)
		.attr("y", 17.5)
		.attr("height", 5)
		.attr("width", 5);

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 2.5)
		.attr("y", 10)
		.attr("style", "text-anchor: start; text-decoration: underline")
		.text("Legende");

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 10)
		.attr("y", 25)
		.attr("dy", "-0.2em")
		.attr("style", "text-anchor: start;")
		.text("Menge pro Jahr");

	//Achsentext um den String g/Jahr erweitern
	var a = document.getElementById('vergleichbev').getElementById('axisymg').getElementsByClassName('tick');
	for (var i = 0; i < a.length; i++) {
		a[i].getElementsByTagName('text')[0].innerHTML = a[i].getElementsByTagName('text')[0].innerHTML + "g/Jahr";
	}
}

/**
 * Funktion herauszugeben/ zu berechnen, welche Menge der Körper nicht abbaut
 * @param {Number}    Menge    gewählte Dosis (g/ Jahr)
 * @param {Object} Wirkstoff Wirkstofftyp mit all seinen Werten
 */
function berAbbKoerper(Wirkstoff, Menge) {
	koerpernichtabbau = (1 - Wirkstoff.Abbaukoerper) * Menge;

	return (koerpernichtabbau);
}

/**
 * Funktion zum darstellen welcher Anteil vom Körper abgebaut wird (symbolisch)
 * @param {Number}    Koerpernichtabbau    vom Körper nicht abgebaute Doseis (g/ Jahr)
 * @param {Object} Menge Wirkstofftyp mit all seinen Werten
 */
function anzeigeAbbKoerper(Menge, Koerpernichtabbau) {

	//Canvas2/ generation1 leeren
	document.getElementById('canvas2').innerHTML = '';
	document.getElementById('canvas2_1').innerHTML = '';
	document.getElementById('canvas2_2').innerHTML = '';

	//neue svg-Elemente kreieren
	d3.select("#canvas2")
		.append("svg")
		.attr("class", "abbau1")
		.attr("viewBox", "-50 0 150 200");

	d3.select("#canvas2_1")
		.append("svg")
		.attr("class", "abbau2")
		.attr("viewBox", "-50 0 150 200");

	d3.select("#canvas2_2")
		.append("svg")
		.attr("class", "abbau3")
		.attr("viewBox", "0 0 100 100")

	//Grundgerüst für vom Körper abgebaute Wirkstoffe
	d3.select("#canvas2")
		.append("p")
		.attr("id", "abbtext")
		.attr("class", "abbautext")
		.text("Abbau im Körper: " + (Math.round(( 1 - (Koerpernichtabbau / Menge)) * 100) ) + "%");

	d3.select(".abbau1")
		.append("polygon")
		.attr('class', 'Abbau')
		.attr('points', (100 + "," + 200 + " " + 50 + "," + 0 + " " + 0 + "," + 200))
		.attr("style", "fill:white;stroke:black;stroke-width:2;fill-opacity:1.0;stroke-opacity:1.0");

	d3.select(".abbau1")
		.append("circle")
		.attr('class', 'Abbau')
		.attr('cx', 50)
		.attr('cy', 50)
		.attr('r', 50)
		.attr("style", "fill:white;stroke:black;stroke-width:2;fill-opacity:1.0;stroke-opacity:1.0");

	d3.select(".abbau1")
		.append("rect")
		.attr('class', 'Abbau')
		.attr('x', 25)
		.attr('y', 100)
		.attr("width", 50)
		.attr("height", 100)
		.attr("style", "fill:none;stroke:none;stroke-width:0;fill-opacity:0.0;stroke-opacity:0.0");

	//Grundgerüst für vom Körper nicht abgebaute Wirkstoffe
	d3.select("#canvas2_1")
		.append("p")
		.attr("id", "nabbtext")
		.attr("class", "abbautext")
		.text("Ins Abwasser: " + Math.round((Koerpernichtabbau / Menge * 100 )) + "%");

	d3.select(".abbau2")
		.append("rect")
		.attr('class', 'InUmlauf')
		.attr('x', 0)
		.attr('y', 0)
		.attr("width", 50)
		.attr("height", 100)
		.attr("style", "fill:none;stroke:none;stroke-width:5;fill-opacity:0.1;stroke-opacity:0.9");

	d3.select(".abbau2")
		.append("defs")
		.append("pattern")
		.attr("id", "image")
		.attr("x", "0%")
		.attr("y", "0%")
		.attr("height", "100%")
		.attr("width", "100%")
		.attr("viewBox", "0 0 100 100")
		.append("image")
		.attr("x", "0")
		.attr("y", "0")
		.attr("width", "100")
		.attr("height", "100")
		.attr("xlink:href", "img/abbau.png");

	d3.select(".abbau2")
		.append("rect")
		.attr('class', 'InUmlauf')
		.attr('x', 0)
		.attr('y', 100)
		.attr("width", 100)
		.attr("height", 100)
		.attr("fill", "url(#image)");

	//Grundgerüst für vom Körper nicht abgebaute Wirkstoffe pro 300l Abwasser
	d3.select("#canvas2_2")
		.append("p")
		.attr("id", "vermtext")
		.attr("class", "abbautext")
		.text("Vermischung mit " + vermabwasser + "l Abwasser");

	d3.select(".abbau3")
		.append("defs")
		.append("pattern")
		.attr("id", "image2")
		.attr("x", "0%")
		.attr("y", "0%")
		.attr("height", "100%")
		.attr("width", "100%")
		.attr("viewBox", "0 0 400 100")
		.append("image")
		.attr("x", "0")
		.attr("y", "0")
		.attr("width", "400")
		.attr("height", "100")
		.attr("xlink:href", "img/welle.png");

	var imgheight = 0;

	for (imgheight; imgheight < 100; i = imgheight = imgheight + 25) {
		d3.select(".abbau3")
			.append("rect")
			.attr('class', 'inclAbwasser')
			.attr('x', 0)
			.attr('y', imgheight)
			.attr("width", 100)
			.attr("height", 25)
			.attr("style", "fill:url(#image2)");
	}

	d3.select(".abbau3")
		.append("rect")
		.attr('class', 'inclAbwasser')
		.attr('x', 0)
		.attr('y', 0)
		.attr("width", 100)
		.attr("height", 100)
		.attr("style", "fill:none;stroke:black;stroke-width:5;fill-opacity:0.1;stroke-opacity:0.9");


	var Pkte_total = Math.round(Menge / dosispropunkt);
	var Pktenicht_abgeb = Math.round(Koerpernichtabbau / dosispropunkt);
	var Pkte_abgeb = Pkte_total - Pktenicht_abgeb;

	var Anz_horiz_Abbau = Math.ceil(Math.sqrt(1 / 2) * Math.sqrt(Pkte_abgeb));
	var Anz_vert_Abbau = Math.ceil(Pkte_abgeb / Anz_horiz_Abbau);
	var Abstand_horiz_Abbau = 50 / Anz_horiz_Abbau;
	var Abstand_vert_Abbau = 100 / Anz_vert_Abbau;

	//effektive abgebaute Wirkstoffe
	for (var i = 0; i < Pkte_abgeb; i++) {
		d3.select(".abbau1")
			.append("circle")
			.attr('class', 'Abbau')
			.attr("cx", ((i % Anz_horiz_Abbau) + 0.5 ) * Abstand_horiz_Abbau + 25)
			.attr("cy", (Math.floor(i / Anz_horiz_Abbau) + 0.5) * Abstand_vert_Abbau + 100)
			.attr("r", (r * 2 / 0.6 / 0.85 / 10));
	}

	//lokale Variablen zur Darstellung
	var Anz_horiz_nAbbau = Math.ceil(Math.sqrt(1 / 2) * Math.sqrt(Pktenicht_abgeb));
	var Anz_vert_nAbbau = Math.ceil(Pktenicht_abgeb / Anz_horiz_nAbbau);
	var Abstand_horiz_nAbbau = 50 / Anz_horiz_nAbbau;
	var Abstand_vert_nAbbau = 100 / Anz_vert_nAbbau;

	//effektive in Umwelt entlassene Wirkstoffe
	for (var e = 0; e < Pktenicht_abgeb; e++) {
		d3.select(".abbau2")
			.append("circle")
			.attr('class', 'nAbbau')
			.attr("cx", ((e % Anz_horiz_nAbbau) + 0.5 ) * Abstand_horiz_nAbbau)
			.attr("cy", (Math.floor(e / Anz_horiz_nAbbau) + 0.5) * Abstand_vert_nAbbau)
			.attr("r", (r * 2 / 0.6 / 0.85 / 10));
	}

	//lokale Variablen zur Darstellung
	var Abstand = 100 / Math.sqrt(Pktenicht_abgeb);
	var Anz = Math.ceil(100 / Abstand);
	var Abstand_neu_horiz = 100 / Anz;
	var Anz_vert = Math.ceil(Pktenicht_abgeb / Anz);
	var Abstand_neu_vert = 100 / Anz_vert;


	//Wirkstoffe pro 3000hl Abwasser
	for (var u = 0; u < Pktenicht_abgeb; u++) {
		d3.select(".abbau3")
			.append("circle")
			.attr('class', 'nAbbau')
			.attr("cx", u % Anz * Abstand_neu_horiz + Abstand_neu_horiz / 2)
			.attr("cy", Math.floor(u / (Anz)) * Abstand_neu_vert + Abstand_neu_vert / 2)
			.attr("r", (r / 0.2 / 0.25 / 10));
	}
}

/**
 * Funktion zur Darstellung der im Körper abgebauten Stoffe (zahlenmässig/ dimensionsmässig)
 * @param    {Number} Menge    Dosis (g/Jahr)
 * @param {Number} Koerpernichtabbau  Im Körper nicht abgebauter Anteil der Stoffe
 */
function visAbbKoerper(Menge, Koerpernichtabbau) {

	//canvas 2_1 leeren
	document.getElementById('canvas2_3').innerHTML = '';

	//Skalen definieren
	var xScale = d3.scaleLinear()
		.domain([1, 0])
		.range([230, 40]);

	var yScaleleft = d3.scaleLinear()
		.domain([0, 100])
		.range([285, 15]);

	var yScaleright = d3.scaleLinear()
		.domain([0, Menge])
		.range([285, 15]);

	//Achsen definieren
	var xAxis = d3.axisBottom()
		.scale(xScale)
		.ticks(0);

	var yAxisleft = d3.axisLeft()
		.scale(yScaleleft)
		.ticks(10);

	var yAxisright = d3.axisRight()
		.scale(yScaleright)
		.ticks(Math.ceil(Menge * 2));

	//svg erstellen
	var svg = d3.select("#canvas2_3")
		.append("svg")
		.attr("id", "nicht_abgebaut")
		.attr("viewBox", "0 0 300 300");

	//bar's und beschreibende Texte erstellen
	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "vomKoerpernicht_abgebautbar")
		.attr("x", 85)
		.attr("y", (15 + 270 - (270 * (Koerpernichtabbau / Menge))))
		.attr("height", (270 - (270 * (1 - (Koerpernichtabbau / Menge))))/*(270 * abbaukoerper)*/)
		.attr("width", 100);

	/*svg.selectAll("bar")
		.data([1])
		.enter()
		.append("text")
		.attr("class", "vomKoerpernicht_abgebautbartext")
		.attr("text-anchor", "middle")
		.attr("alignment-baseline", "hanging")
		.attr("font-size", 8)
		.attr("x", 85 + 50)
		.attr("dy", "0.32em")
		.attr("y", (15 + 270 - (270 * (Koerpernichtabbau / Menge))))
		.text("Ins Abwasser: " + Math.round((Koerpernichtabbau / Menge) * 100) + "%");*/

	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "vomKoerper_abgebautbar")
		.attr("x", 85)
		.attr("y", 15)
		.attr("height", (270 * (1 - (Koerpernichtabbau / Menge))))
		.attr("width", 100);

	/*svg.selectAll("bar")
		.data([1])
		.enter()
		.append("text")
		.attr("class", "vomKoerpernicht_abgebautbartext")
		.attr("text-anchor", "middle")
		.attr("alignment-baseline", "hanging")
		.attr("font-size", 8)
		.attr("x", 85 + 50)
		.attr("dy", "0.32em")
		.attr("y", 15)
		.text("Abbau im Körper: " + Math.round((1 - (Koerpernichtabbau / Menge)) * 100) + "%");*/

	//Achsen definieren und Beschriftung anpassen
	svg.append("g")
		.attr("id", "axisbottom")
		.attr("class", "axis")
		.attr("transform", "translate(0,285)")
		.call(xAxis);

	svg.append("g")
		.attr("id", "axisleft")
		.attr("class", "axis")
		.attr("transform", "translate(40, 0)")
		.call(yAxisleft)
		.append("text")
		.attr("fill", "#000")
		.attr("x", -9)
		.attr("y", 0.5)
		.attr("dy", "0.2em")
		.attr("transform", "rotate(-90 0 0) translate(" + (-9 ) + ", 10)")
		.style("text-anchor", "end")
		.text("prozentualer Abbau");

	var a = document.getElementById('nicht_abgebaut').getElementById('axisleft').getElementsByClassName('tick');
	for (var i = 0; i < a.length; i++) {
		a[i].getElementsByTagName('text')[0].innerHTML = a[i].getElementsByTagName('text')[0].innerHTML + "%";
	}

	svg.append("g")
		.attr("id", "axisright")
		.attr("class", "axis")
		.attr("transform", "translate(230, 0)")
		.call(yAxisright)
		.append("text")
		.attr("fill", "#000")
		.attr("x", -9)
		.attr("y", 0.5)
		.attr("dy", "0.2em")
		.attr("transform", "rotate(-90 0 0) translate(" + (-9 ) + ", -10)")
		.style("text-anchor", "end")
		.text("effektiver Abbau");

	//Legendendefinition
	var legend = svg.append("g")
		.attr("font-size", 10)
		.attr("transform", "translate(225, 0)");

	legend.append("rect")
		.attr("class", "legend")
		.attr("x", 0)
		.attr("y", 0)
		.attr("fill", "white")
		.attr("stroke", "black")
		.attr("height", 35)
		.attr("width", 75);

	legend.append("rect")
		.attr("class", "vomKoerper_abgebautbar")
		.attr("x", 2.5)
		.attr("y", 17.5)
		.attr("height", 5)
		.attr("width", 5);

	legend.append("rect")
		.attr("class", "vomKoerpernicht_abgebautbar")
		.attr("x", 2.5)
		.attr("y", 27.5)
		.attr("height", 5)
		.attr("width", 5);

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 2.5)
		.attr("y", 10)
		.attr("style", "text-anchor: start; text-decoration: underline")
		.text("Legende");

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 10)
		.attr("y", 25)
		.attr("dy", "-0.2em")
		.attr("style", "text-anchor: start;")
		.text("Abgebaut");

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 10)
		.attr("y", 35)
		.attr("dy", "-0.2em")
		.attr("style", "text-anchor: start;")
		.text("Nicht abgebaut");

	var a2 = document.getElementById('nicht_abgebaut').getElementById('axisright').getElementsByClassName('tick');
	for (var k = 0; k < a2.length; k++) {
		a2[k].getElementsByTagName('text')[0].innerHTML = a2[k].getElementsByTagName('text')[0].innerHTML + " g/Jahr";
	}
}

/**
 * Funktion zur Berechnung der Menge pro l
 * @param    {Number} Vermabw    Anzahl l Abwasser
 * @param {Number} Koerpernichtabbau  Im Körper nicht abgebauter Anteil der Stoffe
 */
function vermAbw(Koerpernichtabbau, Vermabw) {
	imabw = Koerpernichtabbau / Vermabw;

	return (imabw);
}

/**
 * Funktion zur Darstellung der im Abwasser vorhnandenen Stoffe (zahlenmässig/ dimensionsmässig)
 * @param    {Number} Imabwasser    Dosis pro l-Abwasser
 * @param {Number} Wirkstoff  Im Körper nicht abgebauter Anteil der Stoffe
 */
function visAbw(Imabwasser, Wirkstoff) {

	//suche maximum im Abwasser
	var maximAbw = (1 - Wirkstoff["Abbaukoerper"]) * wirkstoff.Anwendung[1] / vermabwasser;

	//canvas leeren
	document.getElementById('canvas2_4').innerHTML = '';

	//Achsen definieren
	var xScale = d3.scaleLinear()
		.domain([1, 0])
		.range([285, 65]);

	var yScale = d3.scaleLinear()
		.domain([0, maximAbw * 1000])
		.range([285, 15]);

	var xAxis = d3.axisBottom()
		.scale(xScale)
		.ticks(0);

	var yAxis = d3.axisLeft()
		.scale(yScale)
		.ticks(10);

	//svg erstellen
	var svg = d3.select("#canvas2_4")
		.append("svg")
		.attr("id", "imabwasser")
		.attr("viewBox", "0 0 300 300");

	//Bar Element definieren
	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "imabwasser")
		.attr("x", 115)
		.attr("y", (15 + 270 - (270 * Imabwasser / maximAbw)))
		.attr("height", (270 * Imabwasser / maximAbw))
		.attr("width", 100);

	//Create X axis
	svg.append("g")
		.attr("class", "axis")
		.attr("transform", "translate(0,285)")
		.call(xAxis);

	//Create Y axis
	svg.append("g")
		.attr("id", "axisy")
		.attr("class", "axis")
		.attr("transform", "translate(65, 0)")
		.call(yAxis)
		.append("text")
		.attr("fill", "#000")
		.attr("x", -9)
		.attr("y", 0.5)
		.attr("dy", "0.2em")
		.attr("transform", "rotate(-90 0 0) translate(-10, 10)")
		.style("text-anchor", "end")
		.style("text-anchor", "end")
		.text("Menge pro Liter Abwasser");

	//Legendendefinition
	var legend = svg.append("g")
		.attr("font-size", 10)
		.attr("transform", "translate(225, 0)");

	legend.append("rect")
		.attr("class", "legend")
		.attr("x", 0)
		.attr("y", 0)
		.attr("fill", "white")
		.attr("stroke", "black")
		.attr("height", 25)
		.attr("width", 75);

	legend.append("rect")
		.attr("class", "imabwasser")
		.attr("x", 2.5)
		.attr("y", 17.5)
		.attr("height", 5)
		.attr("width", 5);

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 2.5)
		.attr("y", 10)
		.attr("style", "text-anchor: start; text-decoration: underline")
		.text("Legende");

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 10)
		.attr("y", 25)
		.attr("dy", "-0.2em")
		.attr("style", "text-anchor: start;")
		.text("Im Abwasser");

	//Text ändern
	var a = document.getElementById('imabwasser').getElementById('axisy').getElementsByClassName('tick');
	for (var i = 0; i < a.length; i++) {
		a[i].getElementsByTagName('text')[0].innerHTML = a[i].getElementsByTagName('text')[0].innerHTML + "mg/l";
	}
}

/**
 * Funktion zur Berechnung der Menge nach dem Durchgang einer Kläranlage
 * @param    {Number} ImAbw    ImAbwasser vor Kläranlage vorhandene Stoffe
 * @param {String} Anlage  Anlagetyp
 * @param {Object} Wirkstoff gewählter Wirkstoff mit all seinen Attributen
 */
function abbauAnlage(ImAbw, Anlage, Wirkstoff) {
	nachanl = ImAbw * (1 - Wirkstoff[Anlage.Klaeranlagename]);

	return (nachanl);
}

/**
 * Funktion zur Darstellung der in der Kläranlage abgebauten Stoffe (zahlenmässig/ dimensionsmässig)
 * @param    {Number} ImAbwasser    Im Abwasser vor Kläranlage vorhandenen Stoffe
 * @param {Number} Nachanlage  Im Abwasser nach Kläranlagedurchlauf vorhandenen Stoffe
 */
function visAbbAnlage(ImAbwasser, Nachanlage) {

	//canvas 4_1 leeren
	document.getElementById('canvas3_1').innerHTML = '';

	//Skalen definieren
	var xScale = d3.scaleLinear()
		.domain([1, 0])
		.range([230, 40]);

	var yScaleleft = d3.scaleLinear()
		.domain([0, 100])
		.range([285, 15]);

	var yScaleright = d3.scaleLinear()
		.domain([0, ImAbwasser * 1000])
		.range([285, 15]);

	//Achsen definieren
	var xAxis = d3.axisBottom()
		.scale(xScale)
		.ticks(0);

	var yAxisleft = d3.axisLeft()
		.scale(yScaleleft)
		.ticks(10);

	var yAxisright = d3.axisRight()
		.scale(yScaleright)
		.ticks(Math.ceil(ImAbwasser * 2000));

	//svg erstellen
	var svg = d3.select("#canvas3_1")
		.append("svg")
		.attr("id", "Anlnicht_abgebaut")
		.attr("viewBox", "0 0 300 300");

	//bar's und beschreibende Texte erstellen
	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "vonAnlagenicht_abgebautbar")
		.attr("x", 85)
		.attr("y", (15 + 270 - (270 * (Nachanlage / ImAbwasser))))
		.attr("height", (270 - (270 * (1 - (Nachanlage / ImAbwasser))))/*(270 * abbaukoerper)*/)
		.attr("width", 100);

	/*svg.selectAll("bar")
		.data([1])
		.enter()
		.append("text")
		.attr("class", "vonAnlagenicht_abgebautbartext")
		.attr("text-anchor", "middle")
		.attr("alignment-baseline", "hanging")
		.attr("font-size", 8)
		.attr("x", 85 + 50)
		.attr("dy", "0.32em")
		.attr("y", (15 + 270 - (270 * (Nachanlage / ImAbwasser))))
		.text("Ins Gewässer: " + Math.round((Nachanlage / ImAbwasser) * 100) + "%");*/

	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "vonAnlage_abgebautbar")
		.attr("x", 85)
		.attr("y", 15)
		.attr("height", (270 * (1 - (Nachanlage / ImAbwasser))))
		.attr("width", 100);

	/*if (Math.round(Nachanlage / ImAbwasser * 100) == 100) {
		//nichts geschieht
	}
	else {
		svg.selectAll("bar")
			.data([1])
			.enter()
			.append("text")
			.attr("class", "vonAnlagenicht_abgebautbartext")
			.attr("text-anchor", "middle")
			.attr("alignment-baseline", "hanging")
			.attr("font-size", 8)
			.attr("x", 85 + 50)
			.attr("dy", "0.32em")
			.attr("y", 15)
			.text("Abbau in der Anlage: " + Math.round((1 - (Nachanlage / ImAbwasser)) * 100) + "%");
	}*/

	//Achsen definieren und Beschriftung anpassen
	svg.append("g")
		.attr("id", "axisbottom")
		.attr("class", "axis")
		.attr("transform", "translate(0,285)")
		.call(xAxis);

	svg.append("g")
		.attr("id", "axisleft")
		.attr("class", "axis")
		.attr("transform", "translate(40, 0)")
		.call(yAxisleft)
		.append("text")
		.attr("fill", "#000")
		.attr("x", -9)
		.attr("y", 0.5)
		.attr("dy", "0.2em")
		.attr("transform", "rotate(-90 0 0) translate(" + (-9 ) + ", 10)")
		.style("text-anchor", "end")
		.text("prozentualer Abbau");

	var a = document.getElementById('Anlnicht_abgebaut').getElementById('axisleft').getElementsByClassName('tick');
	for (var i = 0; i < a.length; i++) {
		a[i].getElementsByTagName('text')[0].innerHTML = a[i].getElementsByTagName('text')[0].innerHTML + "%";
	}

	svg.append("g")
		.attr("id", "axisright")
		.attr("class", "axis")
		.attr("transform", "translate(230, 0)")
		.call(yAxisright)
		.append("text")
		.attr("fill", "#000")
		.attr("x", -9)
		.attr("y", 0.5)
		.attr("dy", "0.2em")
		.attr("transform", "rotate(-90 0 0) translate(" + (-9 ) + ", -10)")
		.style("text-anchor", "end")
		.text("effektiver Abbau");

		//Legendendefinition
		var legend = svg.append("g")
			.attr("font-size", 10)
			.attr("transform", "translate(225, 0)");

		legend.append("rect")
			.attr("class", "legend")
			.attr("x", 0)
			.attr("y", 0)
			.attr("fill", "white")
			.attr("stroke", "black")
			.attr("height", 35)
			.attr("width", 75);

		legend.append("rect")
			.attr("class", "vonAnlage_abgebautbar")
			.attr("x", 2.5)
			.attr("y", 17.5)
			.attr("height", 5)
			.attr("width", 5);

		legend.append("rect")
			.attr("class", "vonAnlagenicht_abgebautbar")
			.attr("x", 2.5)
			.attr("y", 27.5)
			.attr("height", 5)
			.attr("width", 5);

		legend.append("text")
			.attr("fill", "#000")
			.attr("x", 2.5)
			.attr("y", 10)
			.attr("style", "text-anchor: start; text-decoration: underline")
			.text("Legende");

		legend.append("text")
			.attr("fill", "#000")
			.attr("x", 10)
			.attr("y", 25)
			.attr("dy", "-0.2em")
			.attr("style", "text-anchor: start;")
			.text("Abgebaut");

		legend.append("text")
			.attr("fill", "#000")
			.attr("x", 10)
			.attr("y", 35)
			.attr("dy", "-0.2em")
			.attr("style", "text-anchor: start;")
			.text("Nicht abgebaut");

	var a2 = document.getElementById('Anlnicht_abgebaut').getElementById('axisright').getElementsByClassName('tick');
	for (var k = 0; k < a2.length; k++) {
		a2[k].getElementsByTagName('text')[0].innerHTML = a2[k].getElementsByTagName('text')[0].innerHTML + "mg";
	}
}

/**
 * Funktion zur Berechnung der im Gewässer vorhandenen Stoffe
 * @param    {Number} Nachanl    Dosis nach Kläranlagedurchlauf
 * @param {Object} Gewaesser  gewähltes Gewässer mit all seinen Eigenschaften
 */
function vermGew(Nachanl, Gewaesser) {
	restwert = Nachanl / Gewaesser.Verduennungswert;

	return (restwert);
}

/**
 * Funktion zur Darstellung der im vorhandenen Stoffe und Vergleich mit dem Grenzwert
 * @param    {Number} Restwert   Dosis im Gewässer (pro Hektare)
 * @param {Object} Wirkstoff  gewählter Wirkstoff mit all seinen Eigenschaften
 * @param {Number} Darstw  dargestellet Wassermenge
 */
function anzeigeRest(Restwert, Wirkstoff, Darstw) {

	//canvas leeren
	document.getElementById('canvas4').innerHTML = '';

	//Fischbild und Resultattext wählen
	var img4, verglstr;
	if ((Math.round(Wirkstoff.Grenzwert * Darstw / dosispropunkt)) < (Math.round(Restwert * Darstw / dosispropunkt))) {
		img4 = img[0];
		verglstr = resulttext[0];
	}
	else if ((Math.round(Wirkstoff.Grenzwert * Darstw / dosispropunkt)) == (Math.round(Restwert * Darstw / dosispropunkt))) {
		img4 = img[1];
		verglstr = resulttext[1];
	}
	else {
		img4 = img[2];
		verglstr = resulttext[2];
	}

	var maxVerdwert = gew.Verduennungswert;
	//maximaler Verdünnungswert
	for (var i = 0; i < Gewaesserlist.length; i++) {
		if (maxVerdwert < Gewaesserlist[i].Verduennungswert) {
			maxVerdwert = Gewaesserlist[i].Verduennungswert
		}
	}

	//canvas neu aufbauen (incl Texte)
	d3.select("#canvas4")
		.append("svg")
		.attr("class", "vergleich1")
		.attr("viewBox", "0 0 " + 100 * Math.sqrt(maxVerdwert / gew.Verduennungswert) + " " + 100 * Math.sqrt(maxVerdwert / gew.Verduennungswert));

	d3.select("#canvas4")
		.append("p")
		.attr("id", "modtext")
		.attr("class", "vergltext")
		.text("Modell");

	d3.select("#canvas4")
		.append("svg")
		.attr("class", "vergleich2")
		.attr("viewBox", "0 0 " + 100 * Math.sqrt(maxVerdwert / gew.Verduennungswert) + " " + 100 * Math.sqrt(maxVerdwert / gew.Verduennungswert));

	d3.select("#canvas4")
		.append("p")
		.attr("id", "grzwtext")
		.attr("class", "vergltext")
		.text("Grenzwert (pro " + Darstw + "l)");

	d3.select("#canvas4")
		.append("svg")
		.attr("class", "vergleich3")
		.attr("viewBox", "0 0 100 200");

	d3.select("#canvas4")
		.append("p")
		.attr("id", "vergltext")
		.attr("class", "vergltext")
		.text(verglstr);

	//Grundgerüst für Modell
	d3.select(".vergleich1")
		.append("defs")
		.append("pattern")
		.attr("id", "image3")
		.attr("x", "0%")
		.attr("y", "0%")
		.attr("height", "100%")
		.attr("width", "100%")
		.attr("viewBox", "0 0 400 100")
		.append("image")
		.attr("x", "0")
		.attr("y", "0")
		.attr("width", "400")
		.attr("height", "100")
		.attr("xlink:href", "img/welle.png");

	d3.select(".vergleich2")
		.append("defs")
		.append("pattern")
		.attr("id", "image3")
		.attr("x", "0%")
		.attr("y", "0%")
		.attr("height", "100%")
		.attr("width", "100%")
		.attr("viewBox", "0 0 400 100")
		.append("image")
		.attr("x", "0")
		.attr("y", "0")
		.attr("width", "400")
		.attr("height", "100")
		.attr("xlink:href", "img/welle.png");

	d3.select(".vergleich3")
		.append("defs")
		.append("pattern")
		.attr("id", "image4")
		.attr("x", "0%")
		.attr("y", "0%")
		.attr("height", "100%")
		.attr("width", "100%")
		.attr("viewBox", "0 0 200 100")
		.append("image")
		.attr("x", "0")
		.attr("y", "0")
		.attr("width", "200")
		.attr("height", "100")
		.attr("xlink:href", img4);


	for (var imgheight = 0; imgheight < 100; imgheight = imgheight + 25) {
		d3.select(".vergleich1")
			.append("rect")
			.attr('class', 'inclAbwasser')
			.attr('x', 0)
			.attr('y', imgheight)
			.attr("width", 100)
			.attr("height", 25)
			.attr("style", "fill:url(#image3)");

		d3.select(".vergleich2")
			.append("rect")
			.attr('class', 'inclAbwasser')
			.attr('x', 0)
			.attr('y', imgheight)
			.attr("width", 100)
			.attr("height", 25)
			.attr("style", "fill:url(#image3)");
	}

	d3.select(".vergleich1")
		.append("rect")
		.attr('class', 'Modell')
		.attr('x', 0)
		.attr('y', 0)
		.attr("width", 100)
		.attr("height", 100)
		.attr("style", "fill:none;stroke:black;stroke-width:5;fill-opacity:0.1;stroke-opacity:0.9");

	//Grundgerüst für Grenzwert
	d3.select(".vergleich2")
		.append("rect")
		.attr('class', 'Grenzwert')
		.attr('x', 0)
		.attr('y', 0)
		.attr("width", 100)
		.attr("height", 100)
		.attr("style", "fill:none;stroke:black;stroke-width:5;fill-opacity:0.1;stroke-opacity:0.9");

	//Hilfsvariablen für die Punktanzahl incl Punktabbildung
	var Pktenicht_abgeb = Math.round(Restwert * Darstw / dosispropunkt);
	var Abstand = 100 / Math.sqrt(Pktenicht_abgeb);
	var Anz = Math.ceil(100 / Abstand);
	var Abstand_neu_horiz = 100 / Anz;
	var Anz_vert = Math.ceil(Pktenicht_abgeb / Anz);
	var Abstand_neu_vert = 100 / Anz_vert;

	var u = 0;
	for (u; u < Pktenicht_abgeb; u = u + 1) {
		d3.select(".vergleich1")
			.append("circle")
			.attr('class', 'Modell')
			.attr("cx", u % Anz * Abstand_neu_horiz + Abstand_neu_horiz / 2)
			.attr("cy", Math.floor(u / (Anz)) * Abstand_neu_vert + Abstand_neu_vert / 2)
			.attr("r", (r * Math.sqrt(maxVerdwert / gew.Verduennungswert) / 0.8 / 0.45 / 10));
	}

	Pktenicht_abgeb = Math.round(Wirkstoff.Grenzwert * Darstw / dosispropunkt);
	Abstand = 100 / Math.sqrt(Pktenicht_abgeb);
	Anz = Math.ceil(100 / Abstand);
	Abstand_neu_horiz = 100 / Anz;
	Anz_vert = Math.ceil(Pktenicht_abgeb / Anz);
	Abstand_neu_vert = 100 / Anz_vert;

	u = 0;
	for (u; u < Pktenicht_abgeb; u = u + 1) {
		d3.select(".vergleich2")
			.append("circle")
			.attr('class', 'Grenzwert')
			.attr("cx", u % Anz * Abstand_neu_horiz + Abstand_neu_horiz / 2)
			.attr("cy", Math.floor(u / (Anz)) * Abstand_neu_vert + Abstand_neu_vert / 2)
			.attr("r", (r * Math.sqrt(maxVerdwert / gew.Verduennungswert) / 0.8 / 0.45 / 10));
	}

	d3.select(".vergleich3")
		.append("rect")
		.attr('class', 'inclAbwasser')
		.attr('x', 0)
		.attr('y', 0)
		.attr("width", 200)
		.attr("height", 100)
		.attr("style", "fill:url(#image4)")

}

/**
 * Funktion zur Darstellung der im Gewässer noch vorhandener Stoffe (zahlenmässig/ dimensionsmässig)
 * @param    {Number} Menge    Dosis (g/Jahr)
 * @param {Number} Restwert  Restwert im Gewässer
 * @param {Object} Wirkstoff  gewählter Wirkstoff mit all seinen Eigenschaften
 */
function visRest(Menge, Restwert, Wirkstoff) {

	//suche minimalen Gewässerverdünnungswert
	var minverdW = Gewaesserlist[0].Verduennungswert;
	for (var i = 0; i < Gewaesserlist.length; i++) {
		if (Gewaesserlist[i].Verduennungswert < minverdW) {
			minverdW = Gewaesserlist[i].Verduennungswert;
		}
	}

	//maximale Dosis mit minimalem Verdünnungswert und ohne Anlage verrechnet.
	var ohneanlmitminverdW = wirkstoff.Anwendung[1] * (1 - wirkstoff.Abbaukoerper) / vermabwasser / minverdW;

	//maximaler Wert der Domain festlegen
	var maxWert_Domain;
	if (ohneanlmitminverdW < Wirkstoff.Grenzwert) {
		maxWert_Domain = Wirkstoff.Grenzwert;
	}
	else {
		maxWert_Domain = ohneanlmitminverdW;
	}

	//canvas leeren
	document.getElementById('canvas4_1').innerHTML = '';

	//Skalen und Achsen definieren
	var xScale = d3.scaleLinear()
		.domain([1, 0])
		.range([285, 65]);

	var yScale = d3.scaleLinear()
		.domain([0, maxWert_Domain * 1000000])
		.range([285, 15]);

	var xAxis = d3.axisBottom()
		.scale(xScale)
		.ticks(0);

	var yAxis = d3.axisLeft()
		.scale(yScale)
		.ticks(10);

	//svg definieren
	var svg = d3.select("#canvas4_1")
		.append("svg")
		.attr("id", "restwert")
		.attr("viewBox", "0 0 300 300");

	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "restwert")
		.attr("x", 115)
		.attr("y", (15 + 270 - (270 * Restwert / maxWert_Domain)))
		.attr("height", (270 * Restwert / maxWert_Domain))
		.attr("width", 100);

	//Grenzwert hinzufügen
	svg.append("g")
		.append("line")
		.attr("class", "grenzwert")
		.attr("x1", 65)
		.attr("y1", (15 + 270 - (270 * Wirkstoff.Grenzwert / maxWert_Domain)))
		.attr("x2", 285)
		.attr("y2", (15 + 270 - (270 * Wirkstoff.Grenzwert / maxWert_Domain)));

	svg.select("g")
		.append("text")
		.attr("class", "grenzwerttext")
		.attr("x", 0)
		.attr("y", 5)
		.attr("dy", "0.5em")
		.attr("transform", "translate(" + 285 + "," + (15 + 270 - (270 * Wirkstoff.Grenzwert / maxWert_Domain)) + ")")
		.attr("text-anchor", "end")
		.attr("font-size", 10)
		.attr("font-family", "sans-serif")
		.text("Grenzwert");

	//Create X axis
	svg.append("g")
		.attr("class", "axis")
		.attr("transform", "translate(0,285)")
		.call(xAxis);

	//Create Y axis
	svg.append("g")
		.attr("id", "axisy")
		.attr("class", "axis")
		.attr("transform", "translate(65, 0)")
		.call(yAxis)
		.append("text")
		.attr("fill", "#000")
		.attr("x", -9)
		.attr("y", 0.5)
		.attr("dy", "0.2em")
		.attr("transform", "rotate(-90 0 0) translate(-9, 10)")
		.style("text-anchor", "end")
		.style("text-anchor", "end")
		.text("Menge pro Liter Flusswasser");

	//Legendendefinition
	var legend = svg.append("g")
		.attr("font-size", 10)
		.attr("transform", "translate(250, 0)");

	legend.append("rect")
		.attr("class", "legend")
		.attr("x", 0)
		.attr("y", 0)
		.attr("fill", "white")
		.attr("stroke", "black")
		.attr("height", 25)
		.attr("width", 50);

	legend.append("rect")
		.attr("class", "restwert")
		.attr("x", 2.5)
		.attr("y", 17.5)
		.attr("height", 5)
		.attr("width", 5);

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 2.5)
		.attr("y", 10)
		.attr("style", "text-anchor: start; text-decoration: underline")
		.text("Legende");

	legend.append("text")
		.attr("fill", "#000")
		.attr("x", 10)
		.attr("y", 25)
		.attr("dy", "-0.2em")
		.attr("style", "text-anchor: start;")
		.text("Restwert");

	//Text anpassen
	var a = document.getElementById('restwert').getElementById('axisy').getElementsByClassName('tick');
	for (var k = 0; k < a.length; k++) {
		a[k].getElementsByTagName('text')[0].innerHTML = a[k].getElementsByTagName('text')[0].innerHTML + "μg/l";
	}
}
