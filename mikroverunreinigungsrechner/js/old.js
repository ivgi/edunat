/**
 * Test Function
 */

//Globale Variablen
//Wirkstoffe
var Wirkstofflist = [{
	Wirkstoffname: "Diclofenac",
	Wirkstoffklasse: "wirkstoff",
	Beschreibung: "Dies ist der Wirkstoff Diclo.",
	Abbaukoerper: 0.3,
	ohne: 0,
	ohnefilter: 0.25,
	mitfilter: 0.95,
	Grenzwert: 0.00001, //Grenzwert mg/hl
	Durchschnitt: 1.1  //Durchschnittsverbrauch der Bevölkerung in Anheit der Anwendung
}, {
	Wirkstoffname: "Paracetamol",
	Wirkstoffklasse: "wirkstoff",
	Beschreibung: "Lorem ipsum",
	Abbaukoerper: 0.95,
	ohne: 0,
	ohnefilter: 0.9,
	mitfilter: 0.98,
	Grenzwert: 0.00001,
	Durchschnitt: 1.1
}, {
	Wirkstoffname: "Ibuprofen",
	Wirkstoffklasse: "wirkstoff",
	Beschreibung: "Lorem ipsum",
	Abbaukoerper: 0.9,
	ohne: 0,
	ohnefilter: 0.9,
	mitfilter: 0.9,
	Grenzwert: 0.00001,
	Durchschnitt: 1.5
}];

//Wertebereich inclusive Durchschnitt definieren
var Anwendung = [0, 5, " mg/Jahr", 0.1]; //min-, max-Wert und Skala (bsp. mg) sowie step

//Kläranlage
var Klaeranlagelist = [{
	Klaeranlagename: "ohne",
	Klaeranlageklasse: "klaeranlage",
	Klaeranlagebegriff: "ohne",
	Beschreibung: "Lorem ipsum"
}, {
	Klaeranlagename: "ohnefilter",
	Klaeranlageklasse: "klaeranlage",
	Klaeranlagebegriff: "ohne MV-Filter",
	Beschreibung: "Lorem ipsum"
}, {
	Klaeranlagename: "mitfilter",
	Klaeranlageklasse: "klaeranlage",
	Klaeranlagebegriff: "mit MV-Filter",
	Beschreibung: "Lorem ipsum"
}];

//Gewaesser
var Gewaesserlist = [{
	Name: "Bach",
	Verduennungswert: 10,
	Beschreibung: "Lorem ipsum"
}, {
	Name: "Fluss",
	Verduennungswert: 50,
	Beschreibung: "Lorem ipsum"
}, {
	Name: "Strom",
	Verduennungswert: 5,
	Beschreibung: "Lorem ipsum"
}];

//Verdünnung mit Abwasser
var vermabwasser = 3000; // hl/Jahr

//Dargestellte Menge Wasser (im Fluss/ Resultat)
var darstwasser = vermabwasser * 10;

//Bilder für die versch. Resultate
var img = ["img/nonhappy.png",
	"img/middlehappy.png",
	"img/happy.png"];

//Texte für versch. Resultate
var resulttext = ["Der Grenzwert wurde überschritten.", "Man ist sehr nahe am Grenzwert.", "Der Grenzwert wurde unterschritten."];

var dosispropunkt = 0.1; //Dosis welche als Pkt dargestellt wird (mg/pkt)
var PunkteproTube = 10; //Punkte welche in einer Tube dargestellt werden

//Defaultwerte
var wirkstoff = Wirkstofflist[0];
var dosis = wirkstoff.Durchschnitt;
var klaeranl = Klaeranlagelist[0];
var gew = Gewaesserlist[0];
var durchschnitt = wirkstoff.Durchschnitt;
var koerpernichtabbau = (1 - wirkstoff.Abbaukoerper) * dosis;
var imabw = koerpernichtabbau / vermabwasser;
var nachanl = imabw * (1 - wirkstoff[klaeranl.Klaeranlagename]);
var restwert = nachanl / gew.Verduennungswert;

/**
 * Funktion zum initialisieren aller Elemente
 */
function init() {

	"use strict";

	//Wirkstoffe initialisieren
	initWirkstoff();

	//Menge initialisieren
	initMenge();

	//Mengentext ändern
	document.getElementById("range1").innerHTML = dosis + Anwendung[2];
	document.getElementById("range2").innerHTML = dosis + Anwendung[2];

	//Menge darstellen
	anzeigeMenge(dosis);
	visMenge(dosis);

	anzeigeAbbKoerper(dosis, koerpernichtabbau);
	visAbbKoerper(dosis, koerpernichtabbau);

	initAnlage();
	initGew();

	anzeigeRest(restwert, wirkstoff, darstwasser);
	visRest(dosis, restwert, wirkstoff);
}

/**
 * Funktion zum initialisieren der Wirkstoff Optionen
 */
function initWirkstoff() {

	//für alle Elemente der globalen Variable Wirkstofflist
	Wirkstofflist.hasOwnProperty("Wirkstoffname");
	for (var i in Wirkstofflist) {
		//Optionen in ID wirkstoffselect schreiben
		d3.select("#wirkstoffselect")
			.append("option")
			.attr("id", Wirkstofflist[i].Wirkstoffname)//Wirkstoffname des Elementes der Wirkstofflist
			.attr("class", "wirkstoff")
			.attr("value", Wirkstofflist[i].Wirkstoffname)
			.text(Wirkstofflist[i].Wirkstoffname);

		//Elemente in ID wirkstoff schreiben
		d3.select("#wirkstoff")
			.append('div')  //untergeordnetes div erstellen
			.attr("id", (Wirkstofflist[i].Wirkstoffname + "wirkstoff"))
			.attr("class", "wirkstoff")
			.append('a')  //link erstellen
			.attr("href", "#")
			.attr("onclick", ("selectorWirkstoffdiv('" + Wirkstofflist[i].Wirkstoffname + "')"))
			.append("h2") //Überschrift erstellen
			.attr("class", "ueberschrift3")
			.text((Wirkstofflist[i].Wirkstoffname));

		//Beschreibung hinzufügen
		d3.select(("#" + Wirkstofflist[i].Wirkstoffname + "wirkstoff"))
			.append("p")  //Beschreibung erstellen
			.text((Wirkstofflist[i].Beschreibung));

		//Durchsuchen der Wirkstofflist nach dem gewählten Element
		if (Wirkstofflist[i].Wirkstoffname == wirkstoff.Wirkstoffname) {

			//Dem Wirkstoff die Klasse selectwirkstoff zuweisen (für css Einfärbung)
			document.getElementById((Wirkstofflist[i].Wirkstoffname + "wirkstoff")).setAttribute("class", "selectwirkstoff");
		}
	}

	//Auswahl der angezeigten Option mithilfe des Attributes "selected"
	document.getElementById(wirkstoff.Wirkstoffname).setAttribute("selected", "1");
}

/**
 * Funktion zum initialisieren der Menge Optionen
 */
function initMenge() {

	//Elemente neu füllen, damit Werte und Anzeigen in Sliders stimmen
	var scale1 = document.createElement("input");
	scale1.setAttribute("id", "scale1");
	scale1.setAttribute("class", "seekBar");
	scale1.setAttribute("type", "range");
	scale1.setAttribute("min", Anwendung[0]);
	scale1.setAttribute("max", Anwendung[1]);
	scale1.setAttribute("step", Anwendung[3]);
	scale1.setAttribute("value", dosis);
	scale1.setAttribute("onchange", "selectorMenge(this.value, 'scale1', 'scale2')");
	document.getElementById('scalelegend').appendChild(scale1);

	var scale2 = document.createElement("input");
	scale2.setAttribute("id", "scale2");
	scale2.setAttribute("class", "seekBar");
	scale2.setAttribute("type", "range");
	scale2.setAttribute("min", Anwendung[0]);
	scale2.setAttribute("max", Anwendung[1]);
	scale2.setAttribute("step", Anwendung[3]);
	scale2.setAttribute("value", dosis);
	scale2.setAttribute("onchange", "selectorMenge(this.value, 'scale2', 'scale1')");
	document.getElementById('scaleanwendung').appendChild(scale2);
}

/**
 * Funktion zum initialisieren der Wirkstoff Optionen
 */
function initAnlage() {

	//für alle Elemente der globalen Variable Klaeranlagelist
	for (var i in Klaeranlagelist) {
		//Optionen in ID wirkstoffselect schreiben
		d3.select("#klaeranlageselect")
			.append("option")
			.attr("id", Klaeranlagelist[i].Klaeranlagename)
			.attr("class", "klaeranlage")
			.attr("value", Klaeranlagelist[i].Klaeranlagename)
			.text(Klaeranlagelist[i].Klaeranlagebegriff);

		//Elemente in ID klaeranlage schreiben
		d3.select("#klaeranlage")
			.append("div")
			.attr("id", (Klaeranlagelist[i].Klaeranlagename + "anlage"))
			.attr("class", "klaeranlage")
			.append("a")
			.attr("href", "#")
			.attr("onclick", ("selectorAnlagediv('" + Klaeranlagelist[i].Klaeranlagename + "')"))
			.append("h2")
			.attr("class", "ueberschrift3")
			.text((Klaeranlagelist[i].Klaeranlagebegriff));

		//Beschreibung hinzufügen
		d3.select(("#" + Klaeranlagelist[i].Klaeranlagename + "anlage"))
			.append("p")
			.text((Klaeranlagelist[i].Beschreibung));

		//Durchsuchen der Klaeranlagelist nach dem gewählten Element
		if (Klaeranlagelist[i].Klaeranlagename == klaeranl.Klaeranlagename) {
			//Der Anlage die Klasse selectanlage zuweisen (für css Einfärbung)
			document.getElementById((Klaeranlagelist[i].Klaeranlagename + "anlage")).setAttribute("class", "selectanlage");
		}
	}

	//Auswahl der angezeigten Option mithilfe des Attributes "selected"
	document.getElementById(klaeranl.Klaeranlagename).setAttribute("selected", "1");
}

/**
 * Funktion zum initialisieren der Gewässer Optionen
 */
function initGew() {

	//für alle Elemente der globalen Variable Gewaesserlist
	for (var i in Gewaesserlist) {

		//Optionen in ID gewaesserselect schreiben
		d3.select("#gewaesserselect")
			.append("option")
			.attr("id", Gewaesserlist[i].Name)
			.attr("class", "gewaesser")
			.attr("value", Gewaesserlist[i].Name)
			.text(Gewaesserlist[i].Name);

		//Elemente in ID gewaesser schreiben
		d3.select("#gewaesser")
			.append("div")
			.attr("id", (Gewaesserlist[i].Name + "gew"))
			.attr("class", "gewaesser")
			.append("a")
			.attr("href", "#")
			.attr("onclick", ("selectorGewdiv('" + Gewaesserlist[i].Name + "')"))
			.append("h2")
			.attr("class", "ueberschrift3")
			.text((Gewaesserlist[i].Name));

		//Beschreibung hinzufügen
		d3.select(("#" + Gewaesserlist[i].Name + "gew"))
			.append("p")
			.text((Gewaesserlist[i].Beschreibung));

		//Durchsuchen der Gewaesserlist nach dem gewählten Element
		if (Gewaesserlist[i].Name == gew.Name) {

			//Dem Wirkstoff die Klasse selectgew zuweisen (für css Einfärbung)
			document.getElementById((Gewaesserlist[i].Name + "gew")).setAttribute("class", "selectgew");
		}
	}

	//Auswahl der angezeigten Option mithilfe des Attributes "selected"
	document.getElementById(gew.Name).setAttribute("selected", "1");
}

/**
 * Funktion zum ein bestimmtes div-Element in den Vordergrund zu setzen
 * @param {String}    Vordergrund    ID des gewählten/ in Vordergrund zu setzenden div-Elementes
 */
function inVordergrund(Vordergrund) {

	//Elemente mit der Klasse selectedlegendlink in lokale Variable setzen
	var selectedlink = document.getElementById("legende").querySelectorAll(".selectlegendlink");

	//für alle Elemente in Variable selectedlink
	var i = 0;
	for (i; i < selectedlink.length; i = i + 1) {

		//Neue Klasse (legendlink) definieren
		selectedlink[i].setAttribute("class", "legendlink");
	}

	//Elemente mit der Klasse legendlink in lokale Variable setzen
	var x = document.getElementById("legende").querySelectorAll(".legendlink");

	//Iteration durch alle Listenelemente von x
	i = 0;
	for (i; i < x.length; i = i + 1) {

		//Abgriff des in den Vordergrund zu serzenden Wertes (value)
		if (x[i].getAttribute('value') == Vordergrund) {

			//Das Element mit der ID des Wertes ('value') bekommt den z-index 1 und ist im Vordergrund
			document.getElementById((x[i].getAttribute('value'))).setAttribute("style", "z-index:1");

			//Das gewählte Element bekommt die Klasse selectlegendlink (wird durch css eingefärbt)
			x[i].setAttribute("class", "selectlegendlink");
		}
		else {

			//Sämtliche Elemente mit anderen ID's bekommen den z-index: 0 und sind im Hintergrund
			document.getElementById((x[i].getAttribute('value'))).setAttribute("style", "z-index:0");//Rest bekommt z-index: , Hintergrund
		}
	}
}

/**
 * Funktion zur Auswahl des Wirkstoffes in der legende
 * @param {String}    Wirkstoff    Wirkstoffstyp
 */
function selectorWirkstofflegend(Wirkstoff) {

	//Wirkstoff aus Wirkstoffliste extrahieren
	for (var i in Wirkstofflist) {
		if (Wirkstofflist[i].Wirkstoffname == Wirkstoff) {
			wirkstoff = Wirkstofflist[i];
		}
	}

	//Durchschnitt des gewählten Wirkstoffes herauslesen
	durchschnitt = wirkstoff.Durchschnitt;

	//Visualisierung des Wirkstoffes und der Menge
	anzeigeWirkstofflegend(wirkstoff);
	visMenge(dosis);

	//Berechnung und Visualisierung des Abbaus im Körper
	berAbbKoerper(wirkstoff, dosis);
	anzeigeAbbKoerper(dosis, koerpernichtabbau);
	visAbbKoerper(dosis, koerpernichtabbau);

	//Vermischung mit dem Abwasser und Vis/ Berechnung der Kläranlage
	vermAbw(koerpernichtabbau, vermabwasser);
	abbauAnlage(imabw, klaeranl, wirkstoff);

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, darstwasser);
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (wirkstoff);
	return (durchschnitt);
}

/**
 * Funktion zur AUswahl des Wirkstoffes im div
 * @param {String}    Wirkstoff    Wirkstoffstyp
 */
function selectorWirkstoffdiv(Wirkstoff) {

	//Wirkstoff aus Wirkstoffliste extrahieren
	for (i in Wirkstofflist) {
		if (Wirkstofflist[i].Wirkstoffname == Wirkstoff) {
			wirkstoff = Wirkstofflist[i];
		}
	}

	//Durchschnitt des gewählten Wirkstoffes herauslesen
	durchschnitt = wirkstoff.Durchschnitt;

	//Visualisierung des Wirkstoffes und der Menge
	anzeigeWirkstoffdiv(wirkstoff);
	visMenge(dosis);

	//Berechnung und Visualisierung des Abbaus im Körper
	berAbbKoerper(wirkstoff, dosis);
	anzeigeAbbKoerper(dosis, koerpernichtabbau);
	visAbbKoerper(dosis, koerpernichtabbau);

	//Vermischung mit dem Abwasser und Vis/ Berechnung der Kläranlage
	vermAbw(koerpernichtabbau, vermabwasser);
	abbauAnlage(imabw, klaeranl, wirkstoff);

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, darstwasser);
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (wirkstoff);
	return (durchschnitt);
}

/**
 * Funktion um Slidervalue zu ändern
 * @param {Number}    Menge    gewählte Dosis (mg/Jahr)
 * @param {String} Identbelives   id des nicht zu ändernden Sliders
 * @param {String} Identremove    id deszu ändernden Sliders
 */
function selectorMenge(Menge, Identbelives, Identremove) {

	//Dosis definieren
	dosis = Menge;

	//Sliders einander anpasseun
	changeSlider(dosis, Identbelives, Identremove);

	//Visualisierung der Menge
	anzeigeMenge(dosis);
	visMenge(dosis);

	//Berechnung und Visualisierung des Abbaus im Körper
	berAbbKoerper(wirkstoff, dosis);
	visAbbKoerper(dosis, koerpernichtabbau);
	anzeigeAbbKoerper(dosis, koerpernichtabbau);

	//Vermischung mit dem Abwasser und Vis/ Berechnung der Kläranlage
	vermAbw(koerpernichtabbau, vermabwasser);
	abbauAnlage(imabw, klaeranl, wirkstoff);

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, darstwasser);
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (dosis);
}

/**
 * Funktion um Anlage in der Legenede zu selektieren
 * @param {String}    Anlage    Anlagetyp
 */
function selectorAnlagelegend(Anlage) {

	//Kläranlage aus Kläranlageliste extrahieren
	for (i in Klaeranlagelist) {
		if (Klaeranlagelist[i].Klaeranlagename == Anlage) {
			klaeranl = Klaeranlagelist[i];
		}
	}

	//Vermischung mit dem Abwasser und Vis/ Berechnung der Kläranlage
	anzeigeAnlagelegend(klaeranl)
	abbauAnlage(imabw, klaeranl, wirkstoff);

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, darstwasser);
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (klaeranl);
}

/**
 * Funktion um Anlage im div zu selektieren
 * @param {String}    Anlage    Anlagetyp
 */
function selectorAnlagediv(Anlage) {

	//Kläranlage aus Kläranlageliste extrahieren
	for (i in Klaeranlagelist) {
		if (Klaeranlagelist[i].Klaeranlagename == Anlage) {
			klaeranl = Klaeranlagelist[i];
		}
	}

	//Vermischung mit dem Abwasser und Vis/ Berechnung der Kläranlage
	anzeigeAnlagediv(klaeranl)
	abbauAnlage(imabw, klaeranl, wirkstoff);

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, darstwasser);
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (klaeranl);
}

/**
 * Funktion um Gewässer in der Legende selektieren
 * @param {String}    Gewaesser    Gewässertyp
 */
function selectorGewlegend(Gewaesser) {

	//Gewässer aus Gewässerliste extrahieren
	for (i in Gewaesserlist) {
		if (Gewaesserlist[i].Name == Gewaesser) {
			gew = Gewaesserlist[i];
		}
	}

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	anzeigeGewlegend(gew);
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, darstwasser);
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (gew);
}

/**
 * Funktion um Gewässer im Div selektieren
 * @param {String}    Gewaesser    Gewässertyp
 */
function selectorGewdiv(Gewaesser) {

	//Gewässer aus Gewässerliste extrahieren
	for (i in Gewaesserlist) {
		if (Gewaesserlist[i].Name == Gewaesser) {
			gew = Gewaesserlist[i];
		}
	}

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	anzeigeGewdiv(gew);
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, darstwasser);
	visRest(dosis, restwert, wirkstoff);

	//Zurückgeben der Werte (Defaultwerte ändern)
	return (gew);
}

/**
 * Funktion um anzuzeigen wie hoch der maximale Durchschnittsverbrauch bei bestimmetn Einstellungen sein darf
 * @param {Object} Wirkstoff gewählter Wirkstoff mit all seinen Eigenschaften
 * @param {Object} Anlage gewählte Anlage mit all ihren Eigenschaften
 * @param {Object} Gewaesser gewähltes Gewässer mit all seinen Eigenschaften
 * @param {Object} VermAbwasser vermischte Abwassermenge
 */
function Grenzwert_zu_Verbrauch(Wirkstoff, Anlage, Gewaesser, VermAbwasser) {

	//Berechnung der dosis
	dosis = Wirkstoff.Grenzwert * Gewaesser.Verduennungswert / (1 - Wirkstoff[Anlage.Klaeranlagename]) * VermAbwasser / (1 - Wirkstoff.Abbaukoerper)

	//dosis wird auf 10-tel Stelle abgerundet
	dosis = (Math.floor(dosis * 10) / 10)

	//Dosis wird herausgegeben
	alert("Mit diesen Einstellungen dürfen durchschnittlich " + dosis + "mg pro Jahr gebraucht werden.");

	//Sliders einander anpasseun
	changeSlider(dosis, 'scale1', 'scale2');
	changeSlider(dosis, 'scale2', 'scale1');

	//Visualisierung der Menge
	anzeigeMenge(dosis);
	visMenge(dosis);

	//Berechnung und Visualisierung des Abbaus im Körper
	berAbbKoerper(wirkstoff, dosis);
	visAbbKoerper(dosis, koerpernichtabbau);
	anzeigeAbbKoerper(dosis, koerpernichtabbau);

	//Vermischung mit dem Abwasser und Vis/ Berechnung der Kläranlage
	vermAbw(koerpernichtabbau, vermabwasser);
	abbauAnlage(imabw, klaeranl, wirkstoff);

	//Vermischung mit dem Gewässer und Visualisierung des Resultates
	vermGew(nachanl, gew);
	anzeigeRest(restwert, wirkstoff, darstwasser);
	visRest(dosis, restwert, wirkstoff);

	return (dosis);
}

/**
 * Funktion um Berechnungen anzuzeigen
 */
function clickgen1() {

	//lokale Variablen definieren
	var gen1 = document.querySelectorAll(".generation1"); //Skizzen-canvases
	var gen2 = document.querySelectorAll(".generation2"); //Berechnungs-canvases

	//für jedes Element der Klasse generation1
	var i = 0;
	for (i; i < gen1.length; i = i + 1) {

		//z-index = 0; somit kommen die Elemente in den Hintergrund
		gen1[i].setAttribute("style", "z-index:0");
	}

	//für jedes Element der Klasse generation2
	var i = 0;
	for (i; i < gen2.length; i = i + 1) {

		//z-index = 1; somit kommen die Elemente in den Vordergrund
		gen2[i].setAttribute("style", "z-index:1");
	}

	//clickbuttontext (value) ändern und neue Funktion (onclick) zuweisen
	document.getElementById("generationchange").setAttribute("value", "Skizzen anzeigen")
	document.getElementById("generationchange").setAttribute("onclick", "clickgen2()")
}

/**
 * Funktion um Skizzen anzuzeigen
 */
function clickgen2() {

	//lokale Variablen definieren
	var gen1 = document.querySelectorAll(".generation1");
	var gen2 = document.querySelectorAll(".generation2");

	//für jedes Element der Klasse generation1
	var i = 0;
	for (i; i < gen1.length; i = i + 1) {

		//z-index = 1; somit kommen die Elemente in den Vordergrund
		gen1[i].setAttribute("style", "z-index:1");
	}

	//für jedes Element der Klasse generation2
	var i = 0;
	for (i; i < gen2.length; i = i + 1) {

		//z-index = 0; somit kommen die Elemente in den Hintergrund
		gen2[i].setAttribute("style", "z-index:0");
	}

	//clickbuttontext (value) ändern und neue Funktion (onclick) zuweisen
	document.getElementById("generationchange").setAttribute("value", "Berechnungen anzeigen")
	document.getElementById("generationchange").setAttribute("onclick", "clickgen1()")
}

/**
 * Funktion um in der Legende gewählten Wirkstoff aunzuzeigen
 * @param {Object}    Wirkstoff    Wirkstoff mit all seinen Werten
 */
function anzeigeWirkstofflegend(Wirkstoff) {

	//für alle Elemente der globalen Variable Wirkstofflist
	for (i in Wirkstofflist) {

		//Durchsuchen der Wirkstofflist nach dem gewählten Element
		if (Wirkstofflist[i].Wirkstoffname == Wirkstoff.Wirkstoffname) {

			//Dem Wirkstoff die Klasse selectwirkstoff zuweisen (für css Einfärbung)
			document.getElementById((Wirkstofflist[i].Wirkstoffname + "wirkstoff")).setAttribute("class", "selectwirkstoff");
		}

		//die restlichen kriegen die Klasse Wirkstoff
		else {
			document.getElementById((Wirkstofflist[i].Wirkstoffname + "wirkstoff")).setAttribute("class", "wirkstoff");
		}
	}
}

/**
 * Funktion um im Div gewählten Wirkstoff aunzuzeigen
 * @param {Object}    Wirkstoff    Wirkstoff mit all seinen Werten
 */
function anzeigeWirkstoffdiv(Wirkstoff) {

	//Element mit id wirkstoffselect leeren
	document.getElementById('wirkstoffselect').innerHTML = '';

	//für alle Elemente der globalen Variable Wirkstofflist
	for (i in Wirkstofflist) {

		//Optionen in ID wirkstoffselect schreiben
		d3.select("#wirkstoffselect")
			.append("option")
			.attr("id", Wirkstofflist[i].Wirkstoffname)//Wirkstoffname des Elementes der Wirkstofflist
			.attr("class", "wirkstoff")
			.attr("value", Wirkstofflist[i].Wirkstoffname)
			.text(Wirkstofflist[i].Wirkstoffname)

		//Durchsuchen der Wirkstofflist nach dem gewählten Element
		if (Wirkstofflist[i].Wirkstoffname == Wirkstoff.Wirkstoffname) {

			//Dem Wirkstoff die Klasse selectwirkstoff zuweisen (für css Einfärbung)
			document.getElementById((Wirkstofflist[i].Wirkstoffname + "wirkstoff")).setAttribute("class", "selectwirkstoff");
			document.getElementById(Wirkstofflist[i].Wirkstoffname).setAttribute("selected", "1");
		}

		//die restlichen kriegen die Klasse Wirkstoff
		else {
			document.getElementById((Wirkstofflist[i].Wirkstoffname + "wirkstoff")).setAttribute("class", "wirkstoff");
		}
	}
}

/**
 * Funktion um Slidervalue zu ändern
 * @param    {Number} Menge    gewählte Dosis (mg/Jahr)
 * @param {String} Identbelives   id des nicht zu ändernden Sliders
 * @param {String} Identremove    id deszu ändernden Sliders
 */
function changeSlider(Menge, Identbelives, Identremove) {

	//Mengentexte ändern
	document.getElementById("range1").innerHTML = Menge + Anwendung[2];
	document.getElementById("range2").innerHTML = Menge + Anwendung[2];

	//Slider-element löschen
	document.getElementById(Identremove).remove();

	//Slider-element neu aufbauen
	var scale = document.createElement("input");
	scale.setAttribute("id", Identremove);
	scale.setAttribute("class", "seekBar");
	scale.setAttribute("type", "range");
	scale.setAttribute("min", Anwendung[0]);
	scale.setAttribute("max", Anwendung[1]);
	scale.setAttribute("step", Anwendung[3]);
	scale.setAttribute("value", Menge);
	scale.setAttribute("onchange", "selectorMenge(this.value, '" + Identremove + "', '" + Identbelives + "')");
	document.getElementsByClassName(Identremove)[0].appendChild(scale);
}

/**
 * Funktion um in der Legende gewählte Anlage aunzuzeigen
 * @param {Object}    Anlage    Anlage mit all ihren Werten
 */
function anzeigeAnlagelegend(Anlage) {

	//für alle Elemente der globalen Liste
	for (i in Klaeranlagelist) {

		//Durchsuchen nach dem gewählten Element
		if (Klaeranlagelist[i].Klaeranlagename == Anlage.Klaeranlagename) {

			//die Klassen zuweisen (für css Einfärbung)
			document.getElementById((Klaeranlagelist[i].Klaeranlagename + "anlage")).setAttribute("class", "selectanlage");
		}
		else {
			document.getElementById((Klaeranlagelist[i].Klaeranlagename + "anlage")).setAttribute("class", "klaeranlage");
		}
	}
}

/**
 * Funktion um im Div gewählte Anlage aunzuzeigen
 * @param {Object}    Anlage    Anlage mit all ihren Werten
 */
function anzeigeAnlagediv(Anlage) {

	//Element leeren
	document.getElementById('klaeranlageselect').innerHTML = '';

	//für alle Elemente der globalen Liste
	for (i in Klaeranlagelist) {

		//Optionen in ID schreiben
		d3.select("#klaeranlageselect")
			.append("option")
			.attr("id", Klaeranlagelist[i].Klaeranlagename)
			.attr("class", "klaeranlage")
			.attr("value", Klaeranlagelist[i].Klaeranlagename)
			.text(Klaeranlagelist[i].Klaeranlagebegriff)

		//Durchsuchen nach dem gewählten Element
		if (Klaeranlagelist[i].Klaeranlagename == Anlage.Klaeranlagename) {

			//Klassen zuweisen (für css Einfärbung)
			document.getElementById((Klaeranlagelist[i].Klaeranlagename + "anlage")).setAttribute("class", "selectanlage");
			document.getElementById(Klaeranlagelist[i].Klaeranlagename).setAttribute("selected", "1");
		}
		else {
			document.getElementById((Klaeranlagelist[i].Klaeranlagename + "anlage")).setAttribute("class", "klaeranlage");
		}
	}
}

/**
 * Funktion um in der Legende gewähltes Gew$sser aunzuzeigen
 * @param {Object}    Gewaesser    Gewässer mit all seinen Werten
 */
function anzeigeGewlegend(Gewaesser) {

	//für alle Elemente der globalen Liste
	for (i in Gewaesserlist) {

		//Durchsuchen dnach dem gewählten Element
		if (Gewaesserlist[i].Name == Gewaesser.Name) {

			//Klassen zuweisen (für css Einfärbung)
			document.getElementById((Gewaesserlist[i].Name + "gew")).setAttribute("class", "selectgew");
		}
		else {
			document.getElementById((Gewaesserlist[i].Name + "gew")).setAttribute("class", "gewaesser");
		}
	}
}

/**
 * Funktion um im Div gewähltes Gew$sser aunzuzeigen
 * @param {Object}    Gewaesser    Gewässer mit all seinen Werten
 */
function anzeigeGewdiv(Gewaesser) {

	//Element leeren
	document.getElementById('gewaesserselect').innerHTML = '';

	//für alle Elemente der globalen Liste
	for (i in Gewaesserlist) {
		//Optionen in ID schreiben
		d3.select("#gewaesserselect")
			.append("option")
			.attr("id", Gewaesserlist[i].Name)
			.attr("class", "gewaesser")
			.attr("value", Gewaesserlist[i].Name)
			.text(Gewaesserlist[i].Name)

		//Durchsuchen nach dem gewählten Element
		if (Gewaesserlist[i].Name == Gewaesser.Name) {

			//Klassen zuweisen (für css Einfärbung)
			document.getElementById((Gewaesserlist[i].Name + "gew")).setAttribute("class", "selectgew");
			document.getElementById(Gewaesserlist[i].Name).setAttribute("selected", "1");
		}
		else {
			document.getElementById((Gewaesserlist[i].Name + "gew")).setAttribute("class", "gewaesser");
		}
	}
}

/**
 * Funktion um die Dosis grafisch (in Tuben) darzustellen
 * @param    {Number} Menge    gewählte Dosis (mg/Jahr)
 */
function anzeigeMenge(Menge) {

	//Vlokale Variablen definieren
	var i = 0; //Hilfsvariable
	var Pkte = Menge / dosispropunkt; //Anzahl dargestellter Punkte
	var e = 0; //Hilfsvariable
	var Tube = Math.ceil(Pkte / PunkteproTube); //Anzahl dargestellter Tuben (pro 10 Pkte 1 Tube)

	//Variablen des svg's (Attribute)
	var w = 800;
	var h = 200 * Tube + 100;
	var padding = 150;

	//Alte Elemente in canvas 1 löschen
	document.getElementById('canvas1').innerHTML = '';

	//Create new svg-Element with class circle
	var svg = d3.select("#canvas1")
		.append("svg")
		.attr("class", "circle")
		.attr("width", w)
		.attr("height", h)
		.attr("viewBox", "0 0 " + w + " " + h)

	//erstellen von Tubenelementen
	for (e; e < Tube; e = e + 1) {
		d3.select(".circle")
			.append("rect")
			.attr('class', 'Tube')
			.attr('x', padding - 40)
			.attr('y', e * 200 + 50)
			.attr("width", 400)
			.attr("height", 100)
			.attr("style", "fill:none;stroke:none;stroke-width:0;fill-opacity:0.0;stroke-opacity:0.0")

		d3.select(".circle")
			.append("polygon")
			.attr('class', 'Tube')
			.attr('points', ((padding - 100) + "," + (e * 200 + 160) + " " + (padding - 70) + "," + (e * 200 + 160) + " " + (padding + 360) + "," +
			(e * 200 + 150) + " " + (padding + 360) + "," + (e * 200 + 50) + " " + (padding - 70) + "," + (e * 200 + 40) +
			" " + (padding - 100) + "," + (e * 200 + 40)))
			.attr("style", "fill:white;stroke:black;stroke-width:2;fill-opacity:1.0;stroke-opacity:1.0")

		d3.select(".circle")
			.append("defs")
			.append("clipPath")
			.attr('id', 'cut-off-bottom')
			.append("rect")
			.attr('x', padding - 70)
			.attr('y', e * 200 + 30)
			.attr('width', 50)
			.attr('height', 140)

		d3.select(".circle")
			.append("ellipse")
			.attr('class', 'Tube')
			.attr('cx', padding - 70)
			.attr('cy', e * 200 + 100)
			.attr("rx", 40)
			.attr("ry", 60)
			.attr('clip-path', 'url(#cut-off-bottom)')
			.attr("style", "fill:white;stroke:black;stroke-width:2;fill-opacity:1.0;stroke-opacity:1.0")

		d3.select(".circle")
			.append("ellipse")
			.attr('class', 'Tube')
			.attr('cx', padding - 100)
			.attr('cy', e * 200 + 100)
			.attr("rx", 40)
			.attr("ry", 60)
			.attr("style", "fill:white;stroke:black;stroke-width:2;fill-opacity:1.0;stroke-opacity:1.0")

		//erstellen von einzelnen Punkten (pro 0.1g des Wirkstoffes 1 Pkt)
		for (i; i < Pkte && i < ((e + 1) * PunkteproTube); i = i + 1) {
			d3.select(".circle")
				.append("circle")
				.attr('class', 'circle')
				.attr("cx", (i % (PunkteproTube / 2)) * 80 + padding)
				.attr("cy", Math.floor(i / (PunkteproTube / 2)) * 50 + 75 + Math.floor(i / PunkteproTube) * 100)
				.attr("r", 15);
		}
	}
}

/**
 * Funktion um die Dosis mit der Gesamten Bevölkerung zu vergleichen
 * @param {Number}    Menge    gewählte Dosis (mg/ Jahr)
 */
function visMenge(Menge) {

	//Alte Elemente in canvas 1_1 leeren
	document.getElementById('canvas1_1').innerHTML = '';

	//Skalen definieren
	var xScale = d3.scaleLinear()
		.domain([1, 0])
		.range([285, 75]);

	var yScale = d3.scaleLinear()
		.domain([0, Anwendung[1]])
		.range([285, 15]);

	//Achsen definieren
	var xAxis = d3.axisBottom()
		.scale(xScale)
		.ticks(0);

	var yAxis = d3.axisLeft()
		.scale(yScale)
		.ticks(10);

	//SVG definieren
	var svg = d3.select("#canvas1_1")
		.append("svg")
		.attr("id", "vergleichbev")
		.attr("viewBox", "0 0 300 300")

	//Bar Element zeichnen
	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "vergleichbevbar")
		.attr("x", 120)
		.attr("y", (15 + 270 - (270 / Anwendung[1] * Menge)))
		.attr("height", (270 / Anwendung[1] * Menge))
		.attr("width", 100)

	//Durchschnitt hinzufügen (als Linie)
	svg.append("g")
		.append("line")
		.attr("class", "durchschnitt")
		.attr("x1", 75)
		.attr("y1", (15 + 270 - 270 / Anwendung[1] * durchschnitt))
		.attr("x2", 285)
		.attr("y2", (15 + 270 - 270 / Anwendung[1] * durchschnitt))

	//Text hinzufügen
	svg.select("g")
		.append("text")
		.attr("class", "durchschnitttext")
		.attr("x", 0)
		.attr("y", 5)
		.attr("dy", "0.5em")
		.attr("transform", "translate(" + 285 + "," + (15 + 270 - 270 / Anwendung[1] * durchschnitt) + ")")
		.attr("text-anchor", "end")
		.attr("font-size", 10)
		.attr("font-family", "sans-serif")
		.text("Durchschnitt")

	//X-Achse kreieren
	svg.append("g")
		.attr("class", "axis")
		.attr("transform", "translate(0,285)")
		.call(xAxis);

	//Y-Achse kreieren
	svg.append("g")
		.attr("id", "axisymg")
		.attr("class", "axis")
		.attr("transform", "translate(75, 0)")
		.call(yAxis);

	//Achsentext um den String mg/Jahr erweitern
	a = document.getElementById('vergleichbev').getElementById('axisymg').getElementsByClassName('tick');
	v = [];
	for (i in a) {
		if (i < a.length) {
			v[i] = a[i].getElementsByTagName('text')[0].innerHTML + "mg/Jahr";
			a[i].getElementsByTagName('text')[0].innerHTML = v[i];
		}
	}
}

/**
 * Funktion herauszugeben/ zu berechnen, welche Menge der Körper nicht abbaut
 * @param {Number}    Menge    gewählte Dosis (mg/ Jahr)
 * @param {Object} Wirkstoff Wirkstofftyp mit all seinen Werten
 */
function berAbbKoerper(Wirkstoff, Menge) {
	koerpernichtabbau = (1 - Wirkstoff.Abbaukoerper) * Menge;

	return (koerpernichtabbau);
}

/**
 * Funktion zum darstellen welcher Anteil vom Körper abgebaut wird (symbolisch)
 * @param {Number}    Koerpernichtabbau    vom Körper nicht abgebaute Doseis (mg/ Jahr)
 * @param {Object} Wirkstoff Wirkstofftyp mit all seinen Werten
 */
function anzeigeAbbKoerper(Menge, Koerpernichtabbau) {

	//Canvas2/ generation1 leeren
	document.getElementById('canvas2').innerHTML = '';
	document.getElementById('canvas2_1').innerHTML = '';
	document.getElementById('canvas2_2').innerHTML = '';

	//neue svg-Elemente kreieren
	d3.select("#canvas2")
		.append("svg")
		.attr("class", "abbau1")
		.attr("viewBox", "0 0 100 200")

	d3.select("#canvas2_1")
		.append("svg")
		.attr("class", "abbau2")
		.attr("viewBox", "0 0 100 200")

	d3.select("#canvas2_2")
		.append("svg")
		.attr("class", "abbau3")
		.attr("viewBox", "0 0 100 100")

	//Grundgerüst für vom Körper abgebaute Wirkstoffe
	d3.select("#canvas2")
		.append("p")
		.attr("id", "abbtext")
		.attr("class", "abbautext")
		.text("Abbau im Körper: " + (Math.round(( 1 - (Koerpernichtabbau / Menge)) * 100) ) + "%")

	d3.select(".abbau1")
		.append("polygon")
		.attr('class', 'Abbau')
		.attr('points', (100 + "," + 200 + " " + 50 + "," + 0 + " " + 0 + "," + 200))
		.attr("style", "fill:white;stroke:black;stroke-width:2;fill-opacity:1.0;stroke-opacity:1.0")

	d3.select(".abbau1")
		.append("circle")
		.attr('class', 'Abbau')
		.attr('cx', 50)
		.attr('cy', 50)
		.attr('r', 50)
		.attr("style", "fill:white;stroke:black;stroke-width:2;fill-opacity:1.0;stroke-opacity:1.0")

	d3.select(".abbau1")
		.append("rect")
		.attr('class', 'Abbau')
		.attr('x', 25)
		.attr('y', 100)
		.attr("width", 50)
		.attr("height", 100)
		.attr("style", "fill:none;stroke:none;stroke-width:0;fill-opacity:0.0;stroke-opacity:0.0")

	//Grundgerüst für vom Körper nicht abgebaute Wirkstoffe
	d3.select("#canvas2_1")
		.append("p")
		.attr("id", "nabbtext")
		.attr("class", "abbautext")
		.text("Ins Abwasser: " + Math.round((Koerpernichtabbau / Menge * 100 )) + "%")

	d3.select(".abbau2")
		.append("rect")
		.attr('class', 'InUmlauf')
		.attr('x', 0)
		.attr('y', 0)
		.attr("width", 50)
		.attr("height", 100)
		.attr("style", "fill:none;stroke:none;stroke-width:5;fill-opacity:0.1;stroke-opacity:0.9")

	d3.select(".abbau2")
		.append("defs")
		.append("pattern")
		.attr("id", "image")
		.attr("x", "0%")
		.attr("y", "0%")
		.attr("height", "100%")
		.attr("width", "100%")
		.attr("viewBox", "0 0 100 100")
		.append("image")
		.attr("x", "0")
		.attr("y", "0")
		.attr("width", "100")
		.attr("height", "100")
		.attr("xlink:href", "img/abbau.png")

	d3.select(".abbau2")
		.append("rect")
		.attr('class', 'InUmlauf')
		.attr('x', 0)
		.attr('y', 100)
		.attr("width", 100)
		.attr("height", 100)
		.attr("fill", "url(#image)")

	//Grundgerüst für vom Körper nicht abgebaute Wirkstoffe pro 300l Abwasser
	d3.select("#canvas2_2")
		.append("p")
		.attr("id", "vermtext")
		.attr("class", "abbautext")
		.text("Vermischung mit " + vermabwasser + "hl Abwasser")

	d3.select(".abbau3")
		.append("defs")
		.append("pattern")
		.attr("id", "image2")
		.attr("x", "0%")
		.attr("y", "0%")
		.attr("height", "100%")
		.attr("width", "100%")
		.attr("viewBox", "0 0 400 100")
		.append("image")
		.attr("x", "0")
		.attr("y", "0")
		.attr("width", "400")
		.attr("height", "100")
		.attr("xlink:href", "img/welle.png")

	var imgheight = 0;

	for (imgheight; imgheight < 100; i = imgheight = imgheight + 25) {
		d3.select(".abbau3")
			.append("rect")
			.attr('class', 'inclAbwasser')
			.attr('x', 0)
			.attr('y', imgheight)
			.attr("width", 100)
			.attr("height", 25)
			.attr("style", "fill:url(#image2)")
	}

	d3.select(".abbau3")
		.append("rect")
		.attr('class', 'inclAbwasser')
		.attr('x', 0)
		.attr('y', 0)
		.attr("width", 100)
		.attr("height", 100)
		.attr("style", "fill:none;stroke:black;stroke-width:5;fill-opacity:0.1;stroke-opacity:0.9")

	//lokale Variablen zur Darstellung
	var i = 0;
	var e = 0;

	var Pkte_total = Math.round(Menge / dosispropunkt);
	var Pktenicht_abgeb = Math.round(Koerpernichtabbau / dosispropunkt);
	var Pkte_abgeb = Pkte_total - Pktenicht_abgeb;

	var Anz_horiz_Abbau = Math.ceil(Math.sqrt(1 / 2) * Math.sqrt(Pkte_abgeb));
	var Anz_vert_Abbau = Math.ceil(Pkte_abgeb / Anz_horiz_Abbau);
	var Abstand_horiz_Abbau = 50 / Anz_horiz_Abbau;
	var Abstand_vert_Abbau = 100 / Anz_vert_Abbau;

	//effektive abgebaute Wirkstoffe
	for (i; i < Pkte_abgeb; i = i + 1) {
		d3.select(".abbau1")
			.append("circle")
			.attr('class', 'Abbau')
			.attr("cx", ((i % Anz_horiz_Abbau) + 0.5 ) * Abstand_horiz_Abbau + 25)
			.attr("cy", (Math.floor(i / Anz_horiz_Abbau) + 0.5) * Abstand_vert_Abbau + 100)
			.attr("r", 5);
	}

	//lokale Variablen zur Darstellung
	Anz_horiz_nAbbau = Math.ceil(Math.sqrt(1 / 2) * Math.sqrt(Pktenicht_abgeb));
	Anz_vert_nAbbau = Math.ceil(Pktenicht_abgeb / Anz_horiz_nAbbau);
	Abstand_horiz_nAbbau = 50 / Anz_horiz_nAbbau;
	Abstand_vert_nAbbau = 100 / Anz_vert_nAbbau;

	//effektive in Umwelt entlassene Wirkstoffe
	for (e; e < Pktenicht_abgeb; e = e + 1) {
		d3.select(".abbau2")
			.append("circle")
			.attr('class', 'nAbbau')
			.attr("cx", ((e % Anz_horiz_nAbbau) + 0.5 ) * Abstand_horiz_nAbbau)
			.attr("cy", (Math.floor(e / Anz_horiz_nAbbau) + 0.5) * Abstand_vert_nAbbau)
			.attr("r", 5);
	}

	//lokale Variablen zur Darstellung
	var Abstand = 100 / Math.sqrt(Pktenicht_abgeb);
	var Anz = Math.ceil(100 / Abstand);
	var Abstand_neu_horiz = 100 / Anz;
	var Anz_vert = Math.ceil(Pktenicht_abgeb / Anz);
	var Abstand_neu_vert = 100 / Anz_vert;

	var u = 0;

	//Wirkstoffe pro 3000hl Abwasser
	for (u; u < Pktenicht_abgeb; u = u + 1) {
		d3.select(".abbau3")
			.append("circle")
			.attr('class', 'nAbbau')
			.attr("cx", u % Anz * Abstand_neu_horiz + 0 + Abstand_neu_horiz / 2)
			.attr("cy", Math.floor(u / (Anz)) * Abstand_neu_vert + 0 + Abstand_neu_vert / 2)
			.attr("r", 5);
	}
}

/**
 * Funktion zur Darstellung der im Körper abgebauten Stoffe (zahlenmässig/ dimensionsmässig)
 * @param    {Number} Menge    Dosis (mg/Jahr)
 * @param {Number} Koerpernichtabbau  Im Körper nicht abgebauter Anteil der Stoffe
 */
function visAbbKoerper(Menge, Koerpernichtabbau) {

	//canvas 2_1 leeren
	document.getElementById('canvas2_3').innerHTML = '';

	//Skalen definieren
	var xScale = d3.scaleLinear()
		.domain([1, 0])
		.range([230, 40]);

	var yScaleleft = d3.scaleLinear()
		.domain([0, 100])
		.range([285, 15]);

	var yScaleright = d3.scaleLinear()
		.domain([0, Menge])
		.range([285, 15]);

	//Achsen definieren
	var xAxis = d3.axisBottom()
		.scale(xScale)
		.ticks(0);

	var yAxisleft = d3.axisLeft()
		.scale(yScaleleft)
		.ticks(10);

	var yAxisright = d3.axisRight()
		.scale(yScaleright)
		.ticks(Math.ceil(Menge * 2));

	//svg erstellen
	var svg = d3.select("#canvas2_3")
		.append("svg")
		.attr("id", "nicht_abgebaut")
		.attr("viewBox", "0 0 300 300")

	//bar's und beschreibende Texte erstellen
	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "vomKoerpernicht_abgebautbar")
		.attr("x", 85)
		.attr("y", (15 + 270 - (270 * (Koerpernichtabbau / Menge))))
		.attr("height", (270 - (270 * (1 - (Koerpernichtabbau / Menge))))/*(270 * abbaukoerper)*/)
		.attr("width", 100)

	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("text")
		.attr("class", "vomKoerpernicht_abgebautbartext")
		.attr("text-anchor", "middle")
		.attr("alignment-baseline", "hanging")
		.attr("font-size", 8)
		.attr("x", 85 + 50)
		.attr("dy", "0.32em")
		.attr("y", (15 + 270 - (270 * (Koerpernichtabbau / Menge))))
		.text("Ins Abwasser: " + Math.round((Koerpernichtabbau / Menge) * 100) + "%")

	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "vomKoerper_abgebautbar")
		.attr("x", 85)
		.attr("y", 15)
		.attr("height", (270 * (1 - (Koerpernichtabbau / Menge))))
		.attr("width", 100)

	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("text")
		.attr("class", "vomKoerpernicht_abgebautbartext")
		.attr("text-anchor", "middle")
		.attr("alignment-baseline", "hanging")
		.attr("font-size", 8)
		.attr("x", 85 + 50)
		.attr("dy", "0.32em")
		.attr("y", 15)
		.text("Abbau im Körper: " + Math.round((1 - (Koerpernichtabbau / Menge)) * 100) + "%")

	//Achsen definieren und Beschriftung anpassen
	svg.append("g")
		.attr("id", "axisbottom")
		.attr("class", "axis")
		.attr("transform", "translate(0,285)")
		.call(xAxis);

	svg.append("g")
		.attr("id", "axisleft")
		.attr("class", "axis")
		.attr("transform", "translate(40, 0)")
		.call(yAxisleft);

	a = document.getElementById('nicht_abgebaut').getElementById('axisleft').getElementsByClassName('tick');
	v = [];
	for (i in a) {
		if (i < a.length) {
			v[i] = a[i].getElementsByTagName('text')[0].innerHTML + "%";
			a[i].getElementsByTagName('text')[0].innerHTML = v[i];
		}
	}

	svg.append("g")
		.attr("id", "axisright")
		.attr("class", "axis")
		.attr("transform", "translate(230, 0)")
		.call(yAxisright);

	a = document.getElementById('nicht_abgebaut').getElementById('axisright').getElementsByClassName('tick');
	v = [];
	for (i in a) {
		if (i < a.length) {
			v[i] = a[i].getElementsByTagName('text')[0].innerHTML + Anwendung[2];
			a[i].getElementsByTagName('text')[0].innerHTML = v[i];
		}
	}
}

/**
 * Funktion zur Berechnung der Menge pro hl
 * @param    {Number} Vermabw    Anzahl hl Abwasser
 * @param {Number} Koerpernichtabbau  Im Körper nicht abgebauter Anteil der Stoffe
 */
function vermAbw(Koerpernichtabbau, Vermabw) {
	imabw = Koerpernichtabbau / Vermabw;

	return (imabw);
}

/**
 * Funktion zur Berechnung der Menge nach dem Durchgang einer Kläranlage
 * @param    {Number} ImAbw    ImAbwasser vor Kläranlage vorhandene Stoffe
 * @param {String} Anlage  Anlagetyp
 * @param {Object} Wirkstoff gewählter Wirkstoff mit all seinen Attributen
 */
function abbauAnlage(ImAbw, Anlage, Wirkstoff) {
	nachanl = ImAbw * (1 - Wirkstoff[Anlage.Klaeranlagename]);

	return (nachanl);
}

/**
 * Funktion zur Berechnung der im Gewässer vorhandenen Stoffe
 * @param    {Number} Nachanl    Dosis nach Kläranlagedurchlauf
 * @param {Object} Gewaesser  gewähltes Gewässer mit all seinen Eigenschaften
 */
function vermGew(Nachanl, Gewaesser) {
	restwert = Nachanl / Gewaesser.Verduennungswert;

	return (restwert);
}

/**
 * Funktion zur Darstellung der im vorhandenen Stoffe und Vergleich mit dem Grenzwert
 * @param    {Number} Restwert   Dosis im Gewässer (pro Hektare)
 * @param {Object} Wirkstoff  gewählter Wirkstoff mit all seinen Eigenschaften
 * @param {Number} Darstw  dargestellet Wassermenge
 */
function anzeigeRest(Restwert, Wirkstoff, Darstw) {

	//canvas leeren
	document.getElementById('canvas3').innerHTML = '';

	//Fischbild und Resultattext wählen
	if ((Math.round(Wirkstoff.Grenzwert * Darstw / dosispropunkt)) < (Math.round(Restwert * Darstw / dosispropunkt))) {
		var img4 = img[0];
		var verglstr = resulttext[0];
	}
	else if ((Math.round(Wirkstoff.Grenzwert * Darstw / dosispropunkt)) == (Math.round(Restwert * Darstw / dosispropunkt))) {
		var img4 = img[1];
		var verglstr = resulttext[1];
	}
	else {
		var img4 = img[2];
		var verglstr = resulttext[2];
	}

	//canvas neu aufbauen (incl Texte)
	d3.select("#canvas3")
		.append("svg")
		.attr("class", "vergleich1")
		.attr("viewBox", "0 0 100 100")

	d3.select("#canvas3")
		.append("p")
		.attr("id", "modtext")
		.attr("class", "vergltext")
		.text("Modell")

	d3.select("#canvas3")
		.append("svg")
		.attr("class", "vergleich2")
		.attr("viewBox", "0 0 100 100")

	d3.select("#canvas3")
		.append("p")
		.attr("id", "grzwtext")
		.attr("class", "vergltext")
		.text("Grenzwert (pro " + Darstw + "hl)")

	d3.select("#canvas3")
		.append("svg")
		.attr("class", "vergleich3")
		.attr("viewBox", "0 0 100 200")

	d3.select("#canvas3")
		.append("p")
		.attr("id", "vergltext")
		.attr("class", "vergltext")
		.text(verglstr)

	//Grundgerüst für Modell
	d3.select(".vergleich1")
		.append("defs")
		.append("pattern")
		.attr("id", "image3")
		.attr("x", "0%")
		.attr("y", "0%")
		.attr("height", "100%")
		.attr("width", "100%")
		.attr("viewBox", "0 0 400 100")
		.append("image")
		.attr("x", "0")
		.attr("y", "0")
		.attr("width", "400")
		.attr("height", "100")
		.attr("xlink:href", "img/welle.png")

	d3.select(".vergleich2")
		.append("defs")
		.append("pattern")
		.attr("id", "image3")
		.attr("x", "0%")
		.attr("y", "0%")
		.attr("height", "100%")
		.attr("width", "100%")
		.attr("viewBox", "0 0 400 100")
		.append("image")
		.attr("x", "0")
		.attr("y", "0")
		.attr("width", "400")
		.attr("height", "100")
		.attr("xlink:href", "img/welle.png")

	d3.select(".vergleich3")
		.append("defs")
		.append("pattern")
		.attr("id", "image4")
		.attr("x", "0%")
		.attr("y", "0%")
		.attr("height", "100%")
		.attr("width", "100%")
		.attr("viewBox", "0 0 200 100")
		.append("image")
		.attr("x", "0")
		.attr("y", "0")
		.attr("width", "200")
		.attr("height", "100")
		.attr("xlink:href", img4)

	var imgheight = 0;

	for (imgheight; imgheight < 100; imgheight = imgheight + 25) {
		d3.select(".vergleich1")
			.append("rect")
			.attr('class', 'inclAbwasser')
			.attr('x', 0)
			.attr('y', imgheight)
			.attr("width", 100)
			.attr("height", 25)
			.attr("style", "fill:url(#image3)")

		d3.select(".vergleich2")
			.append("rect")
			.attr('class', 'inclAbwasser')
			.attr('x', 0)
			.attr('y', imgheight)
			.attr("width", 100)
			.attr("height", 25)
			.attr("style", "fill:url(#image3)")
	}

	d3.select(".vergleich1")
		.append("rect")
		.attr('class', 'Modell')
		.attr('x', 0)
		.attr('y', 0)
		.attr("width", 100)
		.attr("height", 100)
		.attr("style", "fill:none;stroke:black;stroke-width:5;fill-opacity:0.1;stroke-opacity:0.9")

	//Grundgerüst für Grenzwert
	d3.select(".vergleich2")
		.append("rect")
		.attr('class', 'Grenzwert')
		.attr('x', 0)
		.attr('y', 0)
		.attr("width", 100)
		.attr("height", 100)
		.attr("style", "fill:none;stroke:black;stroke-width:5;fill-opacity:0.1;stroke-opacity:0.9")

	//Hilfsvariablen für die Punktanzahl incl Punktabbildung
	var Pktenicht_abgeb = Math.round(Restwert * Darstw / dosispropunkt);
	var Abstand = 100 / Math.sqrt(Pktenicht_abgeb);
	var Anz = Math.ceil(100 / Abstand);
	var Abstand_neu_horiz = 100 / Anz;
	var Anz_vert = Math.ceil(Pktenicht_abgeb / Anz);
	var Abstand_neu_vert = 100 / Anz_vert;

	var u = 0;
	for (u; u < Pktenicht_abgeb; u = u + 1) {
		//Wirkstoffe pro 300l Abwasser
		d3.select(".vergleich1")
			.append("circle")
			.attr('class', 'Modell')
			.attr("cx", u % Anz * Abstand_neu_horiz + Abstand_neu_horiz / 2)
			.attr("cy", Math.floor(u / (Anz)) * Abstand_neu_vert + Abstand_neu_vert / 2)
			.attr("r", 5);
	}

	Pktenicht_abgeb = Math.round(Wirkstoff.Grenzwert * Darstw / dosispropunkt);
	Abstand = 100 / Math.sqrt(Pktenicht_abgeb);
	Anz = Math.ceil(100 / Abstand);
	Abstand_neu_horiz = 100 / Anz;
	Anz_vert = Math.ceil(Pktenicht_abgeb / Anz);
	Abstand_neu_vert = 100 / Anz_vert;

	u = 0;
	for (u; u < Pktenicht_abgeb; u = u + 1) {
		//Wirkstoffe pro 300l Abwasser
		d3.select(".vergleich2")
			.append("circle")
			.attr('class', 'Grenzwert')
			.attr("cx", u % Anz * Abstand_neu_horiz + Abstand_neu_horiz / 2)
			.attr("cy", Math.floor(u / (Anz)) * Abstand_neu_vert + Abstand_neu_vert / 2)
			.attr("r", 5);
	}

	d3.select(".vergleich3")
		.append("rect")
		.attr('class', 'inclAbwasser')
		.attr('x', 0)
		.attr('y', 0)
		.attr("width", 200)
		.attr("height", 100)
		.attr("style", "fill:url(#image4)")

}

/**
 * Funktion zur Darstellung der im Gewässer noch vorhandener Stoffe (zahlenmässig/ dimensionsmässig)
 * @param    {Number} Menge    Dosis (mg/Jahr)
 * @param {Number} Restwert  Restwert im Gewässer
 * @param {Object} Wirkstoff  gewählter Wirkstoff mit all seinen Eigenschaften
 */
function visRest(Menge, Restwert, Wirkstoff) {

	//suche minimalen Gewässerverdünnungswert
	var minverdW = Gewaesserlist[0].Verduennungswert;
	for (i in Gewaesserlist) {

		if (Gewaesserlist[i].Verduennungswert < minverdW) {
			minverdW = Gewaesserlist[i].Verduennungswert;
		}
		else {
			minverdW = minverdW;
		}
	}

	//maximale Dosis mit minimalem Verdünnungswert und ohne Anlage verrechnet.
	var ohneanlmitminverdW = Anwendung[1] * (1 - wirkstoff.Abbaukoerper) / vermabwasser / minverdW;

	//maximaler Wert der Domain festlegen
	if (ohneanlmitminverdW < Wirkstoff.Grenzwert) {
		var maxWert_Domain = Wirkstoff.Grenzwert;
	}
	else {
		var maxWert_Domain = ohneanlmitminverdW;
	}

	//canvas leeren
	document.getElementById('canvas3_1').innerHTML = '';

	//Skalen und Achsen definieren
	var xScale = d3.scaleLinear()
		.domain([1, 0])
		.range([285, 65]);

	var yScale = d3.scaleLinear()
		.domain([0, maxWert_Domain * 1000])
		.range([285, 15]);

	var xAxis = d3.axisBottom()
		.scale(xScale)
		.ticks(0);

	var yAxis = d3.axisLeft()
		.scale(yScale)
		.ticks(10);

	//svg definieren
	var svg = d3.select("#canvas3_1")
		.append("svg")
		.attr("id", "restwert")
		.attr("viewBox", "0 0 300 300")

	svg.selectAll("bar")
		.data([1])
		.enter()
		.append("rect")
		.attr("class", "restwert")
		.attr("x", 115)
		.attr("y", (15 + 270 - (270 * Restwert / maxWert_Domain)))
		.attr("height", (270 * Restwert / maxWert_Domain))
		.attr("width", 100)

	//Grenzwert hinzufügen
	svg.append("g")
		.append("line")
		.attr("class", "grenzwert")
		.attr("x1", 65)
		.attr("y1", (15 + 270 - (270 * Wirkstoff.Grenzwert / maxWert_Domain)))
		.attr("x2", 285)
		.attr("y2", (15 + 270 - (270 * Wirkstoff.Grenzwert / maxWert_Domain)))

	svg.select("g")
		.append("text")
		.attr("class", "grenzwerttext")
		.attr("x", 0)
		.attr("y", 5)
		.attr("dy", "0.5em")
		.attr("transform", "translate(" + 285 + "," + (15 + 270 - (270 * Wirkstoff.Grenzwert / maxWert_Domain)) + ")")
		.attr("text-anchor", "end")
		.attr("font-size", 10)
		.attr("font-family", "sans-serif")
		.text("Grenzwert")

	//Create X axis
	svg.append("g")
		.attr("class", "axis")
		.attr("transform", "translate(0,285)")
		.call(xAxis);

	//Create Y axis
	svg.append("g")
		.attr("id", "axisy")
		.attr("class", "axis")
		.attr("transform", "translate(65, 0)")
		.call(yAxis);

	//Text anpassen
	a = document.getElementById('restwert').getElementById('axisy').getElementsByClassName('tick');
	v = [];
	for (i in a) {
		if (i < a.length) {
			v[i] = a[i].getElementsByTagName('text')[0].innerHTML + "μg/hl";
			a[i].getElementsByTagName('text')[0].innerHTML = v[i];
		}
	}
}
